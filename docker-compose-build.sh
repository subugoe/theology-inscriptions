#!/bin/bash

PROJECT_NAME=$(basename $PWD |  tr '[:upper:]' '[:lower:]')
DOCKER_DIR=./docker
BLACKLIST="common deployment"

# Settings
EXPERIMENTAL=false
SEPARATOR_CHAR=_
PREFIX=""

# Colours
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RESET='\033[0m'

#Check if `docker` command exits
if [ -x "$(command -v docker)" ] ; then
	DOCKER="$(command -v docker)"
elif [ -x "$DOCKER" ] ; then
	DOCKER="$DOCKER"
else
	echo -e "${RED}'docker' command not found, exiting!${RESET}"
	exit 2
fi

# Check for param 1 , additional BUILD_ARGS
if [ -z "${1}" ] ; then
    echo -e "${YELLOW}No build args given${RESET}"
else
	shift
	BUILD_ARGS="$@"
fi

# Check if external $IMAGE_SEPERATOR is set
if [ -x "${IMAGE_SEPERATOR}" ] ; then
	SEPARATOR_CHAR="${IMAGE_SEPERATOR}"
fi
echo -e "${YELLOW}Name of the image will use '${SEPARATOR_CHAR}' as separator${RESET}"

# Check if external $IMAGE_PREFIX is set
if [ -x "${IMAGE_PREFIX}" ] ; then
	PREFIX="${IMAGE_PREFIX}"
fi
echo -e "${YELLOW}Name of the image will use '${PREFIX}' as prefix${RESET}"

echo "Building images"
for DIR in $DOCKER_DIR/*; do
    NAME=`basename $DIR`
    if [[ -d "${DIR}" ]] && [[ ! "${BLACKLIST}" =~ "${NAME}" ]] ; then
		if [ -f docker/${NAME}/Dockerfile ] ; then 
			echo "Processing ${DIR}"
			IMAGE_TAG="${PREFIX}${PROJECT_NAME}${SEPARATOR_CHAR}${NAME}"
			echo -e "${GREEN}Building '${DOCKER_DIR}/${NAME}/Dockerfile' (Tag: '${IMAGE_TAG}')${RESET}"
			DOCKER_BUILDKIT=1 $DOCKER build -t ${IMAGE_TAG} $BUILD_ARGS -f ${DOCKER_DIR}/${NAME}/Dockerfile .
		fi
    fi
done

echo -e "${GREEN}Everything has been build, make sure you run 'docker-compose up' with the '--no-build' flag${RESET}"
