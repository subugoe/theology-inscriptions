<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('var')
    ->exclude('vendor')
    ->exclude('node_modules')
    ->exclude('src/Migrations')
    ->exclude('src/Util/Search/Query')
;

$config = new PhpCsFixer\Config();
    return $config->setRules([
        '@Symfony' => true,
        '@DoctrineAnnotation' => true,
        /* readability */
        'array_syntax' => ['syntax' => 'short'],
        'trailing_comma_in_multiline' => false,
        /* This works locally, but not on GitLab - the line above throws a warning locally
        'trailing_comma_in_multiline' => false, */
        'yoda_style' => false,
        'no_useless_else' => true,
        /* Whitespace and formating */
        'concat_space' => ['spacing' => 'one'],
        'no_blank_lines_after_class_opening' => true,
        'no_trailing_whitespace' => true,
        'no_whitespace_in_blank_line' => true,
        'no_extra_blank_lines' => true,
        'array_indentation' => true,
        'blank_line_before_statement' => true,
        'braces' => true,
        /* other */
        'no_superfluous_phpdoc_tags' => false,
        'doctrine_annotation_spaces' => false,
        'doctrine_annotation_array_assignment' => ['operator' => '=']
    ])
    ->setFinder($finder)
;
