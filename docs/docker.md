Overview about the Docker images
================================

# Docker images

A list of pre build images and their build process is available in the [GitLab documentation](./gitlab.md).
**Note:** You should always use the provided images.

## `app` image

### Building

#### Build the base image

The following line builds the application container, it should be only used to
debug or enhance the application container (connections settings etc.).

**Note:** This isn't really needed anymore if you don't plan to work on it, since it's provided by 
[GitLab](https://gitlab.gwdg.de/subugoe/theology-inscriptions/container_registry).
**Don't follow this step until you know what you're doing!**

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t docker.gitlab.gwdg.de/subugoe/theology-inscriptions:dev -f docker/app/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:** You need to change the version tag of the base version if you plan to build a image from the 
`master` branch, default (see above) is the `dev` branch. Make also sure you change the `FROM` line in 
`docker/app/Dockerfile`.

**Note:** If you didn't build the base image on your local machine one will be fetched from GitLab. 
By default this is the one build from master by default. Change the `FROM:` declaration in `Dockerfile` 
to change it to `dev`. 

#### Building the `dev` variant

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t docker.gitlab.gwdg.de/subugoe/theology-inscriptions/dev:dev -f docker/app/Dockerfile.dev .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## `web-maintenance` image

The `web-maintenance` image used to be part of this project and is now moved to the [SUB Docker SDK Repository](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/docker/web-maintenance).

## `search` image

This image extends `docker.elastic.co/elasticsearch/elasticsearch:6.8.6` to include some Geonames data for the region of the inscriptions.
See the [documentation on geo data](./geodata.md) if you need to create your own pre configured Docker image or just want to 
update existing data.

### Building

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t docker.gitlab.gwdg.de/subugoe/theology-inscriptions/search:dev -f docker/search/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Arguments and environment

None

### Important paths inside the image

Applies after the image has been build, only our own additions

| Name                          | Description                                    |
|-------------------------------|------------------------------------------------|
| /usr/local/bin/wait-for-it.sh | Script to wait until Elasticsearch has started |                 


## Other images

The other containers use externally provided images:

| Container         | Image                                                                                                                                              | Notes                                                                                            |
|-------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| `web`             | [`nginx:stable-alpine`](https://hub.docker.com/_/nginx)                                                                                            |                                                                                                  |
| `web-maintenance` | [`docker.gitlab.gwdg.de/subugoe/sub-docker-sdk/web-maintenance`](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/docker/web-maintenance) |                                                                                                  |
| `database`        | [`mysql:5.5`](https://hub.docker.com/_/mysql)                                                                                                      | Never Update this container beyond 5.5.x since the version should match the GWDG MySQL appliance | 
| `kibana`          | [`docker.elastic.co/kibana/kibana:6.8.6`](docker.elastic.co/kibana/kibana:6.8.6)                                                                   |                                                                                                  |

# Using `docker-compose`

## `docker-compose` files and dependent configuration

| `docker-compose` file         | Purpose                              | Remarks                            | `.env` file | Webserver configuration     | `APP_ENV`     |
|-------------------------------|--------------------------------------|------------------------------------|-------------|-----------------------------|---------------|
| docker-compose.yml            | Base file                            | Production is the default use case | `.env`      | `docker/web/site.conf`      | `DEV`         |
| docker-compose.dev.yml        | Used for <u>local</u> development    | Defines it's own network `dev`     | `.env.dev`  | `docker/web/site.conf`      | `DEV`         |
| docker-compose.kibana.yml     | Additionally adds a Kibana container | Listens on port 5601               | `.env.dev`  | Doesn't apply               | Doesn't apply |
| docker-compose.dev-server.yml | Configuration for test server        |                                    | `.env`      | `docker/web/site.dev.conf`  | `DEV`         |
| docker-compose.prod.yml       | Configuration for production server  | Doesn't include database container | `.env.prod` | `docker/web/site.prod.conf` | `PROD`        |

## Checking the effective configuration for combined `docker-compose` files

To debug settings of combined `docker-compose` files the following command is helpful:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.dev.yml config
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Advanced Docker usage

## Building (and pulling) all required containers

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./docker-compose-build.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(Use this only if you need to debug the Docker build process or `docker-compose`!)

Repulling the needed containers to get updates, make sure you add the wanted `docker-compose.*.yml` files as parameters to 
`docker-compose`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose pull
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Using BuildKit

You need to use BuildKit since some new features of it are required now. It speeds up sequent builds and is used to 
create smaller images. Docker newer then 18.06 is required.
Mak sure you need the following Prefix:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To get a complete log of a BuildKit build you also need to pass the following environment variable:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BUILDKIT_PROGRESS=plain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## (Re-) tagging pre build images of another branch

If you want to test a pre build image of another branch without changing your `docker-compose.yml` file(s), 
you can just pull the desired image and retag it locally:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker tag docker.gitlab.gwdg.de/subugoe/theology-inscriptions/dev:symfony-user docker.gitlab.gwdg.de/subugoe/theology-inscriptions/dev:dev 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Make sure you change the suffix after the last slash (`dev:symfony-user`).