Selenium testing
================

This branch is now merged with the `selenium` branch.

# Additions

| File / Directory   | Description                            |
|--------------------|----------------------------------------|
| `docs/selenium.md` | This file                              |
| `tests/selenium`   | Directory for Selenium files           |
| `gitlab-ci.yml`    | Place for building and testing         |

## Debugging

To debug the selenium container you can run it in a similar setup as it would be done by Gitlab:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker run --mount src=`pwd`,target=/test,type=bind -it docker.gitlab.gwdg.de/subugoe/sub-docker-sdk/selenium:latest /bin/sh -c 'cd /test; /bin/sh'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Usage

## Creating tests

To create tests you need the [Selenium IDE](https://selenium.dev/selenium-ide/), a documentation of this software is out of scope of this document. 
Save the test files you've created in `tests/selenium`.

## Running existing tests (browser based)

A description how to use the tests manually look at the [README](../tests/selenium/README.md) (in German).

## Running existing tests (command line - Docker)
 
Using the container you can run the Selenium test using the wrapper script directly, after you started the `selenium` image.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CONSOLE=true SIDE_RUNNER_CONFIG=./tests/selenium/config/side.yml selenium-wrapper.sh tests/selenium/*.side
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~