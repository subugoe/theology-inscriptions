Production
==========

For production there is a special `docker-compose` file:
`docker-compose.prod.yml`. It sets it's own environment variables and port binds
(see all changes below).

**Important:** Make sure that the [virtual memory is configured](./troubleshoot.md#note_linux) to run ElasticSearch!

# Quick start

## Copy the required files

Get the files from the `docker/deployment/master` directory (or use the GitLab CI as described below) and copy them 
to the server. Run the following line to start the service:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose up -d --pull
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Updating the application

Part of the files in `docker/deployment/master` is the script `restarted-updated.sh`. It stops the containers (as defined in `deployment/master/docker-compose.yml`), 
removes the `web-app` volume, starts the containers again (assuming thet there are new versions of the images) 
and rebuilds the search index.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./restarted-updated.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:** This script doesn't do the needed `--pull` since this is done by the GitLab CI if configured correctly, do 
it yourself, if you don't use this infrastructure.

# Starting from scratch

This used to be the old Quick start guide before there has been a prebuild image from the [Docker registry](https://gitlab.gwdg.de/subugoe/theology-inscriptions/container_registry).

Prepare the Docker base image:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker build -t docker.gitlab.gwdg.de/subugoe/copi-base -f docker/app/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Build and run the development environment with one command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up --build -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set up the application:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker exec -it `docker ps | grep inscriptions | grep app |cut -c-12` /usr/local/bin/composer-init.sh admin copidocker@sub.uni-goettingen.de admin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If there is already a running instance shut it down first and clean up. And get new versions of the prebuild containers:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose down
docker volume rm theologyinscriptions_dbdata theologyinscriptions_web-app
docker-compose pull
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Settings in production mode

-   `APP_ENV` is set to `prod`

-   The port for the Nginx (`web`) continer is changed

-   Build time dependencies, like `npm` are removed from the image

-   Unused files are removed

-   The security check triggered by `composer` is disabled

## Setting up automatic deployment using GitLab

<a name="production_gitlab"></a>

### Configuring GitLab

#### `dev` server - [dev.inscriptions.sub.uni-goettingen.de/](https://dev.inscriptions.sub.uni-goettingen.de/)
Make sure you have the 'Pipelines' and 'Container Registry' enabled as project features. Afterwards you can set additional
variables to pass different settings to your `.gitlab-ci.yml` file. For deployment you need something like the following:

| Name                  | Description                                     |
|-----------------------|-------------------------------------------------|
| $DEV_SERVER_USER      | The user to login to the deployment host        |
| $DEV_HOSTNAME         | The host name of the deployment host            |
| $DEV_SERVER_DIRECTORY | The directory to start the deployment script in |
| $DEV_PRIVATE_KEY      | The private key to log into the deployment host |

On the server side you need to assign a runner to your pipeline definition.

To configure the contents of the `$DEV_PRIVATE_KEY` variable you need to create a private key on the deployment host.
Just run the following and press the return key for each question:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ssh-keygen -t rsa -b 4096 -C "copidocker@sub.uni-goettingen.de"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The resulting key will be saved in `~/.ssh/id_rsa`.

Also you need to add the key to the list auf authorized keys:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Configuring `.gitlab-ci.yml`

Additionally the following entries are needed in the `before_script:` section of your `.gitlab-ci.yml` file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  - 'which ssh-agent || ( apk add --update openssh-client )'
  - mkdir -p ~/.ssh
  - eval $(ssh-agent -s)
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  - echo "$DEV_PRIVATE_KEY" | tr -d '\r' | ssh-add -
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You might also add the following to check if the SSH connection works and fail fast if not. 
This is helpful if your repository and thus the docker registry of your project is protected somehow.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  - ssh -q $DEV_SERVER_USER@$DEV_HOSTNAME exit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It's also possible to use the line above to log in to the docker registry to pull the new image(s) from:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  - ssh -q $DEV_SERVER_USER@$DEV_HOSTNAME "docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This makes sure that `ssh-agent` is installed, creates a directory to configure the `ssh` client and places 
the private key in the need file.

Next step is a section in the `.gitlab-ci.yml` file which is bound to the stage `deploy`

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
deploy-dev:
  stage: deploy
  environment:
    name: dev
  only:
    - dev
  except:
    - tags
  script:
    - ssh $DEV_SERVER_USER@$DEV_HOSTNAME "cd $DEV_SERVER_DIRECTORY && ./restart.sh"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### `prod` server - [inscriptions.sub.uni-goettingen.de/](https://inscriptions.sub.uni-goettingen.de/)

The configuration of the semi automatic deployment to the production instance is very similar to the `dev` instance.

**Note:** Semi automatic deployment means, that required images are build and needed server configurations are deployed but 
are not taken into effect. This has to be done manually.

#### GitLab variables

| Name                   | Description                                     |
|------------------------|-------------------------------------------------|
| $PROD_SERVER_USER      | The user to login to the deployment host        |
| $PROD_HOSTNAME         | The host name of the deployment host            |
| $PROD_SERVER_DIRECTORY | The directory to start the deployment script in |
| $PROD_PRIVATE_KEY      | The private key to log into the deployment host |

#### Differences

- `rsync` is used instad of `scp` to avoid files to be overwritten

-  Some server side configuration is needed to set the database connection, this applies for the files `.env` and `doctrine.yml`.

##### Putting deployed changes in action

Make sure you have the database connection configured in `.env` and `doctrine.yml`. If everything is configured well you just 
need to run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose down && docker-compose up --pull -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The last step is to setup the application, if it hasn't been done before:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker exec -it `docker ps | grep inscriptions | grep app |cut -c-12` /usr/local/bin/composer-init.sh $USER $MAIL $PASSWORD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:** Don't run this during an update process.

#### Configuring the docker host

The script `docker/app/scripts/deploy.sh` is used to do the deployment on the host, it will pull a new version of the 
image, stops running containers, deletes old volumes and starts a new container from the update image.

Make sure you have the `deploy.sh` script together with the needed `docker-compose.yml` file(s) and `.env` file in the directory 
defined by the variable `$DEV_SERVER_DIRECTORY`.

#### Setting up full automatic deployment

The repository contains a special directory `deployment`. This can be used to get rid of the need to set up the 
deployment server. This directory contains a sub directory for each environment. For each environment you need to 
create links inside this directory to the files in your repository that are needed.
If you want to add a file make sure these links are relative not absolute to be resolvable if the repository is 
checked out to another directory. You can create sub directories if you need to place files in a special structure.
This way you don't need to change paths to files that are referenced from somewhere else (like docker-compose.yml). 
It's also possible to name the link different to the real file name in the repository to exchange file names in 
different environments. If you want to have these files inside the repository you can also create it on the fly 
by adding `mkdir`, `ln` and / or `cp` to the `script:` section.

**Hint:** Use GNU `ln` with the switch `r` to use absolute paths to create relative links. This can preserve errors with nesting depth. 

To use this deployment structure you need to add the following before calling the deployment script from `.gitlab-ci.yml`:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
deploy:
  stage: deploy
  script:
    - scp -r deployment/$CI_COMMIT_REF_NAME/* $DEV_SERVER_USER@$DEV_HOSTNAME:$DEV_SERVER_DIRECTORY/
    - ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can use all variables defined for the GitLab CI (and define your own of course).

Another approach for more complex file can be the use of `rsync`:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
deploy:
  stage: deploy
  script:
    - ...
    - shopt -s dotglob && rsync -a -v -L -r -I --ignore-existing -e ssh deployment/$CI_COMMIT_REF_NAME/* $DEV_SERVER_USER@$DEV_HOSTNAME:$DEV_SERVER_DIRECTORY/ 
    - ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| `rsync` Parameter | Description                                                    |
|-------------------|----------------------------------------------------------------|
| -a                | All, include dot files                                         |
| -v                | Verbose, print out copied files                                |
| -L                | Links, copy tagets of symbolic links                           |
| -r                | Relative paths                                                 |
| -I                | Ignore times, overwrite anything regardless of time attributes |
| --ignore-existing | Ignore existing, don't overwrite anything                      |
| -e                | Execute command - run `rsync`over SSH in this case             |

`shopt -s dotglob` changes the handling of the `*` character (globing) to include dot files as well.
You might want to use something like `{.[!.],}*` instead.

**Note:** Make sure you add the following line to the `before_script:` section if you want to use `rsync`:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- 'which rsync || ( apk add --update rsync )'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Scripts

## `restart-updated.sh`

This script can be used to reduce the afford to deploy a new production instance, for more on it's working look at the quick start guide.

### Arguments and Environment

Neither.

## `reindex.sh`

This script is used by `composer-init.sh` to index the database. It also can be used to trigger an the indexing process 
from outside the application.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker exec -it `docker ps | grep inscriptions | grep app |cut -c-12` /usr/local/bin/reindex.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Arguments and Environment

Neither.

<a name="production_maintenance"></a>

# Maintenance

## User management
For the [complete documentation](https://symfony.com/doc/2.0/bundles/FOSUserBundle/command_line_tools.html) of user management, 
look at the documentation.

### Adding a new user

Just start a shell inside the `app`container and run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
php bin/console inscriptions:create-user -n $USER $MAIL $PASSWORD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Change the password of an existing user

Just start a shell inside the `app`container and run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
php bin/console inscriptions:change-password -n $USER $PASSWORD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Generating linked family members

Just start a shell inside the `app`container and run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
php bin/console inscriptions:link-family-members
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Parameters

| Parameter | Description                                         |
|-----------|-----------------------------------------------------|
| -f        | --force - execute changes (without displaying them) |
| -d        | --dump - show what would be done                    |

## Database management

### Extract an database dump

To extract a database dump you need to know which database is in use, it could be either a database 
container hosted in the docker environment or a externally hosted database.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mysqldump -h $HOST -P $PORT -u $USER -p$PASSWORD $DB_NAME
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Memory management of PHP FPM

There have been some memory related issues. For example reindexing a huge amounts of data (about 100 names with persons) used to 
stop the PHP FPM process with complains about missing memory. At first thought the FOS Elastica extension seem to be the problem 
but now it seems to be clear that this isn't really the case, since the error was fired at the server side (and thus behind FPM).
The configuration for FPM can be found in `$PHP_INI_DIR/../php-fpm.d/www.conf`.

Currently the configuration is changed in the following way:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
pm = ondemand
pm.max_children = 10
pm.process_idle_timeout = 5s
pm.max_requests = 50
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Memory Mangement of Docker

Since `docker-compose` files with versions lager then 3.x only allow memory limits for Docker Swarm deployments 
the version of the `docker-compose` files has been downgraded to version 2.1. Even though it's possible to use a 
sort of backward compatibility (using the `--compatibility` flag) with newer versions of `docker-compose`. The 
current server doesn't support this.

Using this approach it's possible to limit the resource usage of a single container:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# For older Docker versions (docker-compose file format V2)
#    mem_limit: 2G
# For newer 'docker-compose' (1.21.0 or newer) use with --compatibility 
# See https://github.com/docker/compose/pull/5684
#    deploy:
      resources:
        limits:
          memory: 2G  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
