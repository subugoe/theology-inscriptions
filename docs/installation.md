Installation auf einer virtuellen Maschine / einem virtuellen Server
====================================================================

# Einleitung

Dieses Dokument beschreibt die Installation der Anwendung auf einem frisch installierten (virtuellem) Server. Die Installation des Grundsystems wir dabei vorrausgesetzt und ist nicht Teil des Dokuments. Die Beschreibung ist für Ubuntu 20.4 (die aktuelle LTS Version) geschrieben, allerdings kann auch eine andere Linuxdistribution eingesetzt werden. Dabei kann aber der Name des Paketmanagers und die Namen der Softwarepakete anders sein. 
Folgende Basisaufgeaben sind nicht Teil dieser Anleitung:
* Die Netzwerkanbindung
* SSH Zugängen zur Remote Wartung
* Regelmäßige Wartungsarbeiten wie Backups und Sicherheitsupdates des Linux-System.
* DNS Einträge für den server und somit der Anwendung
* Konfiguration von HTTP Verschlüsselung (HTTPS)
* Zugriffsbeschränkungen, z.B. auf Basis der IP Adresse

## Vorraussetzungen

* Ein frisch installierter Linux Server oder eine virtuelle Maschine
  Für das Beispiel wurde ein [Ubuntu 20.4](https://www.osboxes.org/ubuntu-server/) Server Image von OSBoxes in [VirtualBox](https://www.virtualbox.org/) gewählt. 
* Es sollten mindestens ca. 10 GB an freiem Festplattenspeicher und 4 GB Ram zur Verfügung stehen.
* Vor der Installation sollten die Paketquellen aktualisiert werden, damit sichergestellt ist, das die jeweils aktuellste Version installiert wird.

# Installation

Für andere Linux-Distributionen können die Kommandos für die Installation von Softwarepaketen und die Namen der Pakete abweichen. Die Kommandos müssen als Benutzer `root` ausgeführt werden.

## Abhängigkeiten 

### Git

[Git](https://git-scm.com/) ist ein Cleient für die Quellcode Verwaltung, es kann nach der Installation wieder entfernt werden.

```shell
apt-get install -y git
```

### Docker

[Docker](https://www.docker.com/) ist eine Laufzeitumgebung für Anwendungscontainer.

```shell
apt-get install -y docker docker-compose
```

Zur Nutzung von Docker ohne `root`-Rechte kann eigene [Gruppe angelegt werden](https://docs.docker.com/engine/install/linux-postinstall/).

### HAProxy

[HAProxy](http://www.haproxy.org/) ist ein Proxy-Frontend und Load Balancer.

```shell
apt-get install -y haproxy
```

## Anwendung

Nachdem die Abhängigkeiten bereitgestellt wurden, kann die Installation der Anwendung beginnen.

### Quellen beziehen

Zuerst muss ein Arbeitsverzeichnis ausgewählt werden, dies kann entweder im Nutzerverzeichnis des Administrators, in einem neuen Verzeichnis unterhalb von `/opt` oder in einem eigens dafür angelegt Nutzerverzeichnis liegen.

```shell
git clone https://gitlab.gwdg.de/subugoe/theology-inscriptions.git
```

**Alle folgenden Schritte werden in heruntergeladen Verzeichnis ausgeführt**

```shell
cd theology-inscriptions
```

### Konfiguration der Datenbankanbindung

#### Für Testzwecke

Für Testzwecke (also z.B. in einer VirtualBox Umgebung) kann die Datenbank in der Docker-Umgebung betrieben werden. Dafür ist nur eine Anpassung eine zusätzliches Argument beim Start von `docker-compose` notwendig.

#### Datenbank für den Produktivbetrieb

Für den Produktionsbetrieb ist es empfehlenswert eine externe Datebnbank (hier bei der GWDG) empfehlenswert, da damit auch eine erhöhte Verfügbarkeit und regelmäßige Backups verbunden sind

#### Anpassung des Host-Namens

Für den Produktivbetrieb sollte ein DNS Namen für die Anwendung vergeben werden. Dieser muss in der Konfiguration des Web-Servers eingetragen werden. Dies ist für lokalen Testbetrieb nicht notwendig. Die Datei `docker/web/site.prod.conf` muss folgenden Eintrag (mit dem vergebenen DNS Namen) enthalten:

```
server_name inscriptions.sub.uni-goettingen.de;
```

(Beispiel DNS Eintrag für die derzeitige Instanz)

#### Docker Registry Anmeldung

Die Anwendung und ihre Abhängigkeiten werden als sog. Docker Images verteilt. Diese werden vom GitLab der GWDG bereit gehalten, für den Zugriff ist ein Login notwendig.

```shell
docker login -u 'USERNAME' -p 'PASSWORD' docker.gitlab.gwdg.de
```

#### Starten der Anwendung

Nachdem die Betriebsart der Datenbank festgelgt wurde, kann die Anwendung gestartet werden.

**Mit lokaler Datenbank:**

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml -f docker-compose.local-db.yml up -d
```

**Mit externer Datenbank:**

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```

Nachdem die Anwendung gestartet wurde, sorgt die Dcoker-Umgebung dafür, dass sie am Betrieb gehalten wird.

### Automatisches starten der Dienste

Damit die benötigten Dienste auch nach einem Neustart etc. automatisch starten, müssen sie entspreched "markiert" werden.

```shell
systemctl enable haproxy
systemctl enable docker
```

## Aktualisierung der Anwendung

Derzeit finden noch die Abschlussarbeiten an der Anwedung statt, daher kann es notwendig werden, dass diese aktualisiert werden muss. Die folgenden Schritte müssen im oben angelegten Anwendungsverzeichnis durchgeführt werden.

### Anwendung beenden

Der erste Schritt, ist das herunterfahren der Anwendung. Da hierbei integierte Daten mit gelöscht werde, sollte eine Sicherung der Datenbank durchgeführt werden (s.u.) - dies ist im Testbetrieb oder bei der verwendung einer externen Datenbank natürlich nicht notwendig.

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml [ -f docker-compose.local-db.yml ] down -v
```

Das Fragment in den eckigen Klammern (`-f docker-compose.local-db.yml`) muss nur bei verwendung einer lokalenn Datenbank eingefügt werden.

### Quellen aktualisieren 

Im nächsten Schritt müssen die Quellen aktualisiert werden:

```shell
git pull
```

### Container aktualisieren

Nachdem die aktuellen Quellen heruntergeladen wurden, müssen noch die Container aktualisiert werden:

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml pull
```

## Zugriff über das Netzwerk (Browser)

### Konfiguration von HAProxy

HAProxy wir benötigt um externe Abfragen (Port 80 - HTTP und ggf. Port 443 - HTTPS) durch die Anwendung in der Docker-Umgebung abarbeiten zu lassen.

Eine Vorlage für die Datei ist unter `./docker/deployment/prod/haproxy.cfg` gespeichert, diese muss an die bestehende `/etc/haproxy` angehängt werden.

```shell
cat ./docker/deployment/prod/haproxy.cfg >> /etc/haproxy/haproxy.cfg
```

(Dabei muss ggf. `sudo` verwendet werden, je nach dem unter welchem Benutzeraccount die Installation durchgeführt wird)

An die Datei `/etc/haproxy/haproxy.cfg` müssen die folgenden Zeilen angehängt / angepasst werden:

```
frontend http
  bind :80
  default_backend inscriptions

backend inscriptions
  option forwardfor
  server s2 127.0.0.1:8080 maxconn 32
```

Für die Nutzung von HTTPS muss ein weiteres Backend konfiguriert werden - siehe Abschnitt "Nächste Schritte".

## Nächste mögliche Schritte

Die Nutzung von HA Proxy als Frontend-Server ermöglicht die einfache Konfiguration der Web Zugriffe ohne in die Docker-Laufzeitumgebung eingreifen zu müssen.

### Konfiguration von HTTPS Zugriffen

Der Zugriff kann [mittels Zertifikaten verschlüsselt](https://www.haproxy.com/blog/haproxy-ssl-termination/) erfolgen. Dabei sollte sichergestellt werden, dass [unverschlüsselte Anfragen immer umgeleitet](https://www.haproxy.com/blog/haproxy-ssl-termination/#redirecting-from-http-to-https) werden.

Das notewndige Zertifikat kann bei der [GWDG beantragt](https://docs.gwdg.de/doku.php?id=de%3Aservices%3Ait_security%3Apki%3Auniras) werden.

### Zugriffsbeschränkungen

Mittels HAProxy können auch Zugriffsbeschränkungen, z.B. [auf Basis der IP Adresse](https://www.haproxy.com/blog/introduction-to-haproxy-acls/) realisiert werden.

# Wartungsaufgaben

## Sicherung der Datenbank

**Nur für den Produktionsbetrieb (mit externer Datenbank)**

Je nach Art des Betriebs (siehe oben: Test- oder Produktivbetrieb) sollte der Inhalt der Datenbank regelmäßig gesichert werden. Falls die Datenbank bei einem externen Anbieter der GWDG abgelegt wurde, kann es sein, das dieser Anbieter auch die Datensicherung übernimmt.

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml -f docker-compose.local-db.yml exec -T app ./symfony_mysqldump.sh
```

Die Ausgabe des Datenbak Dumps erfolgt auf der Konsole und kann in eine Datei umgeleitet werden:

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml -f docker-compose.local-db.yml exec -T app ./symfony_mysqldump.sh > dump.sql
```

## Verwaltung von Nutzern

Zu den regelmäßigen Wartungsaufgaben gehört auch die Verwaltung von Nutzern:

### Anlegen neuer Nutzen

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml -f docker-compose.local-db.yml exec -T app ./bin/console inscriptions:create-user  admin copidocker@sub.uni-goettingen.de admin
```

### Passwörter ändern

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml -f docker-compose.local-db.yml exec -T app ./bin/console inscriptions:change-password admin password
```

### Nutzer löschen

```shell
docker-compose -f docker-compose.yml -f docker-compose.prod.yml -f docker-compose.local-db.yml exec -T app ./bin/console inscriptions:delete-user admin


## Einfache Funktionsprüfungen

Zur Überprüfung der Funktion der Anwendungen können verschiedene Tests ausgeführt werden. Diese können in Teil auch automatisiert werden, entweder via Cron, mittels GitLab oder auch durch eine Monitoring Software. Als Beipiel kommt hier jeweils die URL der aktuellen Entwickliungsinstanz (`https://dev.inscriptions.sub.uni-goettingen.de/`) zum Einsatz.

### Automatisierbar

Zur Automatisierung werden HTTP Abfragen mit Hilfe des Kommandozeilen HTTP-Client [`curl`](https://curl.se/) gestellt und der Rückgabewert ausgewertet.

Beim Aufruf der Startseite werden drei der vier notwendigen Komponeten benutzt: Die Datenbank, der Webserver und die PHP Anwendung. Daher reicht eine Abfrage um ihre Funkttion und Kommunikation untereinander zu prüfen:

```shell
curl --silent --show-error --insecure --fail "https://dev.inscriptions.sub.uni-goettingen.de/"
```

Diese Abfrage schlägt auch bei SSL Problemen fehl:

```shell
curl --silent --show-error --fail "https://dev.inscriptions.sub.uni-goettingen.de/"
```

### Manuell

Die URL der Startseite (Beipspiel `https://dev.inscriptions.sub.uni-goettingen.de/`) kann auch einfach im Browser geöffnet werden. Der Browser berichtet auch über eventuelle SSL Probleme.

## Informationen zu Sicherheitsupdates

(Ohne MySQL, da der Betrieb extern (GWDG) empfohlen wird)

* [NGinX](http://nginx.org/en/security_advisories.html), [CVE Einträge](https://www.cvedetails.com/vulnerability-list/vendor_id-10048/product_id-17956/Nginx-Nginx.html)
* [PHP](https://www.cvedetails.com/vulnerability-list/vendor_id-74/product_id-128/PHP-PHP.html)
* [Docker](https://www.cvedetails.com/vulnerability-list/vendor_id-13534/product_id-28125/Docker-Docker.html)
* [PHP Bibliotheken](https://github.com/advisories?query=ecosystem%3Acomposer)