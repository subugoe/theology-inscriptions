GitLab CI
=========

This document describes the existing pre build images and their build process.
Deployment of the production instance is documented in the the [production documentation](./production.md#production_gitlab)

# Pre build Docker images

This section describes the existing Docker images including the different tags.

**Note:** Not all this Images are available in the [public registry](https://gitlab.gwdg.de/subugoe/theology-inscriptions/container_registry) yet! 
This page only describes what has been configured, not what has been build yet!

| Image                                                               | `master` Tag | `dev` Tag | `latest` Tag | Dependens on                                        | Directory and Dockerfile            | Notes                                                                                                             |
|---------------------------------------------------------------------|--------------|-----------|--------------|-----------------------------------------------------|-------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| docker.gitlab.gwdg.de/subugoe/theology-inscriptions/search          | No           | No        | Yes          | docker.elastic.co/elasticsearch/elasticsearch:6.8.6 | `docker/web-maintenance/Dockerfile` | Just `latest` since no big changes are expected                                                                   |
| docker.gitlab.gwdg.de/subugoe/theology-inscriptions                 | Yes          | Yes       | Yes          | php:7.4.0-fpm-alpine3.10                            | `docker/app/Dockerfile`             | Default production image, `latest` ist an alias for `master`. Other tags wil be given if other branches are build |
| docker.gitlab.gwdg.de/subugoe/theology-inscriptions/dev             | Yes          | Yes       | No           | docker.gitlab.gwdg.de/subugoe/theology-inscriptions | `docker/app/Dockerfile.dev`         | Default development image. Other tags wil be given if other branches are build                                    |

**Note:** If you still find references to to other containers, either in a file or in the documentation, please create an [issue](https://gitlab.gwdg.de/subugoe/theology-inscriptions/issues/new).

# Build process

The build process is different for each branch, since they have different deployment targets and usage scenarios.

Each build image will be tagged with the branch name, images build from the `master` branch will also be tagged as `latest`.

## Common build steps

To enable two build steps and to monitor the success of a deployment there are additional steps to the [default ones](https://docs.gitlab.com/ee/ci/yaml/#stages) 
provided by GitLab. The following table lists them.

| Name of the stage | Description                                                |
|-------------------|------------------------------------------------------------|
| build_base        | Building of the Docker base images                         |
| pre-build-test    | Static code analysis before building the final `app` image |
| post-deploy-test  | Checking of deployed images                                |

### TODO

-  Newer Gitlab versions allow `.pre` and `.post` stages, these can be used to reduce the user defined stages.

### `build_base` stage

This stage builds the base image.

### `build_search`

This target build the Elasticsearch image containing geo data.

### `pre-build-test` stage

This stage runs several static code checkers before building the final `app` image. If a certain degree of coding quality isn't 
meet, the build wil break at this stage. If you don't like it: Don't complain and deliver quality!

The checks that are performet by this target are documented in the [advanced development documentation](./advanced-development.md).

## `dev` branch

The `dev` branch is used to be deployed on the [`dev` testing server](https://dev.inscriptions.sub.uni-goettingen.de/).
It's also used for development in the deployment infrastructure.

### `build-dev`

This target builds the development version of the `app` image. This includes several tools like `npm` and `composer` and all 
build related dependencies of the base operating system. Everything that had been cleaned from the base image is reinstalled.

### Deployment (``deploy-dev``)

This target deploys the application on the [staging server](https://dev.inscriptions.sub.uni-goettingen.de/). For deployment 
the script [`deploy.sh`](../docker/app/scripts/deploy.sh) is used. Note that during deployment another `docker-compose` file 
is added. See `docker-compose.dev-derver.xml` in the [Docker documentation](./docker.md).

### `post-deploy-test` stage

This target waits for a short amount of time and the uses `wget`to determinate if the login window is shown. If this doesn't 
happen it will assume thatt the deployment has failed.

## `master` branch

The `master` branch is used for the deployment of the [production instance](https://inscriptions.sub.uni-goettingen.de/) only.

### `tag-master`

This target adds the `latest` tag to successfully build images of the `master` branch.

### Deployment (`deploy-master`)

The deployment of the production instance is semi automatic: The CI server only copies the required artifacts to the server.
But you need to do the restart of the application yourself. There is a script `restarted-updated.sh` can be used to automate 
the process, it's documented in [`production.md`](./production.md).

## Other branches

For all other branches just the base build is build, afterwards the checks of the `app` image are executed 
and a `dev` image is build and tagged with the name of the branch.

