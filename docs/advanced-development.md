Advanced Development
====================

This document provides some hints for advanced development techniques.

# Changes to third party libraries / unreleased dependencies 

Some requirements could be only fulfilled by changing existing packages , extending their provided implementation at runtime or by using currently unreleased features. Some of those changes might be also provided upstream.

## Direct static code changes (patches) 

### [FOSElastica](https://github.com/FriendsOfSymfony/FOSElasticaBundle)

#### `_meta` fields
Elasticsearch provides mechanisms to store addition [metadata](https://www.elastic.co/guide/en/elasticsearch/reference/6.8/mapping-meta-field.html) on indices. For version 6.8 this field is called `_meta`, in newer version this field has been renamed. This can either be used to pass information between different clients or to control the behaviour of a certain client.

FOSElastica doesn't expose this field to the programmer, probably because they use it on their own, and it's use is subject of some constrains imposed by Elasticsearch. The patch `patches/foselastica.patch` allows one to use this field.

#### Custom attributes in mappings

To be able to submit context specific information to our own [`Transformer`](http://obtao.com/blog/2014/05/advanced-indexing-with-elasticsearch-foselasticabundle/) without having those settings to Elasticsearch a patch is required to filter those out when generating the mapping. It filters properties starting with `_collection`.

See `patches/MappingBuilder.patch `. 

More on the `Transformer` in the 'Extended provided implementation at runtime' section.

### [composer-patches](https://github.com/cweagans/composer-patches)

Yes it's true, the patcher itself has a bug that needs to be patched out. That's basically a typical beginner error: It relies on external programs that might exist in the target environment, without even mentioning it in any documentation. Please design software in a fashion that it works on it's without any external dependencies except the runtime system (Posix OS, interpreter and build system - yes that doesn't include `git` or `bash`). If more is needed, it's the responsibility of the programmer to make sure these dependencies are installed automatically. Anyways, the problem is known, there is a [pull request](https://github.com/cweagans/composer-patches/issues/295) (issue [#294](https://github.com/cweagans/composer-patches/issues/294)), which isn't merged by now.

We do it on our own during the Docker build. Since we can't rely on the patcher here for obvious reasons, we need to apply the patch before composer runs the `install` task:
 * Run the `composer` prefetcher ([`hirak/prestissimo`](https://github.com/hirak/prestissimo)) to have the needed dependencies in the local cache (`/root/.composer/cache/files`)
 * Unpack `composer-patches`
 * Download the pull request as patch from GitHub
 * Apply it
 * Update the archive of the dependecy
 * Delete unneded files
 
 As a result we have a `composer-patches` package which doesn't need any local `.git` directories to work. This should be quite common in docker workflows to prevent short-circuiting controlled development workflows by committing directly from a container.

## Extended provided implementation at runtime

### [FOSElastica](https://github.com/FriendsOfSymfony/FOSElasticaBundle)

#### Collection to index mapping

The provided ModelTransformer can't handle PHP `Collection`'s of properties very well. If entity A references an entity B (with property M, which is a list) multiple times (1:n) but in the index this should be normalised in a form that the index containing objects of entity A should only include the property M of B without any duplicates, the existing Transformer will already fail if it detects that the relation returns a `Collection`. The new Transformer supports three additional parameters in `config/packages/fos_elastica.yaml`:
* `_collection_path`: The 'path' to the entity object `Collection` relative from the current position.
* `_collection_property`: The property of the entity objects to be collected
* `_collection_callback` (optional): A callback to modify the results fields as an array, `key`, `nameOfMethod`. Example `['geonames' , 'queryPlace']`, you need to define the callback as symfony services in `config/services.yaml`.  

Example definition of callback in `config/services.yaml`:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    application_search.service.generic_transformer:
        class: App\Util\Transformer\CollectionMapper
        calls:
            - [setPropertyAccessor, ['@fos_elastica.property_accessor']]
            - [addCallback, ['geonames', '@application_search.service.geonames_enrich_transformer']]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
See `src/Util/Transformer/CollectionMapper.php`

#### Enrichment with geo coordinates

The old map search didn't scale very well, since for nearly (there was a cache at request scope) each place name a search request wa fired. For better scalability the place coordinates are now added at indexing time and aren't persisted in the database. This way there is no need to change the data model and new place data can be leveraged at any time by reindexing. 

See: `src/Util/Transformer/GeonamesEnrichTransformer.php`

### [Elastica](https://elastica.io/)

This API has some simple design flaws, it's not symmetrical. There are setters missing, to circumvent this shortcoming for the `Elastica\Search` class there is the class `App\Util\Search\ElasticMissingMethodFixedSearch` which extends the first.

See `src/Util/Search/Elastic/MissingMethodFixedSearch.php`.

## Unreleased dependencies

### [FOSElastica](https://github.com/FriendsOfSymfony/FOSElasticaBundle)

To be able to use the delete index feature and to provide an upgrade path to Symfony 5 and elasticsearch 7 later on, the current master of FOSElastica is used. This imposes a certain risk of introducing breaking changes or bugs introduced by unreleased code. 

### [antlr/antlr-php-runtime](https://github.com/antlr/antlr-php-runtime)

The currently released version (0.3) was released before the generation target for PHP was introduced with ANTLR 4.8 and emits a warning that it expects ANTLR 4.7 on every invocation. Using the current master fixes this.

# Code generation

The QueryParse (`src/Util/Query`) is generated. This means that every change to it and it's dependencies won't ever get deployed. In fact 
these are even ignored by `git` (`.gitignore`) and `docker` (`.dockerignore`). The sources are generated by [ANTLR](https://www.antlr.org/).
You can also find instructions there how to setup the tool on you local machine. You may also run it inside the `dev` image, it's located in `/opt/antlr`.
If you need to change the edit the grammar file `src/Util/Query/Query.g4` and rerun the generator:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
antlr -Dlanguage=PHP src/Util/Search/Query/Query.g4 -package 'App\Util\Search\Query'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Use `java -jar /opt/antlr/antlr-4.8-complete.jar` as prefix inside the `dev` image.

There is a [IntelliJ Plugin](https://plugins.jetbrains.com/plugin/7358-antlr-v4-grammar-plugin) to visualise the ANTLR rules, which is a great help in debugging your grammar.

# Code analysis tools

**Note:** You need to have the PHP version as required by the Composer dependencies (currently PHP 7.4) on path to run these.

## Using PHP CS

PHP CS is the ['PHP Coding Standards Fixer'](https://cs.symfony.com/). The provided configuration 
(`.php_cs`) uses the Symfony style with a few changes. It's also aware of Doctrine annotations.

The configuration is in `.php_cs`. It's used by the test phase of the CI and is pulled in by the [php-test](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/docker/php-test) image.


To see the violations per for each file (This doesn't print the proposed changes):

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vendor/bin/php-cs-fixer fix --config=.php_cs -v --dry-run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To see the actually proposed changes run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vendor/bin/php-cs-fixer fix --config=.php_cs -v --diff
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To fix a single file run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vendor/bin/php-cs-fixer fix --config=.php_cs -v path/to/file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Proposed workflow

For start you might do a `--dry-run` in the root directory of the repository (see above). Afterwards 
you might either review the proposed changes by adding `--diff` or break the changes down into smaller 
chunks like sub directories or even single files. After you're sure that the proposals suit your needs, 
run the command again without the `--dry-run` parameter and commit the changes made. If you don't agree with 
proposed changes, you should <u>always</u> change the rule file and start over for the whole tree to keep the 
results consistent.

### Differences to default rules

#### Use spaces around concat operator

To enhance readability use a space before and after the concat operator `.`. Rule:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'concat_space' => ['spacing' => 'one']
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Disable Yoda style

Stupid ideas deactivate, you must. Rule:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'yoda_style' => false
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Keep superfluous comment tags

We should be happy people try to comment their work, don't disappoint them! Rule:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'no_superfluous_phpdoc_tags' => false
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Don't insert trailing comma in multiline arrays

This makes it hard to read when the list of array elements is over.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'trailing_comma_in_multiline_array' => false
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Sane handling of whitespace

To enhance readabilty the following additional whitespace related rules are also active:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
'no_blank_lines_after_class_opening' => true,
'no_trailing_whitespace' => true,
'no_whitespace_in_blank_line' => true,
'no_extra_blank_lines' => true,
'array_indentation' => true,
'blank_line_before_statement' => true,
'braces' => true,
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Using PHPStan

[PHPStan](https://github.com/phpstan/phpstan) can be be used to check for coding errors, it's basically a static code checker for PHP. 
But it does more, like checking comments. It's also executed during the testing stage of GitLab. 
we try to comply with level 5. 

The configuration is in `phpstan.neon`. It's used by the test phase of the CI and is pulled in by the [php-test](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/docker/php-test) image.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vendor/bin/phpstan analyse --level 5 src
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Using Psalm

[Psalm](https://github.com/vimeo/psalm) is another PHP code checker. It has some strengths when it comes to type and null 
checking and is able to fixes some problems.

The configuration is in `psalm.xml`. It's used by the test phase of the CI and is pulled in by the [php-test](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/docker/php-test) image.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vendor/bin/psalm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Psalm Plugins

There are two plugins that are targeting the used frameworks, you need to enable them before they are used:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
psalm-plugin enable weirdan/doctrine-psalm-plugin
psalm-plugin enable psalm/plugin-symfony
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Using Rector

[Rector]() is a tool to rewrite existing PHP code. It can be used to change existing code to use new language features. 
It also rewrites code following several anti-patterns.

The configuration is in `rector.yaml`. Rector isn't used by the test phase of the CI (yet). It's pulled in by the [php-test](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/docker/php-test) image.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$PHP vendor/bin/rector process src --dry-run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Using ESLint

[ESLint](https://eslint.org/) is a ECMA Script linter, it also can fix several error it's reporting

The configuration is in `.eslintrc.js`. Rector isn't used by the test phase of the CI (yet). It's pulled in by the [frontend-lint](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/docker/frontend-lint) image.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
npx eslint -c .eslintrc.js assets/js/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Using stylelint

[stylelint](https://stylelint.io/) is a linter for CSS / SCSS files.

The configuration is in `.stylelintrc.json `. Rector isn't used by the test phase of the CI (yet). It's pulled in by the [frontend-lint](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/docker/frontend-lint) image.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
npx stylelint assets/css/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Using Branches

The CI infrastructure has been enhanced to build every existing new branch. This way it's possible to benefit from 
it for every branch. Basically this has two implications: You don't need to abuse the dev branch to see if the Docker 
build passes (remember, it's meant to be deployed). You don't have to give up this benefit by using other branches.

So if you plan larger changes or just can't estimate how log it will take to complete the change, create a new branch.

Create a new branch (named `new_branch`) and switch to it:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git checkout -b new_branch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Push the new brach (only need once with parameters):

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git push --set-upstream origin new_branch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After your changes are done and the build passes, merge the `new_branch` (feature branch) branch 
with the `dev` branch, resolve merge conflicts, if any and push the result:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git checkout dev
git merge new_branch
git push
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Things to consider

-  Only merge your branch with `dev` if you also updated the documentation for the changes of the branch to be merged.

-  Make sure to merge changes from `master` to `dev` if you edit `master` directly. 

# Code analysis during build

All above mentioned tools are available during the build process as part of the [SUB Docker SDK](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/). 
This project is currently almost undocumented. Check back there regularly to learn more about the provided functionality.

## Check before commit

You should run at least PHPstan and PHP CS before you commit since otherwise the build might break.

<a name="githook" />

### Using a Git Hook

You can also use a [Git hook](https://git-scm.com/docs/githooks) for this check, one is provided as a part of this repository in the `.githooks` directory.
Just add the directory to your local repository:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git config --add core.hooksPath .githooks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
