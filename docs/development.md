Development
===========

<a name="development"></a>

# Development / debugging

## Prerequisites (configuration and software)

The following software is needed, use the software management of your OS to get these:

-  [Git](https://git-scm.com/)

-  [Docker](https://www.docker.com/)

To run the PHP and JavaScript development tools on your host system (not from within the Docker container) you also need

-  [PHP](https://www.php.net/) at least version 7.4

-  [NPM](https://npmjs.com/) tested node version 18

To work with Geo data you also need:

-  [`jq`](https://stedolan.github.io/jq/)

## Known Issues

-  If started with less then 4 GB and with Kibana enabled, the Elasticsearch container can fail to start

-  If you have to less memory reserved for Docker (at least 4 GB) strange things might happen: The entrypoint 
   of the `app` image can take ages to complete, Elasticsearch might fail to start.

### Memory limits

Elasticsearch needs a high amount of memory, especially in DEBUG mode. since the project doesn't store much data in the index, it should be safe to reduce the memory limit of Elasticsearch.
Otherwise one needs to increase the memory limit of docker itself, since Elasticsearch will quit without a proper error message if the memory limit is exhausted. 
See `ES_JAVA_OPTS`, make sure to put the whole line in quotes if there is whatespace in it, escaping (`\`) won't work.
**Note:** Make sure Docker has at least 3 GB for the Debug / Dev Mode since Kibana also needs it's share of memory!

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"ES_JAVA_OPTS=-Xms750m -Xmx750m"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additionally memory limits / settings can apply on Linux hosts, [see below](./troubleshoot.md#note_linux).

## Settings in development mode

-   `APP_ENV` is set to `dev`

-   Elasticsearch starts in `debug` mode

-   Elasticsearch is exposed on port `9200`

-   The database is exposed on port `3306`

-   For the PHP (`app`) and Nginx (`web`) containers `/var/www/html` is bound to `.`

-   Environment from `.env.dev` is used

## Accessing the web application

Point your browser to <http://localhost:8008/>

## Reseting changed files in development mode

In development mode the local directory (`.`) itself is mounted into the container, the 
setup will alter some configuration files. The following command can be used to reset to 
the default from the repository:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git checkout config/packages/doctrine.yaml config/packages/fos_elastica.yaml public/build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Rebuilding the `app` container

Rebuilding the `app` container isn't needed anymore since now all local changes are available inside the 
container when started in `dev` mode. If you know what you're doing follow the steps in the [Docker guide](./docker.md).

## Clean up unused volumes:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker volume prune
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Optional Kibana Container

An optional Kibana Container is provided to debug and explore the search index, this feature is currently untested!

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.dev.yml -f docker-compose.kibana.yml up -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:** This container needs ages to start up, make sure you have assigned enough spare memory! 
**Note:** Kibana isn't very stable and doesn't provide clear error messages, don't rely on it! 

## Additional environment settings

These are needed for Kibana, don't change them if you don't plan to work with Kibana on a deployed instance. 

| Name                        | Value                                                                   | Description                                      | File                      |
|-----------------------------|-------------------------------------------------------------------------|--------------------------------------------------|---------------------------|
| http.port                   | 9200                                                                    | The Port to run on                               | docker-compose.dev.yml    |
| http.cors.allow-origin      | http://localhost:1358,http://127.0.0.1:1358                             | From which URL are cross site requests allowed   | docker-compose.dev.yml    |
| http.cors.enabled           | true                                                                    | Are cross site requests allowed?                 | docker-compose.dev.yml    |
| http.cors.allow-headers     | X-Requested-With,X-Auth-Token,Content-Type,Content-Length,Authorization | Which HTTP headers are allowed from other sites? | docker-compose.dev.yml    |
| http.cors.allow-credentials | true                                                                    | Is a authentification allowed from other sites   | docker-compose.dev.yml    |
| bootstrap.memory_lock       | true                                                                    | Special memory settings                          | docker-compose.kibana.yml |

<a name="docker"></a>

## Working with the containers on Docker level

### Checking if everything is running

Run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$ docker ps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To get a listing:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                               NAMES
b755d244b545        inscriptions_app    "docker-php-entrypoi…"   About a minute ago   Up 48 seconds       9000/tcp                            inscriptions_app_1
e09ca35752c6        mysql:5        "docker-entrypoint.s…"   About a minute ago   Up 59 seconds       0.0.0.0:3306->3306/tcp, 33060/tcp   inscriptions_database_1
351f71fbdee3        elasticsearch:5     "/docker-entrypoint.…"   About a minute ago   Up 59 seconds       9200/tcp, 9300/tcp                  inscriptions_search_1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You need to have four running containers for the inscriptions project.

### Accessing the web frontends with a browser

| Application              | Port / Link                   |
|--------------------------|-------------------------------|
| Inscriptions Frontend    | [8008](http://127.0.0.1:8008) |
| Web Maintenance Frontend | [8009](http://127.0.0.1:8009) |
| Kibana                   | [5601](http://127.0.0.1:5601) |

### Running a shell in the one of the used containers

Run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker exec -it `docker ps | grep inscriptions | grep app |cut -c-12` /bin/bash
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Exchange the name of the container (`database` or `search` instead of `app`) to
run a shell in the respective container.

### Reload `nginx` after config changs

If you change the configuration of `nginx` you can reload it's configuration without restarting the container:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose exec web nginx -s reload
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<a name="technical"></a>

# Technical background

This section describes the inner workings of the setup script and the different environment variables.

## Steps to set up the application container (done by the script)

**Note:** This section  only exist for historical purposes, look at `composer-init.sh` to understand the initialization process.

The application container needs to set up after the build. It's required that
dependent services like the database are running as well. These steps create the
database schema.

After checking the arguments the `APP_ENV` is checked, if it's set to `dev` the PHP configuration is changes and `encore` is rerun.

The following steps of the `composer.json` file need to be executed:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"cache:clear": "symfony-cmd",
"assets:install --symlink --relative %PUBLIC_DIR%": "symfony-cmd"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Just rerun the `composer` command in a shell in the started App container (see
above) like this (should be faster since dependencies already exist):

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
php composer.phar install --prefer-dist --no-progress --no-suggest --optimize-autoloader --classmap-authoritative  --no-interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since this can take some time it might be better to check the progress of the
initialization:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
COMPOSER_PROCESS_TIMEOUT=2000 php composer.phar install --prefer-dist --verbose --no-suggest --optimize-autoloader --classmap-authoritative --no-interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The next two steps are to initialize the required database tables and the index:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
php bin/console doctrine:schema:update -f
php bin/console fos:elastica:populate
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To get some of the required entities (like languages and regions) there is a
step to import those via a SQL script. The used environment variables are set by
the Docker `.env` file.

**Note:** This is just an example, `mysql` isn't available in the `app`
container, the real script uses a complicated `php` one liner to import SQL into
the database.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mysql -h "$MYSQL_HOST" -u "$MYSQL_USER" -p "MYSQL_PASSWORD" "$MYSQL_DATABASE" < "$SQL"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `php` call:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
(new PDO('mysql:host=' . getenv('MYSQL_HOST') . ';dbname=' . getenv('MYSQL_DATABASE'), getenv('MYSQL_USER'), getenv('MYSQL_PASSWORD')))->exec(file_get_contents(getenv('SQL')));
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The final step would be the creation of a admin user:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
php bin/console fos:user:create -n $USER $MAIL $PASSWORD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(you can admit the arguments ant the option `-n` to run a interactive session)

From the host you can use:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker exec -it `docker ps | grep inscriptions | grep app |cut -c-12` php bin/console fos:user:create -n $USER $MAIL $PASSWORD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These steps are done by calling the `composer-init.sh` script (see section
"Setting up the application container")

## Configuring Docker and docker-compose

### .env file

#### Database settings

| Name                       | Default value                       | Description                                 |
|----------------------------|-------------------------------------|---------------------------------------------|
| MYSQL_USER                 | inscriptions                        | Database user name                          |
| MYSQL_PASSWORD             | LETMEIN                             | Database password                           |
| MYSQL_DATABASE             | symfony4                            | Database to use                             |
| MYSQL_HOST                 | database                            | Database host (docker name)                 |
| MYSQL_PORT                 | 3306                                | Database port                               |
| MYSQL_RANDOM_ROOT_PASSWORD | true                                | To be used to disable database root access  |

The database settings are used by the `app` container and by the `database`
container.

#### Elasticsearch settings

| Name                       | Default value                       | Description                                 |
|----------------------------|-------------------------------------|---------------------------------------------|
| EL_HOST                    | search                              | Search engine host (docker name)            |
| EL_PORT                    | 9200                                | Search engine port                          |

#### Other settings

| Name                       | Default value                                                                     | Description                                                   |
|----------------------------|-----------------------------------------------------------------------------------|---------------------------------------------------------------|
| APP_ENV                    | prod                                                                              | Symphony mode                                                 |
| DATABASE_URL               | mysql://\$MYSQL_USER:\$MYSQL_PASSWORD\@\$MYSQL_HOST:\$MYSQL_PORT/\$MYSQL_DATABASE | Database connection URI, composed of other values (not used!) |
| SQL                        | /var/tmp/init.sql                                                                 | Initial SQL containing languages, regions etc.                |

#### Optional settings

| Name                       | Default value                       | Description                                                              |
|----------------------------|-------------------------------------|--------------------------------------------------------------------------|
| ES_JAVA_OPTS               | None for production environment     | Java options for Elasticsearch, use the to increase memory limits.      |
| ES_STARTUP_SLEEP_TIME      | 120                                 | Needed to wait for Elasticsearch to come up, increase on slow machines. |

### Build arguments

The app container supports some arguments to set up the initial admin user

| Name                       | Default value                       | Description                  |
|----------------------------|-------------------------------------|------------------------------|
| USER                       | admin                               | User name of initial user    |
| MAIL                       | copidocker\@sub.uni-goettingen.de   | Mail address of initial user |
| PASSWORD                   |                                     | Password of initial user     |

Note that the admin user won't be created if no password is provided!

## Debugging the `web` container configuration

If you need to let Nginx pickup configuration changes without tearing down the container, use this: 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker exec -it `docker ps | grep inscriptions | grep web_ |cut -c-12` nginx -s reload
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Example data

In development mode a set of example data is provided using the dump in `docker/app/sql/init.sql`. 
It's imported on startup by `docker/app/entrypoint.d/composer-init.sh` by running:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
php bin/console doctrine:database:import -v -n "$SQL"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you need to have a empty database you can use this command with one of the following files to truncate the database.

| File name and location          | Purpose                                                                 |
|---------------------------------|-------------------------------------------------------------------------|
| docker/app/sql/truncate-all.sql | Truncates all tables, including those, that can't be edited by the user |
| docker/app/sql/truncate.sql     | Truncates all tables that can be edited by the user                     |

# Quick mode (for local presentations)

The quick mode can be used to have a demonstration system ready fast. It used to be a different setup option 
but now just use the local development for this purpose:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<a name="commit" />

# Commiting your changes

If you plan to commit your changes be aware of the fact that the CI does some checks regarding possible bugs and code quality.
Run the following command to see if your changes can pass them:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.githooks/pre-commit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can add this script as `git-hook`, see the [Advanced Development guide](./advanced-development.md#githook)
