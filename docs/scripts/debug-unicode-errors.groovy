@Grapes(
    @Grab(group='commons-codec', module='commons-codec', version='1.13')
)

import static groovy.json.JsonOutput.*

import javax.xml.bind.DatatypeConverter

import java.nio.ByteBuffer
import java.nio.CharBuffer
import java.nio.charset.CharacterCodingException
import java.nio.charset.Charset
import java.nio.charset.CharsetDecoder
import java.nio.charset.CodingErrorAction

import org.apache.commons.codec.binary.Hex

def decode (byte[] bytes, charset = 'UTF-8') {
    CharBuffer result
    try {
        CharsetDecoder decoder = Charset.forName(charset).newDecoder()
        decoder.onUnmappableCharacter(CodingErrorAction.REPORT)
        decoder.onMalformedInput(CodingErrorAction.IGNORE)
        result = decoder.decode(ByteBuffer.wrap(bytes))
    } catch (CharacterCodingException ex) {
        println ex.getMessage()
    }
    return result
}

def toBinary(byte[] bytes) {
    def result = new StringBuffer()
    byteList = Arrays.asList(bytes)
    byteList.each {
        result.append(String.format("%8s", Integer.toBinaryString(it & 0xFF)).replace(' ', '0'))
        result.append('|')
    }
    return result.toString()
}

def hex1 = ['c3', '8e', 'e2', '80', '9c', 'c3', 'a1', 'c2', 'bc', 'c2', '8f', 'c3', 'a1', 'c2', 'bc', 'c2']
def hex2 = ['8f', 'c3', 'a1', 'c2', 'bc', 'c2', 'b8', 'c3', 'a1', 'c2', 'bc', 'c2', 'b9', 'c3', '8f', 'c2']
def hex3 = ['81']
def hexString = hex1.join('') + hex2.join('') + hex3.join('')
byte[] bytes = DatatypeConverter.parseHexBinary(hexString)

//def bytesArray = new ArrayList<Byte> ([(byte) 0xc3, (byte) 0x8e, (byte) 0xe2])

def result = decode(bytes, 'UTF-8')
println toBinary(bytes)
println result
println prettyPrint(toJson(result.array()))
