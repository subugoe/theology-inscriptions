Troubleshooting Guide
=====================

This section contains solutions for common problems.

<a name="note_composer"></a>

## Composer Timeout

One can extend the timeout for composer using a environment variable

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
COMPOSER_PROCESS_TIMEOUT=2000
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<a name="note_linux"></a>

## ElasticSearch virtual memory on host system

On Linux host systems Elasticsearch can fail to start, in the log for it's container the following error can occur: 
`max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]`

To solve this problem you might want to set the max virtual memory. Root permissions on host system needed.
To solve the problem permanently look at the [Elasticsearch documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sudo sysctl -w vm.max_map_count=262144
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<a name="note_sass"></a>

## `node-sass`fails to run

Node modules may fail in local `dev` mode, when `./node_modules` directory ist mounted from the local development tree. 
This message indicates this error:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Found bindings for the following environments:
  - OS X 64-bit with Node.js 10.x
  - Linux 64-bit with Node.js 10.x
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fix:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rm -rf node_modules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In newer versions `./node_modules` is mounted to an internal volume to get pass this.

**Note:** The internal volume maks it impossible to use `npm` to install new modules from out side the Docker 
environment, consider changing `docker/app/Dockerfile.dev`.

<a name="note_autoloader"></a>

## Autoloader missing

If you get complains of either a missing autoloader or got the feeling that PHP can't find either existing or newly created files 
it's probably related to the so called autoloader. It can help to recreate it from scratch:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
composer.phar dump-autoload --no-scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<a name="note_docker-buildkit"></a>

## Getting Docker output when using BuildKit

To git the output of `docker build` while using BuildKit you need to pass the following environment variable:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BUILDKIT_PROGRESS=plain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
