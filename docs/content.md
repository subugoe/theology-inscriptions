Editing Contents
================

Currently it's possible to edit the [help](https://dev.inscriptions.sub.uni-goettingen.de/help) page using the GitLab 
WebIDE. All you need to do is open [templates/help/index.markdown.twig](https://gitlab.gwdg.de/subugoe/theology-inscriptions/blob/dev/templates/help/index.markdown.twig) 
in your browser (make sure to edit the `dev` branch, to [review your changes on the staging server](https://dev.inscriptions.sub.uni-goettingen.de/help).

There are several Markdown documentations available on the web, mak sure you get a quite basic one 
like [this](https://daringfireball.net/projects/markdown/syntax), since special flavours of Markdown aren't supported.

# Links and anchors

One can add anchors as HTML like `<a name="introduction"></a>`. Currently two anchors are expected to exist:

| Name of anchor   | Purpose                       |
|------------------|-------------------------------|
| help_application | Link from EasyAdmin list view |
| help_search      | Link from search page         |

# Images

It should also be possible to include images, but this is currently untested and undocumented.