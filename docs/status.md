Status and TODO
===============

# Status of the code base

## Done

-   GitLab builds base images ✓

   -  Pre build images are used by default ✓

-   Web access is working ✓

-   Database connection is working ✓

-   Search engine gets initialized ✓

   -   Searching works, including all entities and highlighting ✓
   
   -   Experimental [Geo interface implemented](./geodata.md) ✓

   -   Statistics ✓

   -   Search nested entities ✓

-   Admin user can be created from init script ✓

-   Demo data imported via SQL dump ✓

-   Production mode including cleanup ✓

-   Development mode has been expanded and is currently untested. One base image is now for `dev`and `prod` mode, no Env variable dependency anymore.

-   Upgrade to Symfony 4.4 and EasyAdmin 2.x ✓
    
   -   Deprecated dependencies have been removed ✓
   
-   Many possible PHP errors fixed and warnings removed ✓

   -   Included several static code checkers ✓
   
-   Documentation updated: Include [Docker](./docker.md), [GitLab](./gitlab.md), [deployment](./production.md), [static code checkers](./advanced-development.md)

-   More experimental features

   -   Drag and Drop for family members ✓


## TODO

See [GitLab Issues](https://gitlab.gwdg.de/subugoe/theology-inscriptions/issues).

-   The most important issue is related to filtering the `select2` fields [#60](https://gitlab.gwdg.de/subugoe/theology-inscriptions/issues/60)

-   More experimental features

   -   Draft a portal

# Status of the documentation

## Done 

 -  [`advanced-development.md`](./advanced-development.md)

 -  [`development.md`](./development.md)

 -  [`docker.md`](./docker.md)

 -  [`geodata.md`](./geodata.md)

 -  [`gitlab.md`](./gitlab.md)

 -  [`production.md`](./production.md)

 -  [`README.md`](../README.md)

 -  [`status.md`](./status.md)
 
 -  [`troubleshoot.md`](./troubleshoot.md)
 
## TODO
  
 -  Currently nothing
