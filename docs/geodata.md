Using Geonames with Elasticsearch
=================================

# Generate GeoNames data

To generate the required data dump, you need the [GeoNames Solr image](https://gitlab.gwdg.de/subugoe/sub-docker-sdk/tree/master/examples/geonames). 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
http://localhost:8983/solr/geonames/select?fq=coordinate%3A%5B28.00%2C32.00%20TO%2038.00%2C42.00%20%5D&q=*%3A*&wt=geojson&geojson.field=coordinate&start=0&rows=150000
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

You might want to adjust the coordinates and the number of rows.

# Cleaning GeoJson for Elasticsearch import

You need to have [`jq`](https://stedolan.github.io/jq/) installed:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
jq --compact-output '.response|.features|.[] | . + .properties + .geometry | del(.properties,.geometry,.type,._version_,.n)'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cleaning GeoJson for Elasticsearch import and adding indexing entries

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
jq --compact-output '.response.features[]| {"index": {"_id": .properties.id, "_index": "geonames", "_type": "geoname"}}, . + .properties + .geometry| del(.properties,.geometry,.type,._version_,.n,.id)'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Look at the [Elasticsearch Bulk API documentation](https://www.elastic.co/guide/en/elasticsearch/reference/6.8/docs-bulk.html) to 
see the different options.

# Importing the mapping into Elasticsearch

The following command can be used to create a Index and mapping for Geonames in Elasticsearch

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
curl -XPUT "http://localhost:9200/geonames" -H 'Content-type:application/json' --data-binary @mapping.json 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Import data into Elasticsearch

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
curl -XPUT "http://localhost:9200/geonames/geoname/_bulk" -H 'Content-Type: application/x-ndjson' --data-binary @28.00_32.00_TO_38.00_42.00.json
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
