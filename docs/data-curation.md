Data curation
=============

# Introduction

There are several possible methods to ensure data consistency build into the applikation itself:

  -  Generation of family relations
  
  -  Filtering the list views
  
  
Additionally this document describes methods that can be used based on the database (SQL queries)

# Queries

## Finding persons that aren't referend to in inscriptions

If the following query doesn't return a `ip.incriptions_id` value, this means, that the person isn't linked to an inscription.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SELECT * FROM person p LEFT JOIN person_names pn ON p.id = pn.person_id LEFT JOIN inscriptions_person ip ON (p.id = ip.person_id);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Finding persons that aren't added as names to inscriptions

The following query can find persons that aren't completely added to inscriptions (name missing):

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SELECT p.id, p.fullname, pn.person_id, pn.names_id, ip.*, i_n.* FROM person p LEFT JOIN person_names pn ON p.id = pn.person_id LEFT JOIN inscriptions_person ip ON (p.id = ip.person_id) LEFT JOIN inscriptions_names i_n ON ip.inscriptions_id = i_n.inscriptions_id AND pn.names_id = i_n.names_id;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
