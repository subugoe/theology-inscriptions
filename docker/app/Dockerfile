FROM php:7.4-fpm-alpine

LABEL maintainer="mahnke@sub.uni-goettingen.de"

# Admin user settings
ARG USER=admin
ARG MAIL=copidocker@sub.uni-goettingen.de
ARG PASSWORD

#Check if environment is passed by docker-compose
ENV WORKDIR=/var/www/html/ \
    BUILD_CONTEXT=/mnt/build-context \
    COMPOSER_ALLOW_SUPERUSER=1 \
    APP_ENV=prod \
    MYSQL_USER=inscriptions \
    MYSQL_PASSWORD=LETMEIN \
    MYSQL_DATABASE=symfony4 \
    MYSQL_HOST=database \
    MYSQL_PORT=3306 \
    DATABASE_URL=mysql://$MYSQL_USER:$MYSQL_PASSWORD@$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DATABASE \
    EL_HOST=search \
    EL_PORT=9200 \
# Initial data dump
    SQL_DIR=/var/tmp/sql/ \
# Other settings
    BUILDFILE=.build \
    PHP_MEMORY_LIMIT=768M \
    EXT_APCU_VERSION=5.1.17 \
    REQ_BUILD="npm icu-dev libzip-dev oniguruma-dev libc-dev make g++ gcc autoconf file pkgconf dpkg-dev re2c git zip patch openjdk11-jre" \
    REQ_RUN="icu libzip bash" \
    SETUP_DATABASE_URL=sqlite:///%kernel.project_dir%/var/data.db \
    COMPOSER_VERSION_FILE=/.composer-version \
    GIT_VERSION_FILE=/.git-reversion \
    WAIT_FOR_IT=docker/common/wait-for-it/wait-for-it.sh \
    ANTLR_HOME=/opt/antlr \
    ANTLR_URL=https://www.antlr.org/download/antlr-4.9-complete.jar

COPY . $BUILD_CONTEXT

WORKDIR $WORKDIR
RUN mkdir -p /usr/local/bin/ && \
    mkdir -p $SQL_DIR && \
    mkdir $WORKDIR/public && \
    cp $BUILD_CONTEXT/docker/app/sql/*.sql $SQL_DIR && \
    cp $BUILD_CONTEXT/docker/app/entrypoint.d/*.sh /usr/local/bin/ && \
# Change into BUILD_CONTEXT to be able to get a filtered listing
    cd $BUILD_CONTEXT && \
    cp -r $BUILD_CONTEXT/$(ls -A | grep -vE '.git|docker|vendor|var') $WORKDIR && \
    cp $BUILD_CONTEXT/$WAIT_FOR_IT /usr/local/bin && \
    cd $WORKDIR && \
# Prepare to install OS and PHP dependencies
    apk --update upgrade && \
    apk add --no-cache $REQ_BUILD $REQ_RUN busybox && \
# Save Git revision hash
    GIT_DIR=$BUILD_CONTEXT/.git git rev-list  HEAD --max-count=1 > $GIT_VERSION_FILE && \
# Set version
    export BUILD=`date +%s` && \
    echo $BUILD> /$BUILDFILE && \
    echo "Wrote Version $BUILD to '/$BUILDFILE'" && \
# Set PHP configuration
    mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
    pecl config-set php_ini "$PHP_INI_DIR/php.ini" && \
    echo "error_log = /proc/self/fd/2" >> $PHP_INI_DIR/php.ini && \
# Increase PHP memory limit
    sed -i "s/memory_limit = 128M/memory_limit = $PHP_MEMORY_LIMIT/g" $PHP_INI_DIR/php.ini && \
# Prepare and install PHP extensions
# See https://olvlvl.com/2019-06-install-php-ext-source
    mkdir -p /usr/src/php/ext/apcu && \
    curl -fsSL https://github.com/krakjoe/apcu/archive/v$EXT_APCU_VERSION.tar.gz | tar xvz -C /usr/src/php/ext/apcu --strip 1 && \
    docker-php-ext-install -j 5 \
        apcu \
        intl \
        mbstring \
        zip \
        opcache \
        pdo_mysql && \
    docker-php-ext-enable apcu && \
# Clean up sources
    docker-php-source delete && \
# Get ANTLR
    mkdir -p $ANTLR_HOME && \
    (cd $ANTLR_HOME && wget $ANTLR_URL) && \
# Generate Query Parser
    java -jar $ANTLR_HOME/$(basename $(ls ${ANTLR_HOME}/*.jar)) -Dlanguage=PHP src/Util/Search/Query/Query.g4 -package 'App\Util\Search\Query' && \
# Setup Busybox (See https://www.stefaanlippens.net/bashrc_and_others/)
    echo 'alias less="busybox less"' >> /root/.profile && \
    echo 'alias ping="busybox ping"' >> /root/.profile && \
    echo 'alias bb="busybox"' >> /root/.profile && \
    set -x && \
# Setup dummy database URL
    sed -i "s/#        url:/        url: '$(echo "$SETUP_DATABASE_URL" | sed 's/\//\\\//g')'/g"  config/packages/doctrine.yaml && \
    echo "DATABASE_URL=$SETUP_DATABASE_URL" >> .env && \
    sed -i '18,27 {s/^/#/}' config/packages/doctrine.yaml && \
# Remove 'Fixtures' directory
    rm -rf ./src/DataFixtures && \
# Get composer and install dependencies
    wget https://getcomposer.org/download/latest-2.x/composer.phar && \
# Run composer
    php -d memory_limit=-1 composer.phar install --no-dev --prefer-dist --no-progress --no-suggest --optimize-autoloader --classmap-authoritative --no-interaction  && \
# Remove dummy database config
    sed -i "s/        url: '$(echo "$SETUP_DATABASE_URL" | sed 's/\//\\\//g')'/#        url:/g"  config/packages/doctrine.yaml && \
    sed '/^DATABASE_URL=sqlite.*$/d' .env && \
    sed -i '18,27 {s/^#//}' config/packages/doctrine.yaml && \
#Configure application (database and index)
    sed -i "s/user: root/user: $MYSQL_USER/" config/packages/doctrine.yaml && \
    sed -i "s/password: root/password: $MYSQL_PASSWORD/" config/packages/doctrine.yaml && \
    sed -i "s/host: 127.0.0.1/host: $MYSQL_HOST/" config/packages/doctrine.yaml && \
    sed -i "s/port: 3306/port: $MYSQL_PORT/" config/packages/doctrine.yaml && \
    sed -i "s/dbname: symfony4/dbname: $MYSQL_DATABASE/" config/packages/doctrine.yaml && \
    sed -i "s/default: { host: localhost, port: 9200 }/default: { host: $EL_HOST, port: $EL_PORT }/" config/packages/fos_elastica.yaml && \
    echo "DATABASE_URL=mysql://$MYSQL_USER:$MYSQL_PASSWORD@$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DATABASE" >> .env.dev && \
# Set permissions
    chmod +x /usr/local/bin/composer-init.sh && \
    rm -rf $WORKDIR/var && \
    mkdir -p $WORKDIR/var && chmod -R 777 $WORKDIR/var $WORKDIR/public && \
    chown -R $(stat -c "%U:%G" ..) var public && \
# Set volume version
    export BUILD=`cat /$BUILDFILE` && \
    echo $BUILD> $WORKDIR/$BUILDFILE && \
    sed -i -e 's/[^0-9]*//g' $WORKDIR/$BUILDFILE && \
    echo "Wrote Version '$BUILD' to '$WORKDIR/$BUILDFILE'" && \
# Cleanup and get JS dependencies
    rm -rf public/build/js/app.* public/build/css/app.css && \
#TODO: Update dependencies definitions
    npm ci && \
    npx encore production && \
# Setup init script (disable echo first)
    set +x && \
    sed -i "s/USER=admin/USER=$USER/" /usr/local/bin/composer-init.sh && \
    sed -i "s/MAIL=copidocker@sub.uni-goettingen.de/USER=$MAIL/" /usr/local/bin/composer-init.sh && \
    sed -i "s/PASSWORD=/PASSWORD=$PASSWORD/" /usr/local/bin/composer-init.sh && \
# Change PHP FPM config
    sed -i 's/pm = dynamic/pm = ondemand/g' $PHP_INI_DIR/../php-fpm.d/www.conf && \
    sed -i 's/pm.max_children = 5/pm.max_children = 10/g' $PHP_INI_DIR/../php-fpm.d/www.conf && \
    sed -i 's/;pm.process_idle_timeout = 10s;/pm.process_idle_timeout = 5s/g' $PHP_INI_DIR/../php-fpm.d/www.conf && \
    sed -i 's/;pm.max_requests = 500/pm.max_requests = 10/g' $PHP_INI_DIR/../php-fpm.d/www.conf && \
# Save Composer version
    php composer.phar -V |cut -d ' ' -f 3 > $COMPOSER_VERSION_FILE && \
    echo $COMPOSER_VERSION_FILE && \
# Get database dumper \
    cp $BUILD_CONTEXT/docker/app/scripts/symfony_mysqldump.sh . && \
# Clean up
    rm -rf composer.phar /usr/src/php* && \
    rm -f $(echo "$SETUP_DATABASE_URL" |cut -d / -f 5-9) && \
    npm prune --production && \
    apk del ${REQ_BUILD} && \
    rm -rf /var/cache/apk/* $ANTLR_HOME docker /tmp/* /root/.cache /root/.npm /root/.composer && \
    rm -rf /var/www/html/node_modules /usr/lib/node_modules

VOLUME $WORKDIR
