#!/bin/bash

# Inspired by https://stackoverflow.com/a/35724475

# See http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in/23905052#23905052
ROOT=$(readlink -f $(dirname "$0"))

cd $ROOT

# Get database parameters
dbname=$(grep dbname config/packages/doctrine.yaml | awk '{print $2;}')
dbuser=$(grep user config/packages/doctrine.yaml | awk '{print $2;}')
dbpassword=$(grep password config/packages/doctrine.yaml | awk '{print $2;}')
dbhost=$(grep host config/packages/doctrine.yaml | awk '{print $2;}')

filename="$(date '+%Y-%m-%d_%H-%M-%S').sql"

mysqldump -B "$dbname" -u "$dbuser" --password="$dbpassword" -h "$dbhost" > "$filename"

cat "$filename"