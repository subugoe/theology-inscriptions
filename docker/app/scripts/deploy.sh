#!/bin/bash

# Make sure we see the calls in the GitLab output
set +x

# Don't use this to exec it's not really working with ancient `docker-compose` (like 17.X)
APP_CONTAINER=copidocker_app_1
DOCKER_COMPOSE_CMD="docker-compose -f docker-compose.yml -f docker-compose.dev-server.yml "
DOCKER_EXEC_APP_CMD="docker exec $APP_CONTAINER "

# Get image ID for running App container
APP_CONTAINER_ID=$($DOCKER_COMPOSE_CMD ps -q app)
APP_IMAGE_TAG=$(docker inspect --format='{{.Config.Image}}' $APP_CONTAINER_ID)
echo "Retagging $APP_CONTAINER_ID, current tag $APP_IMAGE_TAG"
APP_IMAGE_NEW_TAG=$(echo $APP_IMAGE_TAG | cut -d ':' -f1):known-running
# Create a new tag for the known running image
docker tag "$APP_IMAGE_TAG" "$APP_IMAGE_NEW_TAG"
# Delete the old tag
docker rmi "$APP_IMAGE_TAG"

# Rotate logs before creating empty new ones
LOGS_DIR=./logs
if [ ! -d "$LOGS_DIR" ] ; then
    mkdir -p ./logs
fi
if [ -r "$LOGS_DIR/php-error.log" ] ; then
    mv "$LOGS_DIR/php-error.log" "$LOGS_DIR/php-error.$(date +%Y%m%dT%H%M%S).log"
    > $LOGS_DIRphp-error.log
fi
if [ -r "$LOGS_DIR/php-access.log" ] ; then
    mv "$LOGS_DIR/php-access.log" "$LOGS_DIR/php-access.$(date +%Y%m%dT%H%M%S).log"
    > $LOGS_DIRphp-access.log
fi

# Pull new versions of images, including our own
$DOCKER_COMPOSE_CMD pull

# Use '--remove-orphans' to get rid of containers that can exist if started with the wrong 'docker-compose.yml' file(s)
$DOCKER_COMPOSE_CMD down -v --remove-orphans

docker volume rm -f theologyinscriptions_web-app theologyinscriptions_dbdata copidocker_dbdata copidocker_web-app

# Start everything again
# For newer 'docker-compose' (1.21.0 or newer) use with --compatibility
# See https://github.com/docker/compose/pull/5684
#docker-compose --compatibility -f docker-compose.yml -f docker-compose.dev-server.yml up -d
$DOCKER_COMPOSE_CMD up -d

#sed -i "s/\\['dev' => true, 'test' => true\\]/['all' => true]/g" config/bundles.php

# Install dev tools
$DOCKER_EXEC_APP_CMD /usr/local/bin/install-web-profiler.sh

# Create accounts etc.
$DOCKER_EXEC_APP_CMD /usr/local/bin/composer-init.sh admin copidocker@sub.uni-goettingen.de password
# Link family relations
$DOCKER_EXEC_APP_CMD bin/console inscriptions:link-family-members -f -d

# Link family relations
$DOCKER_EXEC_APP_CMD php -d memory_limit=-1 ./composer.phar require symfony/debug-pack

# Cleanup
IMAGE_LIST="$(docker images | grep 'docker.gitlab.gwdg.de/subugoe/theology-inscriptions' | grep '<none>' |tr -s [:blank:]|cut -d ' ' -f 3|tr '\n' ' ')"
if [ -n "$IMAGE_LIST" ]; then
    docker rmi $IMAGE_LIST
else
    echo "Image list is empty"
fi
