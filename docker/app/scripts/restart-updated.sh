#!/bin/bash

# Colours
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RESET='\033[0m'

echo -e "${RED}Semiautomatic update part 2!${RESET}"
echo -e "${RED}We will stop the current running application, delete the web-app volume.${RESET}"
echo -e "${RED}Next step would be to start the updated images and then reindexing the database contents.${RESET}"
echo -e "${RED}You might want to hit Ctrl+C to stop, otherwise hunt the 'Any' key.${RESET}"
read -n 1 -s -r -p "Press any key to continue"

docker-compose down -v --remove-orphans
docker volume rm copidocker_web-app copidocker_search-data
docker volume rm theologyinscriptions_web-app theologyinscriptions_search-data

# Rotate logs before creating empty new ones
LOGS_DIR=./logs
if [ ! -d "$LOGS_DIR" ] ; then
    mkdir -p ./logs
fi
if [ -r "$LOGS_DIR/php-error.log" ] ; then
    mv "$LOGS_DIR/php-error.log" "$LOGS_DIR/php-error.$(date +%Y%m%dT%H%M%S).log"
    > $LOGS_DIRphp-error.log
fi
if [ -r "$LOGS_DIR/php-access.log" ] ; then
    mv "$LOGS_DIR/php-access.log" "$LOGS_DIR/php-access.$(date +%Y%m%dT%H%M%S).log"
    > $LOGS_DIRphp-access.log
fi

docker-compose up -d
sleep 15

docker-compose exec app /usr/local/bin/reindex.sh
docker-compose exec app bin/console c:c
