#!/bin/sh

ENV=$APP_ENV

cd $WORKDIR
# Use `--no-debug`, see https://github.com/FriendsOfSymfony/FOSElasticaBundle/issues/791
# For the other options see https://stackoverflow.com/questions/24451983/populating-foselasticabundle-running-out-of-php-memory-possible-memory-leak

echo "Cleaning first to make sure we use the real config file - cache management of Symfony is to aggressive"
php bin/console c:c --env=$ENV
php bin/console fos:elastica:reset $index --env=$ENV
php bin/console fos:elastica:create --env=$ENV
if [[ "$ENV" == "dev" ]] ; then
    php bin/console fos:elastica:populate --no-debug -vvv --env=$ENV
else
    php bin/console fos:elastica:populate --no-debug --env=$ENV
fi
