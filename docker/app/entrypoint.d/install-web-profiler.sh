#!/bin/sh

cd $WORKDIR
curl https://getcomposer.org/installer --output composer-setup.php 
echo "Reinstalling Composer version '$(cat $COMPOSER_VERSION_FILE)'"
php composer-setup.php --version $(cat $COMPOSER_VERSION_FILE) --install-dir=$WORKDIR
rm composer-setup.php
./composer.phar require --no-scripts --prefer-dist --no-progress --no-suggest --no-interaction symfony/web-profiler-bundle
chown -R $(stat -c "%U:%G" $WORKDIR) $WORKDIR/var $WORKDIR/public
#./composer.phar dumpautoloader
