#!/bin/bash

# Login info
USER=admin
MAIL=copidocker@sub.uni-goettingen.de
PASSWORD=

#Internal variables
SQL="$SQL_DIR/init.sql"

# Check if data is provided via variables or arguments
if [ -z "$1" ]; then
    USER=$USER
else
    USER=$1
fi

if [ -z "$2" ]; then
    MAIL=$MAIL
else
    MAIL=$2
fi

if [ -z "$3" ]; then
    PASSWORD=$PASSWORD
else
    PASSWORD=$3
fi

if [ -n "$4" ]; then
    RUN=true
    APP_ENV=dev
fi

# Colours
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RESET='\033[0m'

# Should be set by Docker
# WORKDIR=/var/www/html/
cd $WORKDIR
umask 0000

SCRIPTNAME=`basename "$0"`

# Git ignore content
GITIGNORE="# Lines below added by $SCRIPTNAME
public/build
public/build/**
package.json
composer.json
composer.lock
yarn.lock
.gitignore
config/packages/doctrine.yaml
config/packages/fos_elastica.yaml
.env.dev
symfony.lock
package-lock.json"

############ Check if a local version of this file is executeable
DEV_SCRIPTNAME="${WORKDIR}docker/app/entrypoint.d/composer-init.sh"

if [[ "$APP_ENV" == "dev" && "$SCRIPTNAME" != "$DEV_SCRIPTNAME" ]] ; then
    if [[ "$APP_ENV" = "dev" && -x "$DEV_SCRIPTNAME" ]] ; then
        echo -e "${YELLOW}'dev' mode detected and local version ('${DEV_SCRIPTNAME}') of this script is executable!${RESET}"
        #TODO Check if we are an instance of /usr/local/bin/composer-init.sh. otherwise don't start
        SCRIPT=$(readlink -f "$0")
        SCRIPTPATH=$(dirname "$SCRIPT")
        echo "Current instance is running from $SCRIPTPATH"
        if [[ "$SCRIPTPATH" != '/usr/local/bin' ]] ; then
            echo -e "${GREEN}Starting '${DEV_SCRIPTNAME}'...${RESET}"
            echo "Running local script isn't finished yet!"
            #$DEV_SCRIPTNAME $@
            #exit $?
        fi
    fi
elif [[ "$APP_ENV" == "dev" && "$SCRIPTNAME" == "$DEV_SCRIPTNAME" ]] ; then
    echo -e "${GREEN}'dev' mode detected and local version of this script is running!${RESET}"
fi

############ Check Preconditions

# Check if we got the volume build for this image
echo -e "${YELLOW}Checking version file: $BUILDFILE${RESET}"
if [[ -r "/$BUILDFILE" && -r "$WORKDIR/$BUILDFILE" ]]; then
    if [[ `cat "/$BUILDFILE"` != `cat "$WORKDIR/$BUILDFILE"` ]]; then
        echo -e "${RED}ERROR: Version files don't match, maybe you need to run 'docker volume prune' first?${RESET}"
        echo "/$BUILDFILE: "`cat "/$BUILDFILE"`
        echo "$WORKDIR/$BUILDFILE: "`cat "$WORKDIR/$BUILDFILE"`
    else
        echo -e "${GREEN}Versions for Container and volume match.${RESET}"
    fi
else
    if [[ "$APP_ENV" == "dev" ]] ; then
       echo "In dev mode the volume isn't used, '.' is mounted instead, no $BUILDFILE"
    else
       echo -e "${YELLOW}WARNING: Version files not found!${RESET}"
    fi
fi

echo -e "${YELLOW}Checking Git revision file: ${GIT_VERSION_FILE}${RESET}"
if [[ -r "${GIT_VERSION_FILE}" ]]; then
    echo -e "${GREEN}Git revision file exists.${RESET}"
    GIT_REVISON=`cat "$GIT_VERSION_FILE"`
    echo "Image was build from Git revision ${GIT_REVISON}"
    if [[ "$APP_ENV" = "dev" && -r "$WORKDIR/.git" ]] ; then
        echo "In dev '.' is mounted, '.git' exists, checking Git revision"
        # TODO: We need to get the last three or so _remote_ revisions and check if the current revision is in this range.
        # Local revisions shouldn't be checked since we don't know if these have been pushed.

    fi
fi

if [[ "$APP_ENV" == "dev" ]] ; then
    # Setup XDebug
    if [[ -n "$XDEBUG_HOST" ]] ; then
        XDEBUG_REMOTE_HOST="$XDEBUG_HOST"
    else
        XDEBUG_REMOTE_HOST=host.docker.internal
    fi
    echo -e "${GREEN}Using ${XDEBUG_REMOTE_HOST} as debugging host${RESET}"
    echo 'xdebug.remote_host='"$XDEBUG_REMOTE_HOST" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

    # Check if composer is installed
    if [[ ! -r composer.phar ]] ; then
        curl -sS https://getcomposer.org/installer | php
        echo "Installed composer"
    fi

    # Regenrate Query parser
    echo "Regenerating query parser"
    java -jar $ANTLR_HOME/$(basename $(ls ${ANTLR_HOME}/*.jar)) -Dlanguage=PHP src/Util/Search/Query/Query.g4 -package 'App\Util\Search\Query'

fi

############ Start setup

# Check if dev mode is enabled and change container if required
if [[ "$APP_ENV" = "dev" ]] ; then
    echo -e "${YELLOW}Setting up 'dev' mode...${RESET}"
    DIR=$PWD
    cd $WORKDIR
    if [[ -d ./var ]] ; then
        echo -e "${YELLOW}'./var' directory detected, deleting it...${RESET}"
        rm -rf ./var
    fi

    echo -e "${YELLOW}Rewriting configuration in dev mode, do not commit changed files!${RESET}"
    sed -i "s/user: root/user: $MYSQL_USER/" config/packages/doctrine.yaml
    sed -i "s/password: root/password: $MYSQL_PASSWORD/" config/packages/doctrine.yaml
    sed -i "s/host: 127.0.0.1/host: $MYSQL_HOST/" config/packages/doctrine.yaml
    sed -i "s/port: 3306/port: $MYSQL_PORT/" config/packages/doctrine.yaml
    sed -i "s/dbname: symfony4/dbname: $MYSQL_DATABASE/" config/packages/doctrine.yaml
    sed -i "s/default: { host: localhost, port: 9200 }/default: { host: $EL_HOST, port: $EL_PORT }/" config/packages/fos_elastica.yaml
    cd $DIR
    COMPOSER_PROCESS_TIMEOUT=2000 php -d memory_limit=-1 composer.phar install --dev --prefer-dist --verbose --no-suggest --no-interaction

# Enable this if you need to run PHP 7.3, otherwise PHP 7.4 is required
#    INSTALLED_PHP_VERSION=$(php -v | head -1 |cut -d ' ' -f 2)
#    INSTALLED_PHP_MINOR=$(echo $INSTALLED_PHP_VERSION | cut -d '.' -f2)
#    echo -e "${YELLOW}Disabling version check for 'bin/console' and 'composer.phar'!${RESET}"
#    echo "Since the message would say, that the version is set in 'composer.lock', but this patches 'vendor/autoload.php', this is technically a bug fix."
#    sed -i 's/704[[:digit:]][[:digit:]]/70300/g' vendor/autoload.php
#    sed -i 's/"version": "7.4"/"version": "7.3"/g' symfony.lock

    # Echo some hints on TODO tags
    echo "Develop mode application created, try to fix these:"
    grep -ri TODO public src assets config
    grep -ri FIXME public src assets config

#TODO: Make this work or remove it
    if grep -q "$SCRIPTNAME" .gitignore; then
        echo "${YELLOW}Changing .gitignore to prevent accidents with generated files${RESET}"
        echo "$GITIGNORE" >> .gitignore
    else
        echo ".gitignore already changed"
    fi
    npm ci
elif [[ "$APP_ENV" = "prod" ]] ; then
    echo -e "${YELLOW}Setting up 'prod' mode...${RESET}"
    # Use Symfony commands instead of Composer
    # TODO: Chek if this is really needed
    php bin/console cache:clear
    php bin/console fos:js-routing:dump --format=json --target=assets/js/js_routes.json
    php bin/console assets:install --symlink --relative ./public
fi

echo -e "${YELLOW}If this fails, the database isn't up yet or connection configuration is wrong!${RESET}"
echo "You might want to add a 'sleep' between 'docker-compose up' and this configuration script"

php bin/console doctrine:schema:update -f
DOCTRINE_ERROR=$?
if [ $DOCTRINE_ERROR -ne 0 ]; then
    echo -e "${RED}Doctrine Schema update failed with ${DOCTRINE_ERROR}!${RESET}"
    echo "Maybe you need to run 'docker volume rm inscriptions_dbdata' first?"
    exit $DOCTRINE_ERROR
fi

# Initialize the database
if [ -r "$SQL" ]; then
    echo "Importing database dump $SQL using doctrine and renaming source file"
    echo "Errors in your SQL file won't throw an error here yet, but reindexing might fail later on.";
    echo "If there is an timeout you need to rerun the script and drop existing tables first.";
    # TODO: Use Fixture here
    php bin/console doctrine:database:import -v -n "$SQL"
    mv "$SQL" "$SQL".bak
else
    echo -e "${RED}SQL file '$SQL' not found - may it had already been imported?${RESET}"
fi

# Reindex
/usr/local/bin/wait-for-it.sh -t 0 $EL_HOST:$EL_PORT -- /usr/local/bin/reindex.sh

# Check if everything is set to create a admin user
if [[ -n "$USER" && -n "$MAIL" && -n "$PASSWORD" ]]; then
    # Create a user
    php bin/console inscriptions:create-user -n $USER $MAIL $PASSWORD
    if [ "$APP_ENV" = "dev" ] ; then
        echo -e "${GREEN}User $USER (password: $PASSWORD) created.${RESET}"
    else
        echo -e "${GREEN}User $USER created.${RESET}"
    fi
else
    echo -e "${RED}User data incomplete, admin account wasn't created\nUsage:\n$1 USER MAIL PASSWORD${RESET}"
fi

# Add this https://github.com/docker-library/wordpress/blob/df190dc9c5752fd09317d836bd2bdcd09ee379a5/apache/docker-entrypoint.sh#L146-L171

# Check if php command should be run
if [ -n "$RUN" ]; then
    npx encore dev --watch &
    tail -f $WORKDIR/var/log/dev.log &
    php-fpm
fi
