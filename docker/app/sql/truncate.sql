SET FOREIGN_KEY_CHECKS=0;

LOCK TABLE family_members WRITE; TRUNCATE TABLE family_members;
LOCK TABLE inscriptions WRITE; TRUNCATE TABLE inscriptions;
LOCK TABLE inscriptions_names WRITE; TRUNCATE TABLE inscriptions_names;
LOCK TABLE inscriptions_person WRITE; TRUNCATE TABLE inscriptions_person;
LOCK TABLE names WRITE; TRUNCATE TABLE names;
LOCK TABLE names_languages WRITE; TRUNCATE TABLE names_languages;
LOCK TABLE person WRITE; TRUNCATE TABLE person;
LOCK TABLE person_names WRITE; TRUNCATE TABLE person_names;

UNLOCK TABLES;

SET FOREIGN_KEY_CHECKS=1;