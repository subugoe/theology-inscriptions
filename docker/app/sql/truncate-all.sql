SET FOREIGN_KEY_CHECKS=0;

LOCK TABLE dates WRITE; TRUNCATE TABLE dates;
LOCK TABLE family_members WRITE; TRUNCATE TABLE family_members;
LOCK TABLE user WRITE; TRUNCATE TABLE user;
LOCK TABLE inscriptionlanguages WRITE; TRUNCATE TABLE inscriptionlanguages;
LOCK TABLE inscriptions WRITE; TRUNCATE TABLE inscriptions;
LOCK TABLE inscriptions_inscriptionlanguages WRITE; TRUNCATE TABLE inscriptions_inscriptionlanguages;
LOCK TABLE inscriptions_names WRITE; TRUNCATE TABLE inscriptions_names;
LOCK TABLE inscriptions_person WRITE; TRUNCATE TABLE inscriptions_person;
LOCK TABLE inscriptionsgenre WRITE; TRUNCATE TABLE inscriptionsgenre;
LOCK TABLE inscriptionsmaterial WRITE; TRUNCATE TABLE inscriptionsmaterial;
LOCK TABLE languages WRITE; TRUNCATE TABLE languages;
LOCK TABLE name_types WRITE; TRUNCATE TABLE name_types;
LOCK TABLE names WRITE; TRUNCATE TABLE names;
LOCK TABLE names_languages WRITE; TRUNCATE TABLE names_languages;
LOCK TABLE person WRITE; TRUNCATE TABLE person;
LOCK TABLE person_names WRITE; TRUNCATE TABLE person_names;
LOCK TABLE regions WRITE; TRUNCATE TABLE regions;
LOCK TABLE relation_types WRITE; TRUNCATE TABLE relation_types;
LOCK TABLE religions WRITE; TRUNCATE TABLE religions;
LOCK TABLE migration_versions WRITE; DROP TABLE IF EXISTS migration_versions; 

UNLOCK TABLES;

SET FOREIGN_KEY_CHECKS=1;