FROM docker.gitlab.gwdg.de/subugoe/theology-inscriptions/base:dev

LABEL maintainer="mahnke@sub.uni-goettingen.de"

# Get PHP Sources
COPY --from=php:7.4-fpm-alpine /usr/src/php* /usr/src/
COPY . /mnt/build-context/

RUN apk --update upgrade && \
# Copy scripts again, to not have to do a full rebuild if they have changed
    cp $BUILD_CONTEXT/docker/app/entrypoint.d/composer-init.sh $BUILD_CONTEXT/docker/app/entrypoint.d/reindex.sh /usr/local/bin/ && \
# Reinstall dev dependencies
    apk add --no-cache $REQ_BUILD && \
# Set PHP configuration
    pecl config-set php_ini "$PHP_INI_DIR/php.ini" && \
# Increase PHP memory limit
    sed -i "s/memory_limit = 128M/memory_limit = $PHP_MEMORY_LIMIT/g" $PHP_INI_DIR/php.ini && \
    sed -i "s/max_execution_time = 30/max_execution_time = 60/g" $PHP_INI_DIR/php.ini && \
## Reinstall 'composer' in the version like in the base image
    curl https://getcomposer.org/installer --output composer-setup.php && \
    echo "Reinstalling Composer version '$(cat $COMPOSER_VERSION_FILE)'" && \
    php composer-setup.php --version $(cat $COMPOSER_VERSION_FILE) --install-dir=$WORKDIR && \
    rm composer-setup.php && \
# Install dependencies
    php -d memory_limit=-1 composer.phar install --no-scripts --prefer-dist --no-progress --no-suggest --no-interaction && \
    npm ci && \
    npm run build
