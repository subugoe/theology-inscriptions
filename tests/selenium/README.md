__Inhaltsverzeichnis:__

[[_TOC_]]

-----

# Datei: _inscriptions\_tests.side_

__Allgemeine Anmerkungen:__ 
Im Folgenden bezieht sich der Begriff "Browsertab" auf einen Browsertab und der Begriff "Tab" auf einen Reiter innerhalb der Anwendung (z. B. der Reiter "Inscript Date" im Menüpunkt "Add Inscript").

__Hinweise für alle Tests mit Eingabe neuer Daten:__
1. Bei Entitäten, bei denen die Eingabe der Attribute auf mehrere Tabs verteilt ist, wird für jeden Tab ein separater Test angelegt. Dieser Test überprüft dann die Funktionen von genau diesem Tab. 
    > Ausnahme: Pflichtfelder aus den anderen Tabs müssen natürlich in jedem Test ausgefüllt werden.
    
2. Bestehen in einem Tab Verknüpfungen zu anderen Entitäten (bspw. die Anlage eines neuen Namens während man eine neue Person anlegt), wird für jede dieser Verknüpfungen ebenfalls ein separater Test angelegt. 
3. Einträge aus Drop-Down-Menüs, die erst beim Klick auf das betreffende Feld erscheinen, werden durch die Selenium IDE nicht erkannt. Hier müssen die XPATH-Angaben zu den betreffenden Optionen (z. B. ```//select[@id='person_names']/option[1]```) manuell an den entsprechenden Stellen zu den Tests hinzugefügt werden.
4. Da das Nachladen neu angelegter Namen und Personen ein wenig dauern kann, muss an diesen Stellen evtl. manuell eine Pause in den Tests hinzugefügt werden.
5. Das Sonderzeichen-Keyboard wird nicht in allen Feldern, in denen es erscheint, auch getestet.

__Allgemeine TODOs:__
* __Ganz zu Beginn: Anlage von Testdaten bzw. Import von Testdaten.__

----------
## Test Suite: _01 – General Functions_ <a name="test-suite-1"></a>
__Was wird getestet:__ Allgemeine Navigations- und Anzeigefunktionen der Anwendung.

__Anmerkungen:__ In diesen Tests werden noch keine Daten hinzugefügt, gelöscht oder verändert.

### Login and Logout
* Aufruf von https://dev.inscriptions.sub.uni-goettingen.de
* Einloggen mit "admin" und "password"
* Ausloggen über "sign out"

### Login
* Einloggen mit "admin" und "password"
    > Nötig, damit man sich für die folgenden Tests nicht immer wieder neu einloggen muss

### Main Navigation Menu
* Auswahl aller Hauptmenüpunkte ("Defined Inscriptions", "Add Inscript", etc.)
* ABER: keine weiteren Aktionen auf den einzelnen Seiten
    > Hinweis: Der Browser muss Pop-Ups erlauben, damit die neuen Browsertabs für "Help" und "Search" geöffnet werden können.
    
    > Schließen bzw. Wechseln der Browsertabs nicht implementiert, da auf diesen keine weiteren Aktionen durchgeführt werden.

### Column Ordering
* Sortierung der einzelnen Spalten in der Inscriptions-Tabelle 
    > Funktionalität für alle Entitäten gleich, wird daher nur einmal zentral an dieser Stelle getestet

### Links in Table
* Navigieren innerhalb einer Liste (für alle Entitäten gleich)
* Auswahl der Show-, Edit-, Delete-Buttons eines einzelnen Eintrags in der Liste inkl. Navigation zurück zum Listing
* ABER: es wird kein Speichern oder Löschen durchgeführt! Der Pop-Up-Dialog beim Löschen wird daher nicht weiter bearbeitet und der Test wird beendet.

### Searching
* Suche nach "beqa" innerhalb der Liste.
__TODO: Suche so anpassen, dass die zu findenden Datensätze vorher erst angelegt werden.__


### Filtering List
* Öffnen aller Filter
* Aus- und Abwahl einiger Filter
* Eingabe verschiedener Werte für die Filter
* "Apply" der Filter mit anschließender automatisch getriggerter Suche
* Löschen der ausgewählten Filter


## Test Suite: _02 – Add Inscripts_
__Was wird getestet:__ Hinzufügen neuer Inscripts mit unterschiedlichen Daten

__Anmerkungen:__ --

### Single Tabs
* Auswahl der einzelnen Tabs für diesen Menüpunkt; keine weitere Aktionen

### Inscript Tab
* Eingabe von Siglum (Pflichtfeld) und Languages
* Entfernen einer zuvor ausgewählten Language
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus

### About Region Tab
* Eingabe von Siglum (Pflichtfeld)
* Wechsel zum Tab "About Region"
* Auswahl einer vordefinierten Region
* Eingabe von Subregion und Village unter Nutzung des Sonderzeichen-Keyboards
    > Hinweis: Die Einblendungen des Sonderzeichen-Keyboards werden von der Selenium IDE erkannt.
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus

### Inscript Date Tab
* Eingabe von Siglum (Pflichtfeld)
* Wechsel zum Tab "Inscript Date"
* Eingabe eines Datums und Auswahl einer Periode
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus

### More About Inscript Tab 1
_(Testet die grundlegenden Funktionen)_
* Eingabe von Siglum (Pflichtfeld)
* Wechsel zum Tab "More About Inscript"
*  Auswahl von Genre und Material
* Eingabe von Remarks
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus

### More About Inscript Tab 2
_(Testet die Auswahl bereits existierender Personen und Namen)_
* Eingabe von Siglum (Pflichtfeld)
* Wechsel zum Tab "More About Inscript"
* Auswahl von Personen und Namen
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus

### More About Inscript Tab 3 - Another Person
_(Testet die Erstellung von neuen Personen aus der Eingabemaske für neue Inscripts heraus)_
* Eingabe von Siglum (Pflichtfeld)
* Wechsel zum Tab "More About Inscript"
* Klick auf "Define another person"
* Im automatisch neu geöffneten Browsertab: Eingabe bzw. Auswahl der Pflichtfelder und Speichern der neu angelegten Person
* Schließen des neu geöffneten Browsertabs
* Speichern des Inscripts
* Löschen des neu angelegten Inscripts aus der Liste heraus
* Löschen der neu angelegten Person aus der Personen-Liste heraus
    > Achtung: Um den neuesten Eintrag in der Personen-Liste zu erhalten, muss diese zunächst absteigend nach ID sortiert werden (Standard ist Sortierung nach Name)

### More About Inscript Tab 3 - Another Name
_(Testet die Erstellung von neuen Namen aus der Eingabemaske für neue Inscripts heraus)_
* Eingabe von Siglum (Pflichtfeld)
* Wechsel zum Tab "More About Inscript"
* Klick auf "Define other names"
* Im automatisch neu geöffneten Browsertab: Eingabe der Pflichtfelder und Speichern des neu angelegten Namens
* Schließen des neu geöffneten Browsertabs
* Speichern des Inscripts
* Löschen des neu angelegten Inscripts aus der Liste heraus
* Löschen des neu angelegten Namens aus der Namen-Liste heraus
    > Achtung: Um den neuesten Eintrag in der Namen-Liste zu erhalten, muss diese zunächst absteigend nach ID sortiert werden (Standard ist Sortierung nach Name)


## Test Suite: _03 - Add Names_
__Was wird getestet:__ Hinzufügen neuer Namen

__Anmerkungen:__ --

### Add Name
* Eingabe eines neuen Namens mit allen Feldern
* Speichern des Datensatzes
* Sortierung der Liste nach ID (wichtig für das Löschen des zuletzt angelegten Eintrags)
* Löschen des neu angelegten Namens aus der Liste heraus


## Test Suite: _04 - Add Persons_
__Was wird getestet:__ Hinzufügen neuer Personen mit unterschiedlichen Daten

__Anmerkungen:__ --

### Name Tab 1
* Auswahl von Gender
* Eingabe bzw. Auswahl von Full Name, Type und Names (Pflichtfelder)
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus

### Name Tab 2 - Another Name
_(Testes das Anlegen eines neuen Namens aus der Eingabemaske für neue Personen heraus)_
* Eingabe von Full Name und Type (Pflichtfelder)
* Klick auf "Define other names"
* Im automatisch neu geöffneten Browsertab: Eingabe von Name (Pflichtfeld)
* Schließen des neu geöffneten Browsertabs
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus
* Löschen des neu angelegten Namens aus der Namen-Liste heraus
    > Achtung: Um den neuesten Eintrag in der Namen-Liste zu erhalten, muss diese zunächst absteigend nach ID sortiert werden (Standard ist Sortierung nach Name)

### Other Details Tab
* Eingabe bzw. Auswahl von Full Name, Type und Names (Pflichtfelder)
* Wechsel zum Tab "Other Details"
* Eingabe von Profession, Auswahl von Religion, Angabe zu Ethnicity und Remarks
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus

### Family Members Tab - Existing Person
_(Testet das Anlegen einer Familienbeziehung zu einer bereits existierenden Person)_
* Eingabe bzw. Auswahl von Full Name, Type und Names (Pflichtfelder)
* Wechsel zum Tab "Family Members"
* Auswahl einer Familienbeziehung zu einer bereits in der Datenbank existierenden Person
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus

### Family Members Tab - New Person
_(Testet das Anlegen einer Familienbeziehung zu einer neu zu erstellenden Person)_
* Eingabe bzw. Auswahl von Full Name, Type und Names (Pflichtfelder)
* Wechsel zum Tab "Family Members"
* Klick auf "Define another person"
* Im automatisch neu geöffneten Browsertab: Eingabe bzw. Auswahl von Full Name, Type und Names (Pflichtfelder)
* Schließen des neu geöffneten Browsertabs
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus
* Löschen der zweiten neu angelegten Person aus der Liste heraus

### Family Members Tab - Multiple Persons
_(Testet das Anlegen und Löschen mehrerer Familienbeziehungen)_
* Eingabe von Eingabe von Full Name, Type und Names (Pflichtfelder)
* Wechsel zum Tab "Family Members"
* Anlegen von zwei Familienbeziehungen mit existierenden Personen
* Löschen einer der beiden Familienbeziehungen
* Anlegen einer weiteren Familienbeziehung mit einer neu anzulegenden Person
* Speichern des Datensatzes
* Löschen des neu angelegten Datensatzes aus der Liste heraus
* Löschen der neu angelegten zweiten Person aus der Liste heraus


## Test Suite: _05 - Family Relations_
__Was wird getestet:__ Änderungen an Familienbeziehungen

__Anmerkungen:__ --

__!!! TODO__
(hier z. B. Edit von Familienbeziehungen aus der Familienbeziehungsliste heraus)


## Test Suite: _06 - Single Functions_
__Was wird getestet:__ Einzelne Funktionalitäten der Anwendung

__Anmerkungen:__ --

### Add Family Relation with Drag and Drop
_(Testet das Anlegen einer Familienbeziehung durch Drag & Drop eines Eintrags in der Personen-Liste auf einen anderen Eintrag)_
* Anlegen von zwei neuen Personen
* Wechsel in die Liste von Personen
* Sortierung nach ID, damit die beiden zuletzt angelegten Personen oben erscheinen
* Drag & Drop der zweiten Person auf die erste
* Anlegen einer Familienbeziehung mit Bemerkung
* Speichern des Datensatzes
* Löschen der neu angelegten Familienbeziehung aus der Liste heraus
* Löschen der beiden neu angelegten Personen aus der Personen-Liste heraus


## Test Suite: _07 - Errors and Constraints_
__Was wird getestet:__ Gewünschte Fehlermeldungen und Einschränkungen

__Anmerkungen:__ --

__!!! TODO__
(hier z.B. Fehlermeldung wenn eine Familienbeziehung bereits definiert ist und trotzdem neu eingegeben wird, Ausblenden unmöglicher Verwandtschaftsbeziehungen etc.)

### Family Relations - Check Automatically Generated Family Relations
_(Testet die automatische Anlage neuer Familienbeziehungen und dass diese nicht geändert werden können)_
* Anlegen von zwei neuen Personen, die zweite steht in einer Verwandschaftsbeziehung zur ersten
* Wechsel in die Liste von Personen
* Sortierung nach ID, damit die beiden zuletzt angelegten Personen oben erscheinen
* Edit der ersten angelegten Person
* Wechsel in den "Family Members"-Tab
* Dort Überprüfung, ob die Felder nicht bearbeitbar sind
    > Hinweis: Diese Art der Überprüfung muss manuell mit den entsprechenden XPATH-Ausdrücken zum Test hinzugefügt werden.
* Zurück in die Liste
* Löschen der ersten angelegten Person aus der Liste heraus
* Edit der zweiten angelegten Person
* Wechsel in den "Family Members"-Tab
* Dort Überprüfung, ob Familienbeziehungen leer sind
    > Hinweis: Diese Art der Überprüfung muss manuell mit den entsprechenden XPATH-Ausdrücken zum Test hinzugefügt werden.
* Zurück in die Liste
* Löschen der zweiten angelegten Person aus der Liste heraus

### Family Relations - Check Define Another Person Link
_(Testet_

_(a) die fehlende Anzeige des "Define another person"-Links bei nicht vorhandenen Familienbeziehungen,_

_(b) die Anzeige des Links beim Erstellen von neuen Familienbeziehungen,_

_(c) das Verschwinden des Links bei Löschen aller Familienbeziehungen,_

_(d) die fehlende Anzeige des Links bei Vorhandensein von ausschließlich automatisch generierten Familienbeziehungen,_

_(e) das Verschwinden des Links bei Löschen aller nicht automatisch generierten Familienbeziehungen)_

* Anlegen einer neuen Person ohne Familienbeziehung
* Wechsel in den "Family Members"-Tab, überprüfen, ob der "Define another person"-Link fehlt (a)
* Speichern der Person
* Anlegen einer zweiten neuen Person
* Im "Family Members"-Tab: Anlage einer Familienbeziehung
* Überprüfen, ob der "Define another person"-Link jetzt existiert (b)
    > Hinweis: Diese Art der Überprüfung muss manuell mit den entsprechenden XPATH-Ausdrücken zum Test hinzugefügt werden.
* Abbruch der Anlage einer neuen Familienbeziehung
* Überprüfen, ob der "Define another person"-Link jetzt nicht mehr existiert (c)
* Anlegen einer Familienbeziehung zur ersten Person
* Speichern der zweiten Person, Wechsel in die Personen-Liste
* Sortierung nach ID, damit die beiden zuletzt angelegten Personen oben erscheinen
* Edit der ersten angelegten Person, Wechsel in den "Family Members"-Tab
* Überprüfung, ob "Define another person"-Link fehlt (da Beziehung zur zweiten Person automatisch generiert wurde) (d)
* Anlage einer neuen Familienbeziehung
* Überprüfen, ob "Define another person"-Link jetzt existiert (b)
* Abbruch der Neuanlage einer Familienbeziehung
* Überprüfen, ob der "Define another person"-Link jetzt nicht mehr existiert (e)
    > Achtung: Hier schmeißt der Test eine Fehlermeldung, da die Applikation an dieser Stelle momentan noch fehlerhaft ist (siehe Issue #104).
* Zurück in die Liste
    > Die Sortierung nach ID sollte jetzt noch aktiv sein.
* Löschen der beiden neu angelegten Personen aus der Liste heraus

### Names - Prevent Duplicate Names
_(Testet die Fehlermeldung beim Versuch, einen bereits existierenden Namen ein weiteres Mal einzugeben)_
* Anlegen eines neuen Namens, Speichern des Datensatzes
* Anlage desselben Namens
* Überprüfung, ob Fehlermeldung erscheint
* Wechsel in die Namensliste, Sortieren nach ID und Löschen des angelegten Namens
