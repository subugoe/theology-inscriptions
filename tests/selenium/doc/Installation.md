# Installation von Git
## Windows
* Download der Installationsdatei: https://gitforwindows.org/
* Beim Installationsprozess können die vorgegebenen Standard-Einstellungen beibehalten werden.

## Ubuntu/Linux
* ```sudo apt-get install git```

# Klonen eines Repositories
* Erstellen eines Projekt-Ordners zur Aufnahme der Repository-Dateien
* Wechsel in diesen Ordner
* Klonen des gewünschten Repositories:
```git clone -b selenium https://gitlab.gwdg.de/aluesch/theology-inscriptions.git```<br>(Syntax: __git clone -b__ \<BRANCH IM REPOSITORY\> \<ADRESSE DES REPOSITORIES\>)
* Das gesamte Repository wird in den Projekt-Ordner heruntergeladen. Die für die Selenium IDE benötigten Dateien finden sich im Ordner __./tests/selenium__.

Alternative: Die benötigten Dateien über das GitLab-Webinterface herunterladen.

# Installation der Selenium IDE
* Download des Browser-Plugins für Chrome oder Firefox: https://www.selenium.dev/selenium-ide/
* Die Selenium IDE lässt sich über das entsprechende Symbol in der Symbolleiste des Browsers in einem neuen Fenster öffnen.

# Öffnen einer bereits vorhandenen Datei
* Auswahl einer __*.side__ Datei in der Selenium IDE
![Screenshot Selenium IDE](./img/screenshot_selenium_ide.PNG "Selenium IDE Startbildschirm")
* Nach dem Öffnen der Datei werden im linken Menü alle Tests aufgelistet, die in dieser Datei enthalten sind.

* Tests lassen sich zu sog. Test Suites zusammenfassen. Zur Auswahl der Test Suites klickt man auf das Menü oben links.
![Screenshot Selenium Hauptmenü](./img/screenshot_main_menu.PNG "Selenium IDE Hauptmenü")

* Die Basis-URL für die Tests lässt sich oberhalb der einzelnen Test-Befehle eingeben. 
![Screenshot Selenium URL](./img/screenshot_url.PNG "Selenium IDE URL")

* Ein einzelner Test oder alle Tests innerhalb einer Test Suite werden mithilfe der Run-Symbole abgespielt. 
* Es öffnet sich ein neues Browser-Fenster, in dem die Tests ablaufen. 
* Die Geschwindigkeit der Tests lässt sich mit dem Stoppuhr-Symbol rechts neben den Run-Symbolen anpassen.
![Screenshot Selenium Run](./img/screenshot_run.png "Selenium IDE Run-Symbole")

Weitere Informationen in der offiziellen Dokumentation: https://www.seleniumhq.org/selenium-ide/docs/en/introduction/getting-started/