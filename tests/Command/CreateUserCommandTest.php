<?php

// Taken from https://symfony.com/doc/current/console.html#testing-commands

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CreateUserCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('inscriptions:create-user');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            // pass arguments to the helper
            'username' => 'hwurst',
            'email' => 'hwurst@host.com',
            'password' => 'password',
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();

        //$user = $this->em->getRepository(User::class)->findOneBy(['username' => $user]);
        //$user->delete();
    }
}
