var Encore = require('@symfony/webpack-encore');
var path = require('path');

Encore.
    disableSingleRuntimeChunk().
    // the project directory where compiled assets will be stored
    setOutputPath('public/build/').
    // the public path used by the web server to access the previous directory
    setPublicPath('/build').
    cleanupOutputBeforeBuild().
    enableSourceMaps(!Encore.isProduction()).
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning(Encore.isProduction())

    // uncomment to define the assets of the project
    addEntry('app', './assets/js/app.js').
    addEntry('login', './assets/js/login.js').
    addEntry('search', './assets/js/search.js').
    addEntry('index', './assets/js/index.js').
    addEntry('menu', './assets/js/menu.js').

    // uncomment if you use Sass/SCSS files
    enableSassLoader().

    // enable TypeScript
    //.addEntry('main', './assets/ts/app.ts')
    // eslint-disable-next-line no-unused-vars
    enableTypeScriptLoader(function(tsConfig) {
        // You can use this callback function to adjust ts-loader settings
        // https://github.com/TypeStrong/ts-loader/blob/master/README.md#loader-options
        // For example:
        // tsConfig.silent = false
    }).
    addLoader({
        test: /\.js$/u,
        enforce: 'post',
        include: [path.resolve(__dirname, './assets/js/components/credits')],
        loader: 'obfuscator-loader'
    });
var config = Encore.getWebpackConfig();
// Use config to change `webpack` configuration
module.exports = config;
