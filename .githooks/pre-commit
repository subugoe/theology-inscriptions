#!/bin/bash
# See https://github.com/rycus86/githooks for more information about custom repository based hooks

# Expecte PHP version (major minor without dot)
PHP_REQUIRED_VERSION=74

# For debugging and development on OS X
if [[ "$OSTYPE" == "darwin"* ]]; then
    echo "Mac OS X detected using PHP from Mac Ports"
    PHP=$(which php74)
else
    PHP=$(which php)
fi
PHP_VERSION=$($PHP --version | head -n 1 | cut -d " " -f 2 | cut -c 1,3)
if [ $PHP_VERSION -lt $PHP_REQUIRED_VERSION ] ; then
    echo "Your 'php' version is to old! You need $PHP_REQUIRED_VERSION, you have $PHP_VERSION"
    exit 6
fi

if [ "$(basename $(pwd))" == '.githooks' ] ; then
    cd ..
fi

echo "Using 'php', version $PHP_VERSION at $PHP"

echo "Running PHP analysis tools..."

echo "Running 'psalm'..."
$PHP vendor/bin/psalm --show-info=false
PSALM_ERROR=$?
if [ $PSALM_ERROR != 0 ] ; then
    echo "'psalm' reported errors(code '$PSALM_ERROR'), fix these and commit before you push!"
    exit `expr 100 + $PSALM_ERROR`
fi

echo "Running 'php-cs-fixer'..."
$PHP vendor/bin/php-cs-fixer fix --config=.php-cs-fixer.php -v --dry-run
PHPCSFIXER_ERROR=$?
if [ $PHPCSFIXER_ERROR != 0 ] ; then
    echo "'php-cs-fixer' reported errors(code '$PHPCSFIXER_ERROR'), fix these and commit before you push!"
    exit `expr 200 + $PHPCSFIXER_ERROR`
fi

echo "Running 'phpstan'..."
$PHP -d memory_limit=-1 vendor/bin/phpstan analyse --level 3 src
PHPSTAN_ERROR=$?
if [ $PHPSTAN_ERROR != 0 ] ; then
    echo "'phpstan' reported errors(code '$PHPSTAN_ERROR'), fix these and commit before you push!"
    exit `expr 300 + $PHPSTAN_ERROR`
fi

#echo "Running 'rector'..."
#$PHP vendor/bin/rector process src --dry-run
#RECTOR_ERROR=$?
#if [ $RECTOR_ERROR != 0 ] ; then
#    echo "'rector' reported errors(code '$RECTOR_ERROR'), fix these and commit before you push!"
#    exit `expr 400 + $RECTOR_ERROR`
#fi

echo "Running Frontend analysis tools..."

echo "Running 'lint:twig'..."
$PHP bin/console lint:twig templates/
LINTTWIG_ERROR=$?
if [ $LINTTWIG_ERROR != 0 ] ; then
    echo "'bin/console lint:twig' reported errors(code '$LINTTWIG_ERROR'), fix these and commit before you push!"
    exit `expr 500 + $LINTTWIG_ERROR`
fi

echo "Running 'eslint'..."
npx eslint -c .eslintrc.js assets/js/
ESLINT_ERROR=$?
if [ $ESLINT_ERROR != 0 ] ; then
    echo "'eslint' reported errors(code '$ESLINT_ERROR'), fix these and commit before you push!"
    exit `expr 600 + $ESLINT_ERROR`
fi

echo "Running 'stylelint'..."
npx stylelint assets/css/*
STYLELINT_ERROR=$?
if [ $STYLELINT_ERROR != 0 ] ; then
    echo "'stylelint' reported errors(code '$STYLELINT_ERROR'), fix these and commit before you push!"
    exit `expr 700 + $STYLELINT_ERROR`
fi

echo "Running 'yamllint' on configuration..."
npx yamllint config/**/*.yaml
YAMLLINT_ERROR=$?
if [ $YAMLLINT_ERROR != 0 ] ; then
    echo "'yamllint' reported errors(code '$YAMLLINT_ERROR'), fix these and commit before you push!"
    exit `expr 800 + $YAMLLINT_ERROR`
fi

echo "No errors reported, you can push your changes"
