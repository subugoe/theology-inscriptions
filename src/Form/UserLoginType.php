<?php

declare(strict_types=1);
// Inspired by https://gist.github.com/florentdestremau/78e3828af1832c309e72b429afa5ca06

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class UserLoginType extends AbstractType
{
    private CsrfTokenManagerInterface $csrfTokenManager;

    public function __construct(CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $token = $this->csrfTokenManager->getToken('authenticate')->getValue();
        $builder
            ->add('username', TextType::class)
            ->add('password', PasswordType::class)
            ->add('_csrf_token', HiddenType::class, [
                'data' => $token,
            ])
            ->add('login', SubmitType::class, ['attr' => ['class' => 'login']]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
