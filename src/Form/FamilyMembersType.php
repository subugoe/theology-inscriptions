<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\FamilyMembers;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FamilyMembersType extends AbstractType
{
    private PersonRepository $repository;

    public function __construct(PersonRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('relationType', null, ['label' => 'Relation Type'])
            ->add('person', null, ['label' => 'Related With',
                'attr' => ['class' => 'select2-reload keyboard-input', 'data-widget' => 'select2', 'data-refresh-route' => 'getNewPersons'],
                'query_builder' => fn (PersonRepository $repository) => $repository->createQueryBuilder('person')->orderBy('person.fullname', 'ASC'),
                'choice_label' => function (Person $person) {
                    $id = $person->getId();
                    $fullname = $person->getFullname();
                    if ($id == null) {
                        $id = 'not saved yet';
                    }
                    if ($fullname == null) {
                        $fullname = '';
                    }

                    return '(PID: ' . $id . ') ' . $fullname;
                },
            ])
            ->add('remarks', null, ['label' => 'Remarks', 'attr' => ['class' => 'keyboard-input-textarea']])
            ->add('generated', HiddenType::class, ['attr' => ['class' => 'generated-relationship']]);
    }

    /**
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FamilyMembers::class,
        ]);
    }
}
