<?php

declare(strict_types=1);

namespace App\Util\Transformer;

use Elastica\Document;
use FOS\ElasticaBundle\Transformer\ModelToElasticaAutoTransformer;
use InvalidArgumentException;
use LogicException;

class CollectionMapper extends ModelToElasticaAutoTransformer
{
    protected array $callbacks = [];

    public function __construct(array $options = [])
    {
        parent::__construct($options, null);
    }

    protected function transformObjectToDocument($object, array $fields, $identifier = '')
    {
        $document = new Document($identifier, [], '', $this->options['index']);
        $filteredFields = [];
        foreach ($fields as $key => $mapping) {
            $value = null;
            if (isset($mapping['property_path'])) {
                $value = $this->propertyAccessor->getValue($object, $mapping['property_path']);
            } elseif (isset($mapping['_collection_path']) && isset($mapping['_collection_property'])) {
                $filteredFields[] = $key;
                $collection = $this->getValue($object, $mapping['_collection_path']);
                $tmpVal = [];

                if ($collection !== null) {
                    foreach ($collection as $item) {
                        $tmpVal[] = $this->propertyAccessor->getValue($item, $mapping['_collection_property']);
                    }
                }
                $value = $tmpVal;
            } else {
                $value = $this->propertyAccessor->getValue($object, $key);
            }
            if (isset($mapping['_collection_callback'])) {
                $callback = null;
                $method = null;
                if (array_key_exists($mapping['_collection_callback'][0], $this->callbacks)) {
                    $callback = $this->callbacks[$mapping['_collection_callback'][0]];
                    $method = $mapping['_collection_callback'][1];
                } else {
                    throw new InvalidArgumentException("Callback object for key '" . $mapping['_collection_callback'][0] . "' isn\'t registred!");
                }
                if (is_iterable($value)) {
                    $tmpVal = [];
                    foreach ($value as $item) {
                        $tmpVal[] = $callback->$method($item);
                    }
                    $value = $tmpVal;
                } else {
                    $value = $callback->$method($value);
                }
            }
            if (isset($mapping['type']) && in_array($mapping['type'], ['nested', 'object']) && isset($mapping['properties']) && !empty($mapping['properties'])) {
                $document->set($key, $this->transformNested($value, $mapping['properties']));

                continue;
            }
            $deduplicateNestedArray = function (&$value) {
                $seen = [];
                $key = print_r($value, true);
                if (in_array($key, $seen)) {
                    return false;
                }
                $seen[] = $key;

                return true;
            };

            //Check for unique values here
            if (is_array($value) && $value !== []) {
                $tmpVal = [];
                foreach ($value as $tmp) {
                    // This will be needed for geo points
                    if (is_array($tmp)) {
                        throw new LogicException('The dedupication of arrays for mapped collections isn\'t implemented yet!');
                    }
                    // Don't use 'array_unique' here, since we also get rid of empty strings
                    if (!in_array($tmp, $tmpVal) && $tmp != '') {
                        $tmpVal[] = $tmp;
                    }
                }
                $value = $tmpVal;
            }
            // Use null to be able to remap a null field in index configuration
            if (empty($value)) {
                $value = null;
            }

            $document->set($key, $this->normalizeValue($value));
        }

        return $document;
    }

    public function getValue(object $source, string $property): ?array
    {
        if (!is_iterable($source) && is_object($source)) {
            $source = [$source];
        }
        $collected = [];
        foreach ($source as $item) {
            if (strpos($property, '.') === false) {
                $collected = array_merge($collected, $this->propertyAccessor->getValue($item, $property)->toArray());
            } else {
                $propertyPath = explode('.', $property);
                $firstProperty = $propertyPath[0];
                $newSource = $this->propertyAccessor->getValue($item, $firstProperty);
                $newProperty = implode('.', array_slice($propertyPath, 1));
                $childs = $this->getValue($newSource, $newProperty);
                if ($childs !== null) {
                    $collected = array_merge($collected, $childs);
                }
            }
        }

        if ($collected === []) {
            return null;
        }

        return $collected;
    }

    public function addCallback(string $key, object $callback): void
    {
        $this->callbacks[$key] = $callback;
    }
}
