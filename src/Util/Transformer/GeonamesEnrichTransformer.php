<?php

declare(strict_types=1);

namespace App\Util\Transformer;

use App\Entity\Inscriptions;
use Elastica\Document;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\MatchQuery;
use Elastica\Query\Term;
use Elastica\Query\Terms;
use Elastica\Request;
use FOS\ElasticaBundle\Elastica\Client;
use FOS\ElasticaBundle\Transformer\ModelToElasticaAutoTransformer;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;

class GeonamesEnrichTransformer extends ModelToElasticaAutoTransformer
{
    protected Client $client;
    protected string  $geonamesIndex;
    protected ?LoggerInterface $logger = null;
    private const RETURN_FIELDS = ['coordinates', 'name'];
    protected string $field = 'geonames_coordinates';
    protected string $placeMethod = 'getVillage';
    protected string $clazz = Inscriptions::class;

    public function __construct(Client $client, string $geonamesIndex, LoggerInterface $logger = null)
    {
        parent::__construct();

        $this->client = $client;
        $this->geonamesIndex = $geonamesIndex;
        $this->logger = $logger;
    }

    public function transform($object, array $fields): Document
    {
        $id = $this->propertyAccessor->getValue($object, $this->options['identifier']);
        if (is_a($object, $this->clazz) && $id != null) {
            return $this->transformEntityToDocument($object, $fields, $id);
        }

        throw new InvalidArgumentException("Model Transformer needs an '" . $this->clazz . "' Type object");
    }

    protected function transformEntityToDocument(object $entity, array $fields, $id = ''): Document
    {
        $placeMethod = $this->placeMethod;
        $place = $entity->$placeMethod();
        $doc = $this->transformObjectToDocument($entity, $fields, $id);
        $coords = $this->queryPlace($place);
        if ($doc->has($this->field)) {
            $doc->remove($this->field);
        }
        $doc->set($this->field, $coords);

        return $doc;
    }

    public static function buildQuery(string $index, string $place): Query
    {
        $indexes = [$index];
        $boolQuery = new BoolQuery();
        $mustQueries = [new Terms('featurecode', ['PPL', 'PPLA']), new Term(['featureclass' => 'P']), new MatchQuery('n', $place)];
        $boolQuery->addMust($mustQueries);
        $shouldQueries = [new Term(['n.keyword' => $place]), new Term(['name' => $place])];
        $boolQuery->addShould($shouldQueries);
        $boolQuery->addFilter(new Terms('_index', $indexes));
        $boolQuery->addFilter(new Terms('_index', $indexes));
        $query = new Query($boolQuery);
        $query->setSource(self::RETURN_FIELDS);

        return $query;
    }

    //Provide a wrapper to set our own default values
    protected function queryPlace(?string $place): ?array
    {
        $coords = self::getPlaceCoordinates($this->client, $this->geonamesIndex, $place);
        if ($coords === null) {
            return ['lat' => 199, 'lon' => 199];
        }

        return ['lat' => $coords[0], 'lon' => $coords[1]];
    }

    public static function getPlaceCoordinates(Client $client, string $index, ?string $place): ?array
    {
        if ($place == null) {
            return null;
        }

        $query = self::buildQuery($index, $place)->toArray();
        $response = $client->request('/_search', Request::GET, json_encode($query, JSON_THROW_ON_ERROR));
        $hits = $response->getData()['hits']['hits'];
        if (!empty($hits[0])) {
            return $hits[0]['_source']['coordinates'];
        }

        return null;
    }

    protected static function isGeoPoint(array $point): bool
    {
        return array_key_exists('lat', $point) || array_key_exists('lon', $point);
    }
}
