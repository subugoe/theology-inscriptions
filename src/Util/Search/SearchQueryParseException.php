<?php

namespace App\Util\Search;

use RuntimeException;
use Throwable;

class SearchQueryParseException extends RuntimeException
{
    private array $errors;

    public function __construct(string $msg, array $errors, $int = 0, Throwable $previous = null)
    {
        parent::__construct($msg, $int, $previous);
        $this->errors = $errors;
    }

    public function getErrorString($html = ['sourceStart' => '<span class="query-source">', 'sourceEnd' => '</span>', 'errorStart' => '<em class="query-error">', 'errorEnd' => '</em>']): ?string
    {
        if ($this->errors !== []) {
            if (empty($html)) {
                $html = ['sourceStart' => '', 'sourceEnd' => '', 'errorStart' => '', 'errorEnd' => ''];
            }
            $errorList = '';
            foreach ($this->errors as $error) {
                $errorList .= 'Syntax error at position ' . $error['position'] . ': ' . ucfirst($error['msg']) . "\n";
                if (isset($error['source']) && $error['source'] != '') {
                    $errorList .= $html['sourceStart'] . substr($error['source'], 0, $error['position']) . $html['errorStart'] . substr($error['source'], $error['position']) . $html['errorEnd'] . $html['sourceEnd'] . "\n";
                }
            }

            return $errorList;
        }

        return null;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
