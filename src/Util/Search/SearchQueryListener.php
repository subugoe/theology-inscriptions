<?php

declare(strict_types=1);

namespace App\Util\Search;

use App\Util\Search\Query\QueryBaseListener;

class SearchQueryListener extends QueryBaseListener
{
    private string $curIndex = '';
    private string $curField = '';
    private string $curSubfield = '';
    private string $curFullfield = '';
    private string $curQueryStr = '';
    // We use a boolean since op has only two states, false meaning 'OR', the default
    private bool $curOp = false;
    private bool $lastOp = false;
    private SearchBuilderInterface $searchBuilder;

    public function __construct(SearchBuilderInterface $searchBuilder)
    {
        $this->searchBuilder = $searchBuilder;
    }

    private function reset(): void
    {
        // Reset all except operator ('op')
        $this->curIndex = '';
        $this->curField = '';
        $this->curSubfield = '';
        $this->curQueryStr = '';
        $this->curFullfield = '';
        $this->lastOp = $this->curOp;
        $this->curOp = false;
    }

    public function enterQuery($context): void
    {
        $this->reset();
    }

    public function exitQuery($context): void
    {
        $op = false;
        if ($this->lastOp || $this->curOp) {
            $op = true;
        }
        if ($this->curIndex == '' && $this->curField == '' && $this->curQueryStr != '') {
            $this->searchBuilder->addTerm($this->curQueryStr, $op);
        } else {
            $this->searchBuilder->addField([$this->curIndex, $this->curField, $this->curSubfield], $this->curQueryStr, $op);
        }
    }

    public function exitIndex($context): void
    {
        $this->curIndex = $context->getStart()->getText();
    }

    public function exitField($context): void
    {
        $this->curField = $context->getStart()->getText();
    }

    public function exitSubfield($context): void
    {
        $this->curSubfield = $context->getStart()->getText();
    }

    public function exitTerm($context): void
    {
        if ($context->getStart()->getText() === $context->getStop()->getText()) {
            $this->curQueryStr = $context->getStart()->getText();

            return;
        }
        $this->curQueryStr = $context->getStart()->getText() . $context->getStop()->getText();
    }

    public function exitOperator($context): void
    {
        if ($context->getStart()->getText() === 'AND') {
            $this->curOp = true;

            return;
        }
        $this->curOp = false;
    }

    //Use this for debugging
    /*
    public function enterEveryRule($ctx): void
    {
        echo 'Entered ' . $ctx->getText() . "\n";
        echo 'Actually token: ' . $ctx->getStart()->getText() . "\n";
    }
    */
}
