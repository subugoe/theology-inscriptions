grammar Query;

options {
    language = PHP;
}

tokens {
    OPERATOR , INDEX , FIELD , SUBFIELD
}

/*
 * Parser Rules
 */

operator                     : AND | OR ;
subfield                     : NAME_STRING ;
field                        : NAME_STRING ;
compoundField                : ( field | field FIELD_SEPERATOR subfield? ) TERM_SEPERATOR ;
index                        : NAME_STRING INDEX_SEPERATOR ;
term                         : (QUERY_STRING) | (IDENTIFIER) | (NAME_STRING WILDCARD?) | (UNICODE_STRING WILDCARD*) ;
query                        : ((index compoundField term | compoundField term | term) operator?) ;
queries                      : (query)+ EOL ;

/*
 * Lexer Rules
 */

FIELD_SEPERATOR              : '.' ;
INDEX_SEPERATOR              : '_' ;
TERM_SEPERATOR               : ':' ;
WILDCARD                     : '*' | '?' ;

AND                          : 'AND' ;
OR                           : 'OR' ;

NAME_STRING                  : ([a-zA-Z])+ ;
QUERY_STRING                 : '"' ('""' | ~ '"')+ '"' ;
UNICODE_STRING               : ([\u0080-\uFFFF])+ ;
IDENTIFIER                   : ([0-9])+ ;

WHITESPACE                   : [ \t\r\n]+ -> skip ;
EOL                          : [\n\r] + ;

