<?php

declare(strict_types=1);

namespace App\Util\Search;

interface SearchBuilderInterface
{
    public function addTerm(string $term, bool $required = false): void;

    public function addField(array $fields, string $term, bool $required): void;

    public function setQueryString(string $queryString): self;

    public function getQuery(): object;

    public function setFilter(SearchFilterInterface $filter): self;

    public function setHighlight(bool $highlight): self;

    public function getHighlight(): bool;
}
