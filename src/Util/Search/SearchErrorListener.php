<?php

declare(strict_types=1);

namespace App\Util\Search;

use Antlr\Antlr4\Runtime\Error\Exceptions\RecognitionException;
use Antlr\Antlr4\Runtime\Error\Listeners\BaseErrorListener;
use Antlr\Antlr4\Runtime\Recognizer;

// See https://stackoverflow.com/a/18137301
class SearchErrorListener extends BaseErrorListener
{
    protected array $errors = [];
    protected string $source;

    public function __construct(string $source)
    {
        $this->source = $source;
    }

    public function syntaxError(Recognizer $recognizer, ?object $offendingSymbol, int $line, int $charPositionInLine, string $msg, ?RecognitionException $e): void
    {
        $this->errors[] = ['line' => $line, 'position' => $charPositionInLine, 'msg' => $msg, 'source' => $this->source];
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
