<?php

declare(strict_types=1);

namespace App\Util\Search;

interface SearchFilterInterface
{
    public function match(array $fields, string $term, bool $required): bool;
}
