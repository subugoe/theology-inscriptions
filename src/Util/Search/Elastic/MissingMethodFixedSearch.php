<?php

declare(strict_types=1);

namespace App\Util\Search\Elastic;

use Elastica\Client;
use Elastica\ResultSet\BuilderInterface;
use Elastica\Search;

class MissingMethodFixedSearch extends Search
{
    public function __construct(Client $client = null, BuilderInterface $builder = null)
    {
        if ($client instanceof Client) {
            $this->_client = $client;
        }
        parent::__construct(new Client([]), $builder);
    }

    public function removeIndex(string $index): self
    {
        $index = array_search($index, $this->_indices);
        if ($index !== false) {
            unset($this->_indices[$index]);
        }

        return $this;
    }

    public function setIndices(array $indices = []): self
    {
        $this->_indices = [];
        foreach ($indices as $index) {
            $this->addIndex($index);
        }

        return $this;
    }

    public function setClient(Client $client): self
    {
        $this->_client = $client;

        return $this;
    }
}
