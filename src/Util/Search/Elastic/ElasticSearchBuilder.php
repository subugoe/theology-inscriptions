<?php

declare(strict_types=1);

namespace App\Util\Search\Elastic;

use Antlr\Antlr4\Runtime\CommonTokenStream;
use Antlr\Antlr4\Runtime\InputStream;
use Antlr\Antlr4\Runtime\Tree\ParseTreeWalker;
use App\Util\Search\Query\QueryLexer;
use App\Util\Search\Query\QueryParser;
use App\Util\Search\SearchBuilderInterface;
use App\Util\Search\SearchErrorListener;
use App\Util\Search\SearchFilterInterface;
use App\Util\Search\SearchQueryListener;
use App\Util\Search\SearchQueryParseException;
use Elastica\Query;
use Elastica\Query\AbstractQuery;
use Elastica\Query\BoolQuery;
use Elastica\Query\InnerHits;
use Elastica\Query\MatchPhrase;
use Elastica\Query\MultiMatch;
use Elastica\Query\Nested;
use Elastica\Query\QueryString;
use Elastica\Query\SpanMulti;
use Elastica\Query\SpanNear;
use Elastica\Query\SpanTerm;
use Elastica\Query\Wildcard;
use Elastica\Search;
use LogicException;
use Psr\Log\LoggerInterface;
use function Symfony\Component\String\u;

class ElasticSearchBuilder implements SearchBuilderInterface
{
    // TODO: Check if we need a better mechanism for filtering index names (feilds[0]), if we only want to search a single index
    protected string $queryStr;
    protected SearchQueryListener $listener;
    protected bool $highlight = true;
    private bool $parsed = false;
    protected LoggerInterface $logger;
    protected array $highlightTerms = [];
    protected array $query = [];
    protected array $indices = [];
    protected string $fieldSeperator = '.';
    protected string $termSeperator = ':';
    protected int $resultSize = 10_000;
    protected SearchFilterInterface $filter;
    protected array $indicesMetadata;

    public function __construct(array $indicesMetadata, LoggerInterface $logger)
    {
        $this->indicesMetadata = $indicesMetadata;
        $this->logger = $logger;
        $this->listener = new SearchQueryListener($this);
    }

    public function parse(): self
    {
        if ($this->queryStr === '') {
            throw new LogicException('No query string given, use "setQueryString" to set it!');
        }

        $lexer = new QueryLexer(InputStream::fromString($this->queryStr));
        $lexer->removeErrorListeners();
        $errorListener = new SearchErrorListener($this->queryStr);
        $lexer->addErrorListener($errorListener);
        $tokens = new CommonTokenStream($lexer);
        $parser = new QueryParser($tokens);
        $parser->setBuildParseTree(true);
        $tree = $parser->queries();

        ParseTreeWalker::default()->walk($this->listener, $tree);
        if ($errorListener->getErrors() !== []) {
            throw new SearchQueryParseException('Parsing errors', $errorListener->getErrors(), 0, null);
        }
        $this->parsed = true;
        $this->logger->debug('Parsed subqueries are ', $this->query);

        return $this;
    }

    // See https://stackoverflow.com/questions/24340385/foselastica-bundle-retrieving-highlights-for-results for highlighting
    // See https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-body.html#request-body-search-highlighting
    public static function buildHighlight(array $fields, AbstractQuery $highlightQuery = null, array $tags = ['pre_tags' => ['<em class="hl">'], 'post_tags' => ['</em>']], int $fragments = 0): array
    {
        $highlightSettings = [];
        $fieldSettings = ['require_field_match' => false, 'number_of_fragments' => $fragments];
        $fieldSettings = array_merge($fieldSettings, $tags);
        if ($highlightQuery != null) {
            $fieldSettings = array_merge($fieldSettings, ['highlight_query' => $highlightQuery->toArray()]);
        }
        foreach ($fields as $field) {
            $highlightSettings[] = [$field => $fieldSettings];
        }

        return ['fields' => $highlightSettings];
    }

    protected function buildHighlightQuery(): AbstractQuery
    {
        return self::buildSimpleQuery(implode(' ', $this->highlightTerms));
    }

    public static function buildSimpleQuery(string $query, bool $wildcard = true): AbstractQuery
    {
        $simpleQuery = new QueryString($query);
        $simpleQuery->setDefaultOperator('or');
        $simpleQuery->setParam('analyze_wildcard', $wildcard);

        return $simpleQuery;
    }

    public static function buildNestedQuery(string $path, AbstractQuery $query, array $highlight = null): AbstractQuery
    {
        $nested = new Nested();
        $nested->setPath($path);
        $nested->setParam('ignore_unmapped', true);
        if ($highlight !== null) {
            $innerHits = new InnerHits();
            // This is called Insane defaults syndrome: this is limited to 3 and it's difficult to spot from the docs
            $innerHits->setSize(100);
            $innerHits->setHighlight($highlight);
            $nested->setInnerHits($innerHits);
        }
        $nested->setQuery($query);

        return $nested;
    }

    private function getSearchEngineTerm(string $term, array $fields = []): string
    {
        $fieldStr = implode($this->fieldSeperator, $fields);
        if ($fieldStr !== '' && $fieldStr !== '0' && $fieldStr !== '*') {
            return $fieldStr . $this->termSeperator . $term;
        }

        return $term;
    }

    private function getSearchEngineField(array $fields = []): ?string
    {
        if (isset($fields[1]) && $fields[1] === '.') {
            return '';
        }
        $searchFields = implode($this->fieldSeperator, $fields);

        if ($searchFields === '' || $searchFields === '*') {
            return null;
        }

        return $searchFields;
    }

    protected function buildTermQuery(string $term, array $fields = ['*']): AbstractQuery
    {
        $field = $this->getSearchEngineField($fields);
        if (u($term)->containsAny('"')) {
            $term = str_replace('"', '', $term);
        }
        if (!u($term)->containsAny(' ')) {
            if ($field === '') {
                return self::buildSimpleQuery($term);
            }

            return self::buildSimpleQuery($this->getSearchEngineTerm($term, $fields));
        }

        if (u($term)->containsAny(['*', '?'])) {
            if ($field === null) {
                $termPos = u($this->queryStr)->indexOf($term);
                $wildcardPos = 0;
                if (strpos($term, '*') !== false) {
                    $wildcardPos = u($term)->indexOf('*');
                } elseif (strpos($term, '?') !== false) {
                    $wildcardPos = u($term)->indexOf('?');
                }
                $error = [['line' => 0, 'position' => ($termPos + $wildcardPos), 'msg' => 'Unexpected wildcard', 'source' => $this->queryStr]];

                throw new SearchQueryParseException('The search doesn\'t support wildcards in phrases without given field name', $error, 0, null);
            }

            // Everything could had been so easy, but no one could have imagined how stupid elastic search has been designed - we try to do the most complicated thing ever - a phrase search with wildcards!
            // See https://discuss.elastic.co/t/wildcard-with-exact-phrase-search-using-query-string/102631/4
            // TODO: This isn't tested yet
            $spanNear = new SpanNear();
            $spanNear->setInOrder(true);
            $term = str_replace('"', '', $term);
            foreach (preg_split('/\s+/', $term) as $fragment) {
                if (strpos($fragment, '*') !== false || strpos($fragment, '?') !== false) {
                    $multiWildcard = new SpanMulti(new Wildcard($field, $fragment));
                    $spanNear->addClause($multiWildcard);
                } else {
                    $spanNear->addClause(new SpanTerm([$field, $fragment]));
                }
            }

            return $spanNear;
        }
        // Use just a MatchPhrase if we got no wildcards and know the field :(
        // TODO: This isn't tested yet
        if ($field !== null) {
            return new MatchPhrase($field, $term);
        }
        // See https://stackoverflow.com/a/54186740
        $multiMatch = new MultiMatch();
        $multiMatch->setQuery($term);
        $multiMatch->setType('phrase');
        $multiMatchFields = $this->getSearchEngineField($fields);
        if ($multiMatchFields !== null) {
            $multiMatch->setFields([$multiMatchFields]);
        }

        return $multiMatch;
    }

    protected function buildQuery(): Query
    {
        if (!$this->parsed) {
            $this->parse();
        }
        $highlight = self::buildHighlight(['*'], $this->buildHighlightQuery());
        $tmpQueries = [];
        $nestedQueries = [];

        // Direct to Elasticsearch
        foreach ($this->query as $q) {
            $term = $q['term'];
            $baseQuery = null;
            // Check if whe got a query for a complete nested entity
            if ((isset($q['index']) && $q['index'] != '') && (!isset($q['subfield']) || $q['subfield'] == '') && in_array($q['field'], $this->indicesMetadata[$q['index']]['nested'])) {
                $q['subfield'] = '.';
            }
            if ((!isset($q['index']) || $q['index'] == '') && (!isset($q['field']) || $q['field'] == '') && $term != '') {
                // Term only query
                $baseQuery = $this->buildTermQuery($q['term']);
                // Also search nested objects :( This stupid ElasticSearch doesn't do anything on its own...
                foreach (array_keys($this->indicesMetadata) as $setIndex) {
                    foreach ($this->indicesMetadata[$setIndex]['nested'] as $nestedField) {
                        $nestedQueries[$nestedField]['should'][] = $baseQuery;
                    }
                }
            } elseif ((isset($q['index']) && $q['index'] != '') && (!isset($q['field']) || $q['field'] == '') && $term != '') {
                // Term only search on given index
                $baseQuery = $this->buildTermQuery($q['term']);
                // Also search nested objects :( This stupid ElasticSearch doesn't do anything on its own...
                foreach ($this->indicesMetadata[$q['index']]['nested'] as $nestedField) {
                    $nestedQueries[$nestedField]['should'][] = $baseQuery;
                }
            } elseif ((!isset($q['index']) || $q['index'] == '') && (isset($q['field']) && $q['field'] != '') && (isset($q['subfield']) && $q['subfield'] != '') && $term != '') {
                // Field and term only search
                $baseQuery = self::buildTermQuery($term, [$q['field'], $q['subfield']]);
            } elseif ((isset($q['index']) && $q['index'] != '') && (isset($q['field']) && $q['field'] != '') && (!isset($q['subfield']) || $q['subfield'] == '') && $term != '') {
                // Index, field and term search
                $baseQuery = self::buildTermQuery($term, [$q['field']]);
            } elseif ((isset($q['field']) && $q['field'] != '') && (isset($q['subfield']) && $q['subfield'] != '')) {
                // Index, field, subfield and term search
                /*
                if ($q['subfield'] === '.') {
                    // Search the whole nested entity
                    $q['subfield'] = '';
                }
                */
                $subKey = 'should';
                if ($q['required']) {
                    $subKey = 'must';
                }
                $nestedQueries[$q['field']][$subKey][] = self::buildTermQuery($term, [$q['field'], $q['subfield']]);
            } else {
                $this->logger->debug('Query for index "' . $q['index'] . '" (field "' . $q['field'] . '", term "' . $term . '") ignored');
            }
            if ($baseQuery instanceof AbstractQuery) {
                if ($q['required']) {
                    //Add a must query
                    $tmpQueries['must'][] = $baseQuery;
                } else {
                    //Add should query
                    $tmpQueries['should'][] = $baseQuery;
                }
            }
        }
        foreach ($nestedQueries as $path => $bools) {
            // We need to check whether we have a 'should' or 'must' query :(
            $nested = new Nested();
            $nested->setPath($path);
            $nested->setParam('ignore_unmapped', true);
            if ($highlight !== null) {
                $innerHits = new InnerHits();
                // This is called Insane defaults syndrome: this is limited to 3 and it's difficult to spot from the docs
                $innerHits->setSize(100);
                $innerHits->setHighlight($highlight);
                $nested->setInnerHits($innerHits);
            }
            $boolQuery = new BoolQuery();
            // TODO: Check what happens if we have a 'must' and a 'should' for the same path (might happen for term only searches)
            // TODO: check what happens if we got a nested field with the same name in two different indices
            // This might happen for the field 'person' (which is in 'names' and 'inscriptions') - maybe it's better to use 'index'.'field' notation for the keys in $nestedQueries array
            foreach ($bools as $bool => $boolQueries) {
                $clauseMethod = 'add' . u($bool)->title()->toString();
                foreach ($boolQueries as $bq) {
                    $boolQuery->$clauseMethod($bq);
                }
            }
            $nested->setQuery($boolQuery);
            $tmpQueries['should'][] = $nested;
        }

        $boolQuery = new BoolQuery();
        foreach ($tmpQueries as $clause => $clauseQueries) {
            $clauseMethod = 'add' . u($clause)->title()->toString();
            if ($clauseQueries != null) {
                $boolQuery->$clauseMethod($clauseQueries);
            }
        }
        $query = new Query($boolQuery);
        if ($highlight !== null) {
            $query->setHighlight($highlight);
        }
        $query->setSize($this->resultSize);

        return $query;
    }

    public static function createSearch(Query $query, array $indices): Search
    {
        $search = new MissingMethodFixedSearch();
        $search->addIndices($indices);
        if ($query != null) {
            $search->setQuery($query);
        }

        return $search;
    }

    protected function getIndices(): array
    {
        return $this->indices;
    }

    // The methods required by the interface

    public function addTerm(string $term, bool $required = false): void
    {
        if ($term != '') {
            $this->highlightTerms[] = $term;
        }
        $this->query[] = ['term' => $term, 'required' => $required];
        $this->logger->debug('Added term "' . $term . '" required? "' . json_encode($required, JSON_THROW_ON_ERROR) . '"');
    }

    public function addField(array $fields, string $term, bool $required): void
    {
        // Call our filter
        if (!$this->filter->match($fields, $term, $required)) {
            $this->logger->debug('Ignored fielded query for index "' . $fields[0] . '", field "' . $fields[1] . '", subfield "' . $fields[2] . '", term "' . $term . '" required? "' . json_encode($required, JSON_THROW_ON_ERROR) . '"');

            return;
        }
        if ($fields[0] != '' && !in_array($fields[0], $this->indices)) {
            $this->indices[] = $fields[0];
        }
        if ($term != '') {
            $this->highlightTerms[] = $term;
        }
        $this->query[] = ['index' => $fields[0], 'field' => $fields[1], 'subfield' => $fields[2], 'term' => $term, 'required' => $required];
        $this->logger->debug('Added fielded query for index "' . $fields[0] . '", field "' . $fields[1] . '", subfield "' . $fields[2] . '", term "' . $term . '" required? "' . json_encode($required, JSON_THROW_ON_ERROR) . '"');
    }

    public function getQuery(): object
    {
        $this->logger->debug('Building Query for: ', $this->getIndices());

        return self::createSearch($this->buildQuery(), $this->getIndices());
    }

    public function setFilter(SearchFilterInterface $filter): self
    {
        $this->filter = $filter;
        $this->parsed = false;

        return $this;
    }

    public function setQueryString(string $queryString): self
    {
        $this->queryStr = $queryString;
        $this->parsed = false;

        return $this;
    }

    public function setHighlight(bool $highlight): self
    {
        $this->highlight = $highlight;

        return $this;
    }

    public function getHighlight(): bool
    {
        return $this->highlight;
    }
}
