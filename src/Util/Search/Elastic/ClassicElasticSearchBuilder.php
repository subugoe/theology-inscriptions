<?php

declare(strict_types=1);

namespace App\Util\Search\Elastic;

use App\Util\Search\SearchQueryListener;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\QueryString;
use LogicException;
use Psr\Log\LoggerInterface;
use function Symfony\Component\String\u;

class ClassicElasticSearchBuilder extends ElasticSearchBuilder
{
    protected array $indexes = [];
    private bool $parsed = false;

    public function __construct(array $indicesMetadata, LoggerInterface $logger)
    {
        parent::__construct($indicesMetadata, $logger);
        $this->listener = new SearchQueryListener($this);
    }

    protected function getIndices(): array
    {
        if (!$this->parsed) {
            $this->parse();
        }

        return $this->indexes;
    }

    // Methods from SearchController

    protected static function parseQueryRemoveIndexes(string $query): string
    {
        return preg_replace('/\\w*_([\\w\\*\\.]*:)/', '$1', $query);
    }

    protected function buildQuery(): Query
    {
        $queryString = $this->queryStr;
        $highlight = null;
        if ($this->highlight) {
            $highlight = ElasticSearchBuilder::buildHighlight(['*']);
        }

        $queryIndices = $this->getIndices();
        if (strpos($queryString, '_') !== false) {
            $queryString = self::parseQueryRemoveIndexes($queryString);
        }
        $this->logger->debug('Building query for string "' . $queryString . '" on classic index array', $queryIndices);

        $nested = [];
        $alreadyNestedPaths = [];
        foreach ($queryIndices as $i => $fullfieldsList) {
            $must = [];
            foreach ($fullfieldsList as $fullfield) {
                // This filters only fields with subfields
                if (!empty($fullfield['subfield'])) {
                    $must[] = $fullfield['field'];
                }
            }
            if (!empty($this->indicesMetadata[$i]['nested'])) {
                // No duplicates allowed, must clauses have priority, at least we don't need to merge, if we had two clauses - this sucks
                foreach ($this->indicesMetadata[$i]['nested'] as $n) {
                    // If a nested field is in the query string, add nested to must, else to should
                    if (in_array($n, $must) && !in_array($n, $alreadyNestedPaths)) {
                        $nested['must'][] = ElasticSearchBuilder::buildNestedQuery($n, $this->buildBoolQuery($queryString, 'must'), $highlight);
                        $alreadyNestedPaths[] = $n;
                    }
                }
                foreach ($this->indicesMetadata[$i]['nested'] as $n) {
                    if (!in_array($n, $must) && !in_array($n, $alreadyNestedPaths)) {
                        // We need to rewrite queries, since the field name isn't relative to the nested objects name / path - who has invented this?
                        // If we search for the field 'name' in the object with path 'person', the name of the field would be 'person.name'
                        // A good API must not be redundant!
                        $replacements = ['/AND/' => 'OR'];
                        preg_match_all('/(\\w+:)+/', $queryString, $fields);
                        if (!empty($fields[1])) {
                            foreach ($fields[1] as $m) {
                                $replacements['/' . $m . '/'] = $n . '.' . $m;
                            }
                        }
                        // Ugly hack to append search terms - for highlighting
                        $append = ' ';
                        preg_match_all('/[\\w\\.]+:([^ "\']+|["\'][^"\']*["\'][\\S]*)/', $queryString, $values);
                        if (!empty($values[1])) {
                            foreach ($values[1] as $v) {
                                $append .= ' ' . $v;
                            }
                        }
                        $replacements['/$/'] = $append;
                        $nested['should'][] = ElasticSearchBuilder::buildNestedQuery($n, $this->buildBoolQuery($queryString, 'should', $replacements), $highlight);
                        $alreadyNestedPaths[] = $n;
                    }
                }
            }
        }

        $boolQuery = null;
        // TODO: We probably need to split the search string between the top levey Query object and the NestedQuery :(
        //       Currently only one of both is supported, the nested search has priority
        if (isset($nested['must'])) {
            $boolQuery = self::buildBoolQuery('');
        } else {
            $boolQuery = self::buildBoolQuery($queryString);
        }
        foreach ($nested as $type => $queries) {
            $method = 'add' . ucfirst($type);
            foreach ($queries as $q) {
                $boolQuery->$method($q);
            }
        }
        $this->logger->debug('Bool Query including nested documents:', $boolQuery->toArray());
        $query = new Query($boolQuery);
        if ($highlight !== null) {
            $query->setHighlight($highlight);
        }
        $query->setSize($this->resultSize);

        return $query;
    }

    protected function buildBoolQuery(string $queryString, string $clause = 'must', array $rewriteMap = [], bool $wildcard = true): BoolQuery
    {
        $clauses = ['must', 'should', 'filter', 'must_not'];
        if (!in_array($clause, $clauses)) {
            throw new LogicException('The boolean search clause has to be be one of the following [' . implode(',', $clauses) . '], see https://www.elastic.co/guide/en/elasticsearch/reference/6.8/query-dsl-bool-query.html');
        }

        $boolQuery = new BoolQuery();
        if ($queryString !== '') {
            $query = $queryString;
            if ($rewriteMap !== []) {
                foreach ($rewriteMap as $search => $replace) {
                    $this->logger->debug('Applying rewrite on "' . $query . '", searching for "' . $search . '" replacing with "' . $replace . '"');
                    $query = preg_replace($search, $replace, $query);
                }
                $this->logger->debug('Rewritten query string is "' . $query . '"');
            }

            $simpleQuery = new QueryString($query);
            $simpleQuery->setDefaultOperator('or');
            $simpleQuery->setParam('analyze_wildcard', $wildcard);
            $clauseMethod = 'add' . u($clause)->title()->toString();
            $boolQuery->$clauseMethod($simpleQuery);
        }

        return $boolQuery;
    }

    // The methods required by the interface

    public function addTerm(string $term, bool $required = false): void
    {
        // The old approach didn't care for single terms since they are used as part of the given query string
    }

    public function addField(array $fields, string $term, bool $required = false): void
    {
        // Call our filter
        if (!$this->filter->match($fields, $term, $required)) {
            $this->logger->debug('Ignored fielded query for index "' . $fields[0] . '", field "' . $fields[1] . '", subfield "' . $fields[2] . '", term "' . $term . '" required? "' . json_encode($required, JSON_THROW_ON_ERROR) . '"');

            return;
        }
        // This currently creates the old structure
        $index = $fields[0];
        $subfield = '';
        $fullfield = implode('.', array_slice($fields, 1, 2));
        $fieldDefinition = ['index' => $index, 'query' => $term, 'field' => $fields[1]];
        if (isset($fields[2])) {
            $subfield = $fields[2];
            $fieldDefinition = array_merge($fieldDefinition, ['subfield' => $subfield]);
        }
        if ($fullfield != $fields[1]) {
            $fieldDefinition = array_merge($fieldDefinition, ['fullfield' => $fullfield]);
        }
        $this->indexes[$index][] = $fieldDefinition;
        $this->logger->debug('Added fielded query for index "' . $fields[0] . '", field "' . $fields[1] . '", subfield "' . $subfield . '", term "' . $term . '" required? "' . json_encode($required, JSON_THROW_ON_ERROR) . '"');
    }

    public function getQuery(): object
    {
        return self::createSearch($this->buildQuery(), array_keys($this->getIndices()));
    }
}
