<?php

declare(strict_types=1);

namespace App\Util\Search;

class IndexMatchSearchFilter implements SearchFilterInterface
{
    protected array $indices;

    public function __construct(array $indices)
    {
        $this->indices = $indices;
    }

    public function match(array $fields, string $term, bool $required): bool
    {
        if ($this->indices === []) {
            return true;
        }
        if ($fields != null && $fields !== [] && isset($fields[0]) && in_array($fields[0], $this->indices)) {
            return true;
        }

        return false;
    }
}
