<?php

namespace App\Util;

interface DisplayNameInterface
{
    public function getDisplayName(): ?string;

    // This is needed to satisfy the form builder

    public function setDisplayName(?string $displayName): self;
}
