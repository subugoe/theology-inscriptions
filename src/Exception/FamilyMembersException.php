<?php

declare(strict_types=1);

namespace App\Exception;

use InvalidArgumentException;

class FamilyMembersException extends InvalidArgumentException
{
    public function __construct(string $msg)
    {
        parent::__construct($msg);
    }
}
