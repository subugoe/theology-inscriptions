<?php

declare(strict_types=1);

namespace App\Exception;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\ORMException;
use EasyCorp\Bundle\EasyAdminBundle\Exception\BaseException;
use EasyCorp\Bundle\EasyAdminBundle\Exception\ExceptionContext;
use Exception;

class InscriptionsDatabaseException extends BaseException
{
    public function __construct(array $parameters = [])
    {
        $exceptionContext = new ExceptionContext(
            'exception.insriptions_database',
            self::getErrorMessage($parameters['exception']),
            $parameters,
            500
        );

        parent::__construct($exceptionContext);
    }

    public static function getErrorMessage(Exception $e): string
    {
        $saveWorkMsg = 'Save your work in another programm like an editor and report this error to the development team, make sure to include the following lines:<br />';
        $msg = '<h1 class="exception">An database error occurred!</h1>';
        // Note: Since Exceptions can be subclasses of each other the order of the 'instanceof' checks matter here
        if ($e instanceof NotNullConstraintViolationException) {
            $msg .= '<b>The reference you tried to add doesn\'t exist anymore!</b><br />';
            $msg .= 'This might happen if a person or name has been deleted in another tab while editing this entry. Please review in a new tab and try again.<br />';
        } elseif ($e instanceof UniqueConstraintViolationException) {
            $msg .= '<b>The relation you tried to create already exists!</b><br />';
            $msg .= 'This might happen if you try to add a existing family relationship. Please review in a new tab and try again.<br />';
        } elseif ($e instanceof \Doctrine\DBAL\Exception) {
            $msg .= '<b>An unknown Database error occurred!</b><br />';
            $msg .= $saveWorkMsg;
        } elseif ($e instanceof ORMException) {
            $msg .= '<b>An unknown Database (ORM) error occurred!</b><br />';
            $msg .= $saveWorkMsg;
        } elseif ($e instanceof FamilyMembersException) {
            $msg .= '<b>An application error has happened:<br />';
            $msg .= $e->getMessage() . '</b><br />';
            $msg .= $saveWorkMsg;
        } else {
            $msg .= '<b>An unknown Database error occurred!</b><br />';
            $msg .= 'Save your work in another programm like an editor and report this error to the development team, make sure to include the following lines:<br />';
        }

        $msg .= 'Technical details (include this in your error report):<br />';

        return $msg . $e->getMessage();
    }
}
