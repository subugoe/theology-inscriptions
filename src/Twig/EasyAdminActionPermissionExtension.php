<?php

declare(strict_types=1);

namespace App\Twig;

use EasyCorp\Bundle\EasyAdminBundle\Configuration\ConfigManager;
use EasyCorp\Bundle\EasyAdminBundle\Router\EasyAdminRouter;
use EasyCorp\Bundle\EasyAdminBundle\Security\AuthorizationChecker;
use EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Security\Http\Logout\LogoutUrlGenerator;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\TwigFunction;

class EasyAdminActionPermissionExtension extends EasyAdminTwigExtension
{
    private $configManager;

    public function __construct(ConfigManager $configManager, PropertyAccessorInterface $propertyAccessor, EasyAdminRouter $easyAdminRouter, bool $debug, ?LogoutUrlGenerator $logoutUrlGenerator, TranslatorInterface $translator, AuthorizationChecker $authorizationChecker)
    {
        parent::__construct($configManager, $propertyAccessor, $easyAdminRouter, $debug, $logoutUrlGenerator, $translator, $authorizationChecker);
        $this->configManager = $configManager;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('easyadmin_action_is_permitted', [$this, 'isActionPermitted'], ['needs_context' => true]),
        ];
    }

    public function isActionPermitted($context, $view, $action, $entityName): bool
    {
        if (array_key_exists('entity_config', $context)) {
            $entityConfig = $context['entity_config'];
        } else {
            $entityConfig = $this->configManager->getEntityConfig($entityName);
        }

        if ($action == 'delete') {
            $action = 'edit';
        }
        $requiredPermission = $entityConfig[$action]['item_permission'];
        if (is_null($requiredPermission)) {
            return true;
        }

        if (!is_null($context['app']->getUser()) && in_array($requiredPermission, $context['app']->getUser()->getRoles())) {
            return true;
        }

        return false;
    }
}
