<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\TwigTest;

class TypeExtension extends AbstractExtension
{
    public function getTests()
    {
        return [
            new TwigTest('bool', [$this, 'isBool']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('bool_val', [$this, 'boolValue']),
        ];
    }

    public function isBool($var): bool
    {
        return is_bool($var);
    }

    public function boolValue(bool $var): string
    {
        return json_encode($var, JSON_THROW_ON_ERROR);
    }
}
