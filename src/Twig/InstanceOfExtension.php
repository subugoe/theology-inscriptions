<?php

declare(strict_types=1);

// Inspired by https://gist.github.com/adrienbrault/7045544

namespace App\Twig;

use ReflectionClass;
use Twig\Extension\AbstractExtension;
use Twig\TwigTest;

class InstanceOfExtension extends AbstractExtension
{
    public function getTests()
    {
        return [
            new TwigTest('instanceof', [$this, 'isInstanceOf']),
        ];
    }

    public function isInstanceOf(object $var, string $instance): bool
    {
        $reflexionClass = new ReflectionClass($instance);

        return $reflexionClass->isInstance($var);
    }
}
