<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigTest;

class ArrayExtension extends AbstractExtension
{
    public function getTests()
    {
        return [
            new TwigTest('hash', [$this, 'isHash']),
            new TwigTest('nestedArray', [$this, 'nestedArray']),
        ];
    }

    // See https://stackoverflow.com/a/6968499
    public function isHash(array $array): bool
    {
        return $array !== array_values($array);
    }

    public static function nestedArray(array $array): bool
    {
        foreach ($array as $val) {
            if (is_array($val)) {
                return true;
            }
        }

        return false;
    }
}
