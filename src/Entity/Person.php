<?php

declare(strict_types=1);

namespace App\Entity;

use App\Util\DisplayNameInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @ORM\Table(name="person", indexes={@ORM\Index(name="fullname_idx", columns={"fullname"})})
 */
class Person implements DisplayNameInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $gender = null;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private ?string $fullname = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Names", inversedBy="person")
     * @ORM\OrderBy({"name"= "ASC"})
     *
     * @var Collection
     */
    private Collection $names;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NameTypes", inversedBy="person")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?NameTypes $type = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $profession = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $ethnicity = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $remarks = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Religions", inversedBy="person")
     */
    private ?Religions $religion = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FamilyMembers", mappedBy="person", orphanRemoval=true)
     *
     * @var Collection
     */
    private Collection $familyMembers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FamilyMembers", mappedBy="fromPerson", orphanRemoval=true, cascade={"persist"})
     *
     * @var Collection
     */
    private Collection $FamilyMembersOfPerson;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Inscriptions", mappedBy="persons")
     */
    private Collection $inscriptions;

    public static string $idPrefix = 'PID';

    public function __construct()
    {
        $this->familyMembers = new ArrayCollection();
        $this->FamilyMembersOfPerson = new ArrayCollection();
        $this->names = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getType(): ?NameTypes
    {
        return $this->type;
    }

    public function setType(NameTypes $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(?string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }

    public function getReligion(): ?Religions
    {
        return $this->religion;
    }

    public function setReligion(?Religions $religion): self
    {
        $this->religion = $religion;

        return $this;
    }

    public function __toString(): string
    {
        if ($this->fullname != null) {
            return $this->fullname;
        }

        return 'New Person';
    }

    public function getPrefixedName(): string
    {
        if ($this->fullname != null && $this->id != null) {
            return '(PID:' . $this->id . ') ' . $this->fullname;
        }

        return 'New Person';
    }

    /**
     * @return Collection|FamilyMembers[]
     *
     * @psalm-return Collection<int, FamilyMembers>|array<FamilyMembers>
     */
    public function getFamilyMembers()
    {
        return $this->familyMembers;
    }

    public function addFamilyMember(FamilyMembers $familyMember): self
    {
        if (!$this->familyMembers->contains($familyMember)) {
            $this->familyMembers[] = $familyMember;
            $familyMember->setPerson($this);
        }

        return $this;
    }

    public function removeFamilyMember(FamilyMembers $familyMember): self
    {
        if ($this->familyMembers->contains($familyMember)) {
            $this->familyMembers->removeElement($familyMember);
            // set the owning side to null (unless already changed)
            if ($familyMember->getPerson() === $this) {
                $familyMember->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FamilyMembers[]
     *
     * @psalm-return Collection<int, FamilyMembers>|array<FamilyMembers>
     */
    public function getFamilyMembersOfPerson()
    {
        return $this->FamilyMembersOfPerson;
    }

    public function addFamilyMembersOfPerson(FamilyMembers $familyMembersOfPerson): self
    {
        if (!$this->FamilyMembersOfPerson->contains($familyMembersOfPerson)) {
            $this->FamilyMembersOfPerson[] = $familyMembersOfPerson;
            $familyMembersOfPerson->setFromPerson($this);
        }

        return $this;
    }

    public function removeFamilyMembersOfPerson(FamilyMembers $familyMembersOfPerson): self
    {
        if ($this->FamilyMembersOfPerson->contains($familyMembersOfPerson)) {
            $this->FamilyMembersOfPerson->removeElement($familyMembersOfPerson);
            // set the owning side to null (unless already changed)
            if ($familyMembersOfPerson->getFromPerson() === $this) {
                $familyMembersOfPerson->setFromPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getNames(): Collection
    {
        return $this->names;
    }

    public function addName(Names $name): self
    {
        if (!$this->names->contains($name)) {
            $this->names[] = $name;
        }

        return $this;
    }

    public function removeName(Names $name): self
    {
        if ($this->names->contains($name)) {
            $this->names->removeElement($name);
        }

        return $this;
    }

    public function getProfession(): ?string
    {
        return $this->profession;
    }

    public function setProfession(?string $profession): self
    {
        $this->profession = $profession;

        return $this;
    }

    public function getEthnicity(): ?string
    {
        return $this->ethnicity;
    }

    public function setEthnicity(?string $ethnicity): self
    {
        $this->ethnicity = $ethnicity;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscriptions $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->addPerson($this);
        }

        return $this;
    }

    public function removeInscription(Inscriptions $inscription): self
    {
        if ($this->inscriptions->contains($inscription)) {
            $this->inscriptions->removeElement($inscription);
            $inscription->removePerson($this);
        }

        return $this;
    }

    public function getIdPrefixName(): string
    {
        $str = '(' . self::$idPrefix . ':';
        if ($this->id != null) {
            $str .= $this->id;
        }
        $str .= ') ';
        if ($this->fullname != null) {
            $str .= $this->fullname;
        }

        return $str;
    }

    public function getDisplayName(): ?string
    {
        return $this->fullname;
    }

    public function setDisplayName(?string $displayName): self
    {
        return $this;
    }
}
