<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InscriptionsmaterialRepository")
 */
class Inscriptionsmaterial
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $material;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Inscriptions", mappedBy="material")
     */
    private Collection $inscriptions;

    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getMaterial(): ?string
    {
        return $this->material;
    }

    public function setMaterial(string $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function __toString()
    {
        return $this->material;
    }

    /**
     * @return Collection|Inscriptions[]
     *
     * @psalm-return Collection<int, Inscriptions>|array<Inscriptions>
     */
    public function getInscriptions()
    {
        return $this->inscriptions;
    }

    public function addRemark(Inscriptions $inscriptions): self
    {
        if (!$this->inscriptions->contains($inscriptions)) {
            $this->inscriptions[] = $inscriptions;
            $inscriptions->setMaterial($this);
        }

        return $this;
    }

    public function removeInscriptions(Inscriptions $inscriptions): self
    {
        if ($this->inscriptions->contains($inscriptions)) {
            $this->inscriptions->removeElement($inscriptions);
            // set the owning side to null (unless already changed)
            if ($inscriptions->getMaterial() === $this) {
                $inscriptions->setMaterial(null);
            }
        }

        return $this;
    }
}
