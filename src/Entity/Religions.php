<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReligionsRepository")
 */
class Religions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $religion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Person", mappedBy="religion")
     *
     * @var Collection
     */
    private Collection $person;

    public function __construct()
    {
        $this->person = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getReligion(): string
    {
        return $this->religion;
    }

    public function setReligion(string $religion): self
    {
        $this->religion = $religion;

        return $this;
    }

    /**
     * @return Collection|Person[]
     *
     * @psalm-return Collection<int, Person>|array<Person>
     */
    public function getPerson()
    {
        return $this->person;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
            $person->setReligion($this);
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
            // set the owning side to null (unless already changed)
            if ($person->getReligion() === $this) {
                $person->setReligion(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->religion;
    }
}
