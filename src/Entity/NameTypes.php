<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NameTypesRepository")
 */
class NameTypes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $nametype;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Person", mappedBy="type")
     *
     * @var Collection
     */
    private Collection $person;

    public function __construct()
    {
        $this->person = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNametype(): ?string
    {
        return $this->nametype;
    }

    public function setNametype(string $nametype): self
    {
        $this->nametype = $nametype;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPerson(): Collection
    {
        return $this->person;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
            $person->setType($this);
        }

        return $this;
    }

    public function removeperson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
        }

        return $this;
    }

    public function __toString(): ?string
    {
        return $this->nametype;
    }
}
