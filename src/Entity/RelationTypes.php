<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RelationTypesRepository")
 */
class RelationTypes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public string $relationType = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    public string $propertyName = '';

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\RelationTypes")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private ?RelationTypes $opposite = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getRelationType(): ?string
    {
        return $this->relationType;
    }

    public function setRelationType(string $relationType): self
    {
        $this->relationType = $relationType;

        return $this;
    }

    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    public function __toString(): ?string
    {
        return $this->relationType;
    }

    public function getOpposite(): ?RelationTypes
    {
        return $this->opposite;
    }

    public function getOppositeID(): ?int
    {
        if ($this->opposite != null) {
            return $this->opposite->id;
        }

        return null;
    }
}
