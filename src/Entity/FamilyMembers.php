<?php

declare(strict_types=1);

namespace App\Entity;

use App\Exception\FamilyMembersException;
use App\Util\DisplayNameInterface;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FamilyMembersRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="family_members", uniqueConstraints={@ORM\UniqueConstraint(name="relationship", columns={"from_person_id", "relation_type_id", "person_id"})})
 * @ORM\EntityListeners({"App\EntityListener\FamilyMembersListener"})
 */
class FamilyMembers implements DisplayNameInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="boolean", options={"default"=false})
     */
    private bool $generated = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="FamilyMembersOfPerson")
     * @ORM\JoinColumn(nullable=false)
     * @ORM\OrderBy({"fullname"= "ASC"})
     */
    private ?Person $fromPerson = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RelationTypes")
     * @ORM\JoinColumn(nullable=false)
     */
    public ?RelationTypes $relationType = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="familyMembers")
     * @ORM\JoinColumn(nullable=false)
     * @ORM\OrderBy({"fullname"= "ASC"})
     */
    private ?Person $person = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $remarks = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\FamilyMembers", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private ?FamilyMembers $linked = null;

    /**
     * This variable contains the primary relation for generated.
     *
     * @var FamilyMembers
     */
    private ?FamilyMembers $primary = null;

    // No constructor overloading...
    public static function newFamilyMembers(Person $fromPerson, ?RelationTypes $relationType, Person $person): FamilyMembers
    {
        $fm = new FamilyMembers();
        $fm->fromPerson = $fromPerson;
        $fm->relationType = $relationType;
        $fm->person = $person;

        return $fm;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRelationType(): ?RelationTypes
    {
        return $this->relationType;
    }

    public function setRelationType(RelationTypes $relationType): self
    {
        $this->relationType = $relationType;
        $this->addOpposite();

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;
        $this->addOpposite();

        return $this;
    }

    public function getFromPerson(): ?Person
    {
        return $this->fromPerson;
    }

    public function setFromPerson(?Person $fromPerson): self
    {
        $this->fromPerson = $fromPerson;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(?string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }

    public function setLinked(?FamilyMembers $linked): self
    {
        $this->linked = $linked;

        return $this;
    }

    public function getLinked(): ?FamilyMembers
    {
        return $this->linked;
    }

    public function __toString(): string
    {
        if ($this->person != null && $this->relationType != null && $this->fromPerson != null) {
            return $this->fromPerson->getIdPrefixName() . ' ' . $this->relationType . ' ' . $this->person->getIdPrefixName();
        }

        throw new InvalidArgumentException('Person isn\'t set!');
    }

    /**
     * @psalm-suppress TypeDoesNotContainNull
     */
    public function getGenerated(): bool
    {
        /* FOSElastica doen't seem to respect the doctrine default of 'false' here.
           Since PHPStan doesn't like this, we set null_value: false in the index definition
        if ($this->generated === null) {
            return false;
        }
        */

        return $this->generated;
    }

    public function setGenerated(?bool $generated = true): self
    {
        if ($generated == null) {
            return $this;
        }
        $this->generated = $generated;

        return $this;
    }

    public function getPrimary(): ?FamilyMembers
    {
        if ($this->generated) {
            return $this->primary;
        }

        return null;
    }

    public function setPrimary(FamilyMembers $primary): self
    {
        $this->primary = $primary;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * Use this to create and update a new linked FamilyMembers.
     */
    public function addOpposite(): FamilyMembers
    {
        if (!$this->generated) {
            $opposite = self::generateOpposite($this);
            if ($opposite != null && $this->linked != null) {
                $rel = $opposite->getRelationType();
                if ($rel != null) {
                    $this->linked->setRelationType($rel);
                    $this->linked->setFromPerson($opposite->getFromPerson());
                    $this->linked->setGenerated(true);
                }
            } elseif ($opposite == null && $this->linked != null) {
                $this->setLinked(null);
            } else {
                $this->setLinked($opposite);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * This checks if person and fromPerson are identical
     */
    public function checkPersons(): void
    {
        if ($this->person === $this->fromPerson) {
            throw new FamilyMembersException('Person can\'t be relateted with the same person!');
        }
    }

    // The returned FamilyMembers is always marked as 'generated'
    public static function generateOpposite(FamilyMembers $members): ?FamilyMembers
    {
        if ($members->relationType != null) {
            $opposite = $members->relationType->getOpposite();
            if ($opposite != null && $members->person != null && $members->fromPerson != null) {
                $oppositeMember = self::newFamilyMembers($members->person, $opposite, $members->fromPerson);
                $oppositeMember->setGenerated(true);

                return $oppositeMember;
            }
        }

        return null;
    }

    public function getDisplayName(): ?string
    {
        if ($this->person != null && $this->fromPerson != null && $this->relationType != null) {
            return $this->fromPerson . ' ' . $this->relationType . ' ' . $this->person;
        }

        return 'New Person';
    }

    public function setDisplayName(?string $displayName): self
    {
        return $this;
    }

    public function generateCriteria(): ?array
    {
        if ($this->fromPerson != null && $this->relationType != null && $this->person != null) {
            return ['fromPerson' => $this->fromPerson->getId(), 'relationType' => $this->relationType->getId(), 'person' => $this->person->getId()];
        }

        return null;
    }
}
