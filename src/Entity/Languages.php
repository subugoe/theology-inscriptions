<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LanguagesRepository")
 */
class Languages
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     */
    private string $language;

    /**
     * @ORM\Column(type="boolean", options={"default"=false})
     */
    private bool $inscriptionLanguage = false;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Names", mappedBy="language")
     *
     * @var Collection
     */
    private Collection $names;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Inscriptions", mappedBy="languages")
     *
     * @var Collection
     */
    private Collection $inscriptions;

    public function __construct()
    {
        $this->names = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function __toString(): string
    {
        return $this->language;
    }

    /**
     * @return Collection
     */
    public function getNames(): Collection
    {
        return $this->names;
    }

    public function addName(Names $name): self
    {
        if (!$this->names->contains($name)) {
            $this->names[] = $name;
            $name->addLanguage($this);
        }

        return $this;
    }

    public function removeName(Names $name): self
    {
        if ($this->names->contains($name)) {
            $this->names->removeElement($name);
            $name->removeLanguage($this);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscriptions $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->addLanguage(self::getInscriptionlanguages($this));
        }

        return $this;
    }

    public function removeInscription(Inscriptions $inscription): self
    {
        if ($this->inscriptions->contains($inscription)) {
            $this->inscriptions->removeElement($inscription);
            $inscription->removeLanguage(self::getInscriptionlanguages($this));
        }

        return $this;
    }

    private static function getInscriptionlanguages(Languages $lang): Inscriptionlanguages
    {
        $iLang = new Inscriptionlanguages();
        $iLang->setLanguage($lang->getLanguage());

        return $iLang;
    }
}
