<?php

declare(strict_types=1);

namespace App\Entity;

use App\Util\DisplayNameInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InscriptionsRepository")
 */
class Inscriptions implements DisplayNameInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $siglum = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Inscriptionlanguages", inversedBy="inscriptions")
     *
     * @var Collection
     */
    private Collection $languages;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Regions", inversedBy="inscriptions")
     */
    private ?Regions $region = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $subregion = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $village = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $date = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Dates", inversedBy="inscriptions")
     */
    private ?Dates $period = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Inscriptionsgenre", inversedBy="inscriptions")
     */
    private ?Inscriptionsgenre $genre = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Inscriptionsmaterial", inversedBy="inscriptions")
     */
    private ?Inscriptionsmaterial $material = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $remarks = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", inversedBy="inscriptions")
     * @ORM\OrderBy({"fullname"= "ASC"})
     */
    private Collection $persons;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Names", inversedBy="inscriptions")
     * @ORM\OrderBy({"name"= "ASC"})
     */
    private Collection $names;

    public function __construct()
    {
        $this->languages = new ArrayCollection();
        $this->persons = new ArrayCollection();
        $this->names = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiglum(): ?string
    {
        return $this->siglum;
    }

    public function setSiglum(string $siglum): self
    {
        $this->siglum = $siglum;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getLanguages(): Collection
    {
        return $this->languages;
    }

    public function addLanguage(Inscriptionlanguages $language): self
    {
        if (!$this->languages->contains($language)) {
            $this->languages[] = $language;
        }

        return $this;
    }

    public function removeLanguage(Inscriptionlanguages $language): self
    {
        if ($this->languages->contains($language)) {
            $this->languages->removeElement($language);
        }

        return $this;
    }

    public function getRegion(): ?Regions
    {
        return $this->region;
    }

    public function setRegion(?Regions $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getSubregion(): ?string
    {
        return $this->subregion;
    }

    public function setSubregion(?string $subregion): self
    {
        $this->subregion = $subregion;

        return $this;
    }

    public function getVillage(): ?string
    {
        return $this->village;
    }

    public function setVillage(?string $village): self
    {
        $this->village = $village;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPeriod(): ?Dates
    {
        return $this->period;
    }

    public function setPeriod(?Dates $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getGenre(): ?Inscriptionsgenre
    {
        return $this->genre;
    }

    public function setGenre(?Inscriptionsgenre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getMaterial(): ?Inscriptionsmaterial
    {
        return $this->material;
    }

    public function setMaterial(?Inscriptionsmaterial $material): self
    {
        $this->material = $material;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(?string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getNames(): Collection
    {
        return $this->names;
    }

    public function addName(Names $name): self
    {
        if (!$this->names->contains($name)) {
            $this->names[] = $name;
        }

        return $this;
    }

    public function removeName(Names $name): self
    {
        if ($this->names->contains($name)) {
            $this->names->removeElement($name);
        }

        return $this;
    }

    public function __toString(): string
    {
        if ($this->siglum != null) {
            return $this->siglum;
        }

        return 'New Inscript';
    }

    public function getDisplayName(): ?string
    {
        return $this->siglum;
    }

    public function setDisplayName(?string $displayName): self
    {
        return $this;
    }

    // these get filled during indexing by App\Util\Transformer\GeonamesEnrichTransformer
    public function getGeonamesCoordinates(): ?array
    {
        return [];
    }
}
