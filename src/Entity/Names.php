<?php

declare(strict_types=1);

namespace App\Entity;

use App\Util\DisplayNameInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NamesRepository")
 * @ORM\Table(name="names", indexes={@ORM\Index(name="name_idx", columns={"name"})})
 */
class Names implements DisplayNameInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private ?string $name = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Languages", inversedBy="names")
     */
    private Collection $language;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $philologicalAnalysis = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $remarks = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", mappedBy="names")
     *
     * @var Collection
     */
    private Collection $person;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Inscriptions", mappedBy="names")
     *
     * @var Collection
     */
    private Collection $inscriptions;

    public function __construct()
    {
        $this->language = new ArrayCollection();
        $this->person = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getLanguage(): Collection
    {
        return $this->language;
    }

    public function addLanguage(Languages $language): self
    {
        if (!$this->language->contains($language)) {
            $this->language[] = $language;
        }

        return $this;
    }

    public function removeLanguage(Languages $language): self
    {
        if ($this->language->contains($language)) {
            $this->language->removeElement($language);
        }

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(?string $remarks): self
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPerson(): Collection
    {
        return $this->person;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
            $person->addName($this);
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
            $person->removeName($this);
        }

        return $this;
    }

    public function __toString()
    {
        if ($this->name != null) {
            return $this->name;
        }

        return 'New Name';
    }

    /**
     * @return Collection
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscriptions $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->addName($this);
        }

        return $this;
    }

    public function removeInscription(Inscriptions $inscription): self
    {
        if ($this->inscriptions->contains($inscription)) {
            $this->inscriptions->removeElement($inscription);
            $inscription->removeName($this);
        }

        return $this;
    }

    public function getPhilologicalAnalysis(): ?string
    {
        return $this->philologicalAnalysis;
    }

    public function setPhilologicalAnalysis(?string $philologicalAnalysis): self
    {
        $this->philologicalAnalysis = $philologicalAnalysis;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->name;
    }

    public function setDisplayName(?string $displayName): self
    {
        return $this;
    }
}
