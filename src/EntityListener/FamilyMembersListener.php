<?php

declare(strict_types=1);

namespace App\EntityListener;

use App\Entity\FamilyMembers;
use App\Repository\FamilyMembersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping\PostLoad;

class FamilyMembersListener
{
    private ?EntityManagerInterface $om = null;
    private FamilyMembersRepository $repository;

    /**
     * @PostLoad
     */
    public function postLoadHandler(FamilyMembers $fm, LifecycleEventArgs $event): void
    {
        if ($this->om === null) {
            $this->om = $event->getEntityManager();
            $repository = $this->om->getRepository(FamilyMembers::class);
            if ($repository instanceof FamilyMembersRepository) {
                $this->repository = $repository;
            }
        }
        $primary = $this->repository->findOppositeFamilyMembers($fm);
        if ($primary !== null && $fm->getGenerated()) {
            $fm->setPrimary($primary);
        }
    }
}
