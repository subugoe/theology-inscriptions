<?php

// Ispired by http://blog.code4hire.com/2017/07/loading-sql-sequentially-with-doctrine-fixtures-in-symfony-project/

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Finder\Finder;

class InscriptionsDataFixtures extends Fixture
{
    /**
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $finder = new Finder();
        $finder->in(__DIR__ . '/../../docker/app');
        $finder->name('*.sql');
        $finder->files()->sortByName()->reverseSorting();

        foreach ($finder as $file) {
            echo "Importing: {$file->getBasename()} " . PHP_EOL;
            $sql = $file->getContents();
            if ($manager instanceof EntityManagerInterface) {
                $manager->getConnection()->exec($sql);
                $manager->flush();
            }

            throw new Exception("Couldn't get database connection from 'ObjectManager'");
        }
    }
}
