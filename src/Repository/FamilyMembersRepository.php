<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\FamilyMembers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FamilyMembers|null find($id, $lockMode = null, $lockVersion = null)
 * @method FamilyMembers|null findOneBy(array $criteria, array $orderBy = null)
 * @method FamilyMembers[]    findAll()
 * @method FamilyMembers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FamilyMembersRepository extends ServiceEntityRepository
{
    public static string $clazz = FamilyMembers::class;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, self::$clazz);
    }

    public function findFamilyMembersById(int $id): ?FamilyMembers
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function findOppositeFamilyMembers(FamilyMembers $fm): ?FamilyMembers
    {
        $opposite = self::$clazz::generateOpposite($fm);
        if ($opposite != null) {
            $criteria = $opposite->generateCriteria();
            if ($criteria != null) {
                return $this->findOneBy($criteria);
            }
        }

        return null;
    }
}
