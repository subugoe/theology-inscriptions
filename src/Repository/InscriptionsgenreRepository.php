<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Inscriptionsgenre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Inscriptionsgenre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inscriptionsgenre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inscriptionsgenre[]    findAll()
 * @method Inscriptionsgenre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InscriptionsgenreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inscriptionsgenre::class);
    }
}
