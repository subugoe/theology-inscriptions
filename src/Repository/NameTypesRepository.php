<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\NameTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method NameTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method NameTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method NameTypes[]    findAll()
 * @method NameTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NameTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NameTypes::class);
    }
}
