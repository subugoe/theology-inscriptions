<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\RelationTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RelationTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelationTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelationTypes[]    findAll()
 * @method RelationTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelationTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RelationTypes::class);
    }
}
