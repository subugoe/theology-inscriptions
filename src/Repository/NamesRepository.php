<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Names;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Names|null find($id, $lockMode = null, $lockVersion = null)
 * @method Names|null findOneBy(array $criteria, array $orderBy = null)
 * @method Names[]    findAll()
 * @method Names[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NamesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Names::class);
    }

    public function findAll()
    {
        return $this->findBy([], ['name' => 'ASC']);
    }
}
