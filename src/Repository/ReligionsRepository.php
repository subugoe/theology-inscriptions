<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Religions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Religions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Religions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Religions[]    findAll()
 * @method Religions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReligionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Religions::class);
    }
}
