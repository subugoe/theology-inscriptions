<?php

declare(strict_types=1);

namespace App\Command\Util;

use App\Entity\FamilyMembers;

class Update
{
    public string $type;
    public FamilyMembers $familyMembers;
    public string $desc;

    public function __construct(string $type, FamilyMembers $familyMembers, string $desc)
    {
        $this->type = $type;
        $this->familyMembers = $familyMembers;
        $this->desc = $desc;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getDesc(): string
    {
        return $this->desc;
    }

    public function getFamilyMembers(): FamilyMembers
    {
        return $this->familyMembers;
    }
}
