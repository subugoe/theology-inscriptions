<?php

declare(strict_types=1);
// Inspired by https://github.com/FriendsOfSymfony/FOSUserBundle/blob/master/Command/

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DeleteUserCommand extends Command
{
    protected static $defaultName = 'inscriptions:delete-user';

    private EntityManagerInterface $em;
    private UserPasswordEncoderInterface $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct();
        $this->em = $em;
        $this->encoder = $encoder;
    }

    public function setEntityManager(EntityManagerInterface $em): self
    {
        $this->em = $em;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setName('inscriptions:delete-user')
            ->setDescription('Delete a user.')
            ->setDefinition([
                new InputArgument('username', InputArgument::REQUIRED, 'The username'),
            ])
            ->setHelp(<<<'EOT'
The <info>inscriptions:delete-user</info> command creates a user:
  <info>php %command.full_name% admin</info>
This interactive shell will ask you for an user name to delete - note, there isn't any further questioning!.
You can alternatively specify the user name as argument:
  <info>php %command.full_name% admin</info>
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string */
        $username = $input->getArgument('username');
        $this->delete($username);
        $output->writeln(sprintf('Deleted user <comment>%s</comment>', $username));

        return 0;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];
        if (!$input->getArgument('username')) {
            $question = new Question('Please choose a username:');
            $question->setValidator(function (string $username) {
                if ($username === '' || $username === '0') {
                    throw new Exception('Username can not be empty');
                }

                return $username;
            });
            $questions['username'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    //See https://gist.github.com/tip2tail/1722f7a96123938a563ffdb00a4ffe79
    public function delete(string $username): void
    {
        $userEntity = $this->em->getRepository(User::class)->findOneBy(['username' => $username]);
        if ($userEntity != null) {
            $this->em->remove($userEntity);
            $this->em->flush();
        }

        throw new InvalidArgumentException('User ' . $username . " doesn't exist!");
    }
}
