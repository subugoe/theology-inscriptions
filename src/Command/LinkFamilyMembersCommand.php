<?php

declare(strict_types=1);

namespace App\Command;

use App\Command\Util\Update;
use App\Entity\FamilyMembers;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnexpectedValueException;

class LinkFamilyMembersCommand extends Command
{
    protected static $defaultName = 'inscriptions:link-family-members';
    private EntityManagerInterface $em;
    protected LoggerInterface $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $logger;
    }

    public function setEntityManager(EntityManagerInterface $em): self
    {
        $this->em = $em;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Generate linked FamiliyRelations from existing ones')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Changes the database.')
            ->addOption('dump', 'd', InputOption::VALUE_NONE, 'Dumps the proposed changes.')
            ->setHelp(<<<'EOT'
The <info>inscriptions:link-family-members</info> command checks entries in the FamilyRalations table:
  <info>php %command.full_name% -d</info>
There is no interactive mode.
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $ui = new SymfonyStyle($input, $output);
        $ui->caution('This operation should not be executed in a production environment!');
        $force = (bool) $input->getOption('force');
        $dump = (bool) $input->getOption('dump');

        $repository = $this->em->getRepository(FamilyMembers::class);
        // Loop thru all FamilyMembers
        $members = $repository->findBy([], ['id' => 'DESC']);

        foreach ($members as $fm) {
            /** @var array<Update> */
            $updates = [];
            $linkedFm = $fm->getLinked();
            $oppositeFm = $this->findOppositeFamilyMembers($fm);
            $fmId = $fm->getId();
            // If FamilMembers is generated, check if it's linked somewhere, if not fix, if not possible delete
            if ($fm->getGenerated()) {
                $linkedBy = $repository->findOneBy(['linked' => $fm->getId()]);
                $delete = false;
                if ($linkedBy != null) {
                    $this->logger->info($fm->__toString() . 'is a valid generated relationship.');
                // Check of opposite exists, corner case valid from mid to end of January 2020
                } elseif ($oppositeFm != null) {
                    $this->logger->info($fm->__toString() . 'is a generated relationship missing a reference from primary.');
                    if ($oppositeFm->getGenerated() != null) {
                        $this->logger->info($fm->__toString() . 'is a generated relationship and is linked by another generated, deleting!');
                        $delete = true;
                    } else {
                        $desc = $fm->__toString() . 'is a generated relationship and ' . $oppositeFm->__toString() . ' is the primary relation, linking them';
                        $this->logger->info($desc);
                        // Set link to this on opposite
                        $oppositeFm->setLinked($fm);
                        $updates[] = new Update('change', $oppositeFm, $desc);
                    }
                } else {
                    $this->logger->info($fm->__toString() . '(' . $fmId . ') is a generated relationship and no possible target to link from found, deleting!');
                    $delete = true;
                }
                // Delete
                if ($delete) {
                    $updates[] = new Update('delete', $fm, 'Generated relationship ' . $fmId . ' which isn\'t linked will be removed.');
                }

                // If not generated, check if linked Member is set and if yes, check if it exists (should be covered by database)
            } elseif (!$fm->getGenerated() && $linkedFm != null) {
                $linked = $repository->findOneBy(['id' => $linkedFm->getId()]);
                if ($linked == null) {
                    $this->logger->info($fm->__toString() . ' has a linked FamilyMembers relation that doesn\'t exist!');

                    throw new UnexpectedValueException('This case should be prevented by the database!');
                }
                // If not generated, check id opposite exists, if so, make the one with the smaller id the primary relationship
                // Othewise create a new one
            } elseif (!$fm->getGenerated() && $linkedFm == null) {
                $this->logger->info($fm->__toString() . 'doesn\'t have a opposite relationship defined, searching or creating one.');

                if ($oppositeFm != null) {
                    // Find which id is lower
                    $desc = '';
                    if ($fm->getId() < $oppositeFm->getId()) {
                        $oppositeFm->setGenerated(true);
                        $fm->setLinked($oppositeFm);
                        $desc .= 'Relation \'' . $fm->__toString() . '\' (' . $fmId . ') is older then ' . $oppositeFm->__toString() . '. ';
                    } else {
                        $fm->setGenerated(true);
                        $oppositeFm->setLinked($fm);
                        $desc .= 'Relation \'' . $oppositeFm->__toString() . '\' is older then ' . $fm->__toString() . ' (' . $fmId . '). ';
                    }
                    // Save changes
                    $updates[] = new Update('change', $fm, $desc);
                    $updates[] = new Update('change', $oppositeFm, $desc);

                    $this->logger->info($fm->__toString() . '(' . $fmId . ') and ' . $oppositeFm->__toString() . '(' . $fmId . ') linked.');
                } else {
                    // Just generate a opposite
                    $fm->addOpposite();
                    $generatedOpposite = $fm->getLinked();
                    if ($generatedOpposite != null) {
                        $updates[] = new Update('change', $fm, 'Relation ' . $fmId . ' without opposite found, the follwing will be generated: ' . $generatedOpposite->__toString());
                    } else {
                        $this->logger->info('Couldn\'t generate opposite for ' . $fm->__toString());
                    }
                }
            }
            // Execute changes and delete's
            foreach ($updates as $update) {
                $change = $update->getType();
                $fm = $update->getFamilyMembers();
                if ($dump) {
                    $msg = 'Relation ' . $fm->__toString() . ' would be ' . $change . 'd.';
                    if ($change === 'change') {
                        $ui->text($msg);
                    } elseif ($change === 'delete') {
                        $ui->caution($msg);
                    }
                    $ui->text($msg);
                    $ui->text("\tAdditional notes: " . $update->getDesc());
                    $ui->newLine();
                }
                if ($force) {
                    if ($change === 'change') {
                        $this->em->persist($fm);
                    } elseif ($change === 'delete') {
                        $this->em->remove($fm);
                    }
                    $this->em->flush();
                }
            }
        }

        return 0;
    }

    public function findOppositeFamilyMembers(FamilyMembers $fm): ?FamilyMembers
    {
        $repository = $this->em->getRepository(FamilyMembers::class);
        $opposite = FamilyMembers::generateOpposite($fm);

        if ($opposite != null) {
            $criteria = $opposite->generateCriteria();
            if ($criteria != null) {
                return $repository->findOneBy($criteria);
            }
        }

        return null;
    }
}
