<?php

declare(strict_types=1);
// Inspired by https://github.com/FriendsOfSymfony/FOSUserBundle/blob/master/Command/

/*
 * This file was part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 * (c) SUB Göttingen
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChangePasswordCommand extends Command
{
    protected static $defaultName = 'inscriptions:change-password';

    private EntityManagerInterface $em;
    private UserPasswordEncoderInterface $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct();
        $this->em = $em;
        $this->encoder = $encoder;
    }

    public function setEntityManager(EntityManagerInterface $em): self
    {
        $this->em = $em;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setName('inscriptions:change-password')
            ->setDescription('Change the password of a user.')
            ->setDefinition([
                new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                new InputArgument('password', InputArgument::REQUIRED, 'The password'),
            ])
            ->setHelp(<<<'EOT'
The <info>inscriptions:change-password</info> command changes the password of a user:
  <info>php %command.full_name% admin</info>
This interactive shell will first ask you for a password.
You can alternatively specify the password as a second argument:
  <info>php %command.full_name% admin mypassword</info>
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string */
        $username = $input->getArgument('username');
        /** @var string */
        $password = $input->getArgument('password');
        $this->changePassword($username, $password);
        $output->writeln(sprintf('Changed password for user <comment>%s</comment>', $username));

        return 0;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];
        if (!$input->getArgument('username')) {
            $question = new Question('Please give the username:');
            $question->setValidator(function (string $username) {
                if ($username === '' || $username === '0') {
                    throw new Exception('Username can not be empty');
                }

                return $username;
            });
            $questions['username'] = $question;
        }
        if (!$input->getArgument('password')) {
            $question = new Question('Please enter the new password:');
            $question->setValidator(function (string $password) {
                if ($password === '' || $password === '0') {
                    throw new Exception('Password can not be empty');
                }

                return $password;
            });
            $question->setHidden(true);
            $questions['password'] = $question;
        }
        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    public function changePassword(string $user, string $newPassword): void
    {
        $userEntity = $this->em->getRepository(User::class)->findOneBy(['username' => $user]);
        if ($userEntity != null) {
            $password = $this->encoder->encodePassword($userEntity, $newPassword);
            $userEntity->setPassword($password);
            $this->em->flush();
        }

        throw new InvalidArgumentException('User ' . $user . " doesn't exist!");
    }
}
