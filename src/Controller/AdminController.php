<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: salih
 * Date: 31.05.18
 * Time: 17:11.
 */

namespace App\Controller;

use App\Entity\FamilyMembers;
use App\Entity\Person;
use App\Exception\FamilyMembersException;
use App\Exception\InscriptionsDatabaseException;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\Exists;
use Elastica\Search;
use FOS\ElasticaBundle\Elastica\Client;
use FOS\ElasticaBundle\Elastica\Index;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends EasyAdminController
{
    private Client $client;
    /** @var array<Index> */
    private array $index = [];
    protected LoggerInterface $logger;

    public function __construct(Client $client, Index $nameIndex, Index $personIndex, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->index['name'] = $nameIndex;
        $this->index['persons'] = $personIndex;
        $this->logger = $logger;
    }

    /**
     * @Route("/database", name="easyadmin")
     */
    public function indexAction(Request $request): Response
    {
        return parent::indexAction($request);
    }

    public static function getNamesQueryBuilder(EntityRepository $er): QueryBuilder
    {
        return $er->createQueryBuilder('names')->orderBy('names.name', 'ASC');
    }

    public static function getPersonQueryBuilder(EntityRepository $er): QueryBuilder
    {
        return $er->createQueryBuilder('person')->orderBy('person.fullname', 'ASC');
    }

    public static function getPersonChoiceLabel(Person $person): string
    {
        $id = $person->getId();
        $fullname = $person->getFullname();
        if ($id == null) {
            $id = 'not saved yet';
        }
        if ($fullname == null) {
            $fullname = '';
        }

        return '(PID: ' . $id . ') ' . $fullname;
    }

    private function getFieldFromIndex(string $index, string $field, array $returnFields = null, int $size = 10_000): array
    {
        $search = new Search($this->client);
        $search->addIndex($index);
        $boolQuery = new BoolQuery();
        $boolQuery->addShould(new Exists($field));

        if ($returnFields == null) {
            $returnFields = [$field];
        }

        $query = new Query();
        $query->setSource($returnFields)
              ->setQuery($boolQuery)
              ->setSort([$field . '.keyword' => ['order' => 'asc']])
              ->setSize($size);
        $search->setQuery($query);
        $results = $search->search();
        $hitsArray = [];
        foreach ($results as $result) {
            $hitsArray[] = $result->getSource();
        }
        $this->logger->debug('Result array:', $hitsArray);

        return $hitsArray;
    }

    // These two functions are needed to catch malformed request URLs, makes it easier to debug Nginx problems ;)

    public function catchAll(Request $request): Response
    {
        return new RedirectResponse('/');
    }

    /**
     * @return RedirectResponse
     */
    protected function redirectToBackendHomepage(): RedirectResponse
    {
        $homepageConfig = $this->config['homepage'];
        $url = $this->get('router')->generate($homepageConfig['route'], $homepageConfig['params']);

        return $this->redirect(str_replace('/index.php', '', $url));
    }

    /**
     * @Route("/getNewNames", name="getNewNames", methods={"GET"}, options={"expose"=true})
     */
    public function refreshNames(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse($this->getFieldFromIndex($this->index['name']->getName(), 'name', ['name', 'id']));
        }

        return new JsonResponse([
            'type' => 'error',
            'message' => 'AJAX only',
        ]);
    }

    /**
     * @Route("/getNewPersons", name="getNewPersons", methods={"GET"}, options={"expose"=true})
     */
    public function refreshPersons(Request $request): Response
    {
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse($this->getFieldFromIndex($this->index['persons']->getName(), 'fullname', ['fullname', 'id']));
        }

        return new JsonResponse([
            'type' => 'error',
            'message' => 'AJAX only',
        ]);
    }

    public function createNewFamilyEntity(): FamilyMembers
    {
        if ($this->request->get('fromPerson') && $this->request->get('person') != null && $this->em != null) {
            $this->logger->debug('Got requst for prepopulated FamilyMembers form. fromPerson: ' . $this->request->get('fromPerson') . ' person:' . $this->request->get('person'));
            $repository = $this->em->getRepository(Person::class);
            if ($repository != null) {
                $fromPerson = $repository->findOneBy(['id' => $this->request->get('fromPerson')]);
                $person = $repository->findOneBy(['id' => $this->request->get('person')]);
                if ($fromPerson != null && $person != null) {
                    return FamilyMembers::newFamilyMembers($fromPerson, null, $person);
                }
            }
        }

        return new FamilyMembers();
    }

    /**
     * {@inheritdoc}
     *
     * @psalm-suppress PossiblyNullArgument
     * Ignore some Psalm errors here since method is borrowed from EasyAdmin
     */
    protected function editAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_EDIT);

        $id = $this->request->query->get('id');
        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        if ($this->request->isXmlHttpRequest() && $property = $this->request->query->get('property')) {
            $newValue = 'true' === mb_strtolower((string) $this->request->query->get('newValue'));
            $fieldsMetadata = $this->entity['list']['fields'];

            if (!isset($fieldsMetadata[$property]) || 'toggle' !== $fieldsMetadata[$property]['dataType']) {
                throw new RuntimeException(sprintf('The type of the "%s" property is not "toggle".', (string) $property));
            }

            $this->updateEntityProperty($entity, $property, $newValue);

            // cast to integer instead of string to avoid sending empty responses for 'false'
            return new Response((string) (int) $newValue);
        }

        $fields = $this->entity['edit']['fields'];

        $editForm = $this->executeDynamicMethod('create<EntityName>EditForm', [$entity, $fields]);
        $deleteForm = $this->createDeleteForm($this->entity['name'], $id);

        $editForm->handleRequest($this->request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->processUploadedFiles($editForm);

            $this->dispatch(EasyAdminEvents::PRE_UPDATE, ['entity' => $entity]);

            try {
                $this->executeDynamicMethod('update<EntityName>Entity', [$entity, $editForm]);
            } catch (Exception | FamilyMembersException $e) {
                $errorMessage = InscriptionsDatabaseException::getErrorMessage($e);
                $this->addFlash('error', $errorMessage);
                goto formEdit;
            } catch (ORMException $e) {
                throw new InscriptionsDatabaseException(['exception' => $e]);
            }
            $this->dispatch(EasyAdminEvents::POST_UPDATE, ['entity' => $entity]);

            return $this->redirectToReferrer();
        }
        formEdit:
        $this->dispatch(EasyAdminEvents::POST_EDIT);

        $parameters = [
            'form' => $editForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['edit', $this->entity['templates']['edit'], $parameters]);
    }

    /**
     * {@inheritdoc}
     */
    protected function newAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_NEW);

        $entity = $this->executeDynamicMethod('createNew<EntityName>Entity');

        $easyadmin = $this->request->attributes->get('easyadmin');
        $easyadmin['item'] = $entity;
        $this->request->attributes->set('easyadmin', $easyadmin);

        $fields = $this->entity['new']['fields'];

        $newForm = $this->executeDynamicMethod('create<EntityName>NewForm', [$entity, $fields]);

        $newForm->handleRequest($this->request);
        if ($newForm->isSubmitted() && $newForm->isValid()) {
            $this->processUploadedFiles($newForm);

            $this->dispatch(EasyAdminEvents::PRE_PERSIST, ['entity' => $entity]);

            try {
                $this->executeDynamicMethod('persist<EntityName>Entity', [$entity, $newForm]);
            } catch (Exception | FamilyMembersException $e) {
                $errorMessage = InscriptionsDatabaseException::getErrorMessage($e);
                $this->addFlash('error', $errorMessage);
                goto formNew;
            } catch (ORMException $e) {
                throw new InscriptionsDatabaseException(['exception' => $e]);
            }
            $this->dispatch(EasyAdminEvents::POST_PERSIST, ['entity' => $entity]);

            return $this->redirectToReferrer();
        }
        formNew:
        $this->dispatch(EasyAdminEvents::POST_NEW, [
            'entity_fields' => $fields,
            'form' => $newForm,
            'entity' => $entity,
        ]);

        $parameters = [
            'form' => $newForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['new', $this->entity['templates']['new'], $parameters]);
    }
}
