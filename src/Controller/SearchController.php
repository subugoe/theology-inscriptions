<?php

declare(strict_types=1);

namespace App\Controller;

use App\Util\Search\Elastic\ElasticSearchBuilder;
use App\Util\Search\Elastic\MissingMethodFixedSearch;
use App\Util\Search\IndexMatchSearchFilter;
use App\Util\Search\SearchBuilderInterface;
use App\Util\Search\SearchQueryParseException;
use Elastica\Exception\PartialShardFailureException;
use Elastica\Exception\ResponseException;
use Elastica\Query;
use Elastica\Query\AbstractQuery;
use Elastica\Result;
use Elastica\ResultSet;
use Elastica\ResultSet\DefaultBuilder;
use Elastica\Search;
use FOS\ElasticaBundle\Configuration\ConfigManager;
use FOS\ElasticaBundle\Elastica\Client;
use FOS\ElasticaBundle\Index\IndexManager;
use LogicException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    protected Client $client;
    protected IndexManager $mngr;
    protected ConfigManager $cfmgr;
    protected LoggerInterface $logger;
    protected bool $highlight;
    protected bool $flatFieldList = false;
    protected array $indicesMetadata;
    protected SearchBuilderInterface $queryBuilder;

    public function __construct(Client $client, IndexManager $mngr, ConfigManager $cfmgr, LoggerInterface $logger, bool $highlight = true)
    {
        $this->client = $client;
        $this->mngr = $mngr;
        $this->cfmgr = $cfmgr;
        $this->logger = $logger;
        $this->highlight = $highlight;
        $this->indicesMetadata = $this->getIndicesMetadata();
        $this->queryBuilder = new ElasticSearchBuilder($this->indicesMetadata, $this->logger);
        // the old query builder, dont'forget to import it, if you want to use it
        //$this->queryBuilder = new ClassicElasticSearchBuilder($this->indicesMetadata, $this->logger);
        $this->queryBuilder->setHighlight($this->highlight);
    }

    /**
     * @Route("/search", name="search")
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $index = $request->get('index');
        if ($request->get('q') !== null && $request->get('q') != '') {
            $query = trim($request->get('q'));
            $this->queryBuilder->setQueryString($query);
            $queryIndices = [];
            if (!empty($index) && array_key_exists($index, $this->indicesMetadata)) {
                $this->logger->debug('Searching only in index "' . $index . '"');
                $queryIndices = [$index];
            } else {
                $queryIndices = array_keys($this->indicesMetadata);
                /* Reenable this if you want the classic query builder
                foreach (array_keys($this->indicesMetadata) as $i) {
                    $queryIndices[$i] = [];
                }
                */
            }
            $results = $this->performSearch($query, $queryIndices);

            return $this->render('search/index.html.twig', $this->defaultTemplateValues(['indexcfg' => $this->cfmgr, 'results' => $results]));
        }

        return $this->render('search/index.html.twig', $this->defaultTemplateValues());
    }

    /**
     * @Route("/search/map/{index}", name="map", methods={"GET"}, options={"expose"=true})
     *
     * @return Response
     */
    public function map(Request $request): Response
    {
        $index = $request->attributes->get('index');
        if (!array_key_exists($index, $this->indicesMetadata) || !$this->indicesMetadata[$index]['map']['enabled']) {
            $this->addFlash('error', "Index doesn't exist or isn't marked as mapable!");
        }
        if ($request->get('q') !== null && $request->get('q') != '') {
            $query = trim($request->get('q'));
            $this->queryBuilder->setQueryString($query);
            $results = $this->performSearch($query, [$index]);

            return $this->render('map/index.html.twig', $this->defaultTemplateValues([
                'index' => $index,
                'results' => $results,
            ]));
        }

        return $this->render('map/index.html.twig', $this->defaultTemplateValues(['index' => $index]));
    }

    /**
     * @Route("/map", name="old_map", methods={"GET"})
     *
     * @return Response
     */
    public function oldMap(Request $request): Response
    {
        return $this->redirectToRoute('map', ['index' => 'inscriptions']);
    }

    /**
     * @Route("/search/geojson/{index}", name="geojson", methods={"GET"}, options={"expose"=true})
     *
     * @return Response
     */
    public function geojson(Request $request): Response
    {
        // TODO: Check for exceptions
        $index = $request->attributes->get('index');
        $query = trim($request->get('q'));
        if (!array_key_exists($index, $this->indicesMetadata) || !$this->indicesMetadata[$index]['map']['enabled']) {
            return new JsonResponse(['error' => 'Index doesn\'t exist or isn\'t marked as mapable!']);
        }

        if ($request->get('q') !== null) {
            $results = $this->performSearch($query, [$index]);

            return JsonResponse::fromJsonString($this->renderView('common/geojson.json.twig', $this->defaultTemplateValues([
                'index' => $index,
                'results' => $results,
            ])));
        }

        return new JsonResponse([]);
    }

    protected function defaultTemplateValues(array $arr = null): array
    {
        $default = [
            'indexes_metadata' => $this->indicesMetadata,
        ];
        if ($arr == null) {
            return $default;
        }

        return array_merge($default, $arr);
    }

    protected function performSearch(string $queryStr, array $indices, bool $silent = false): array
    {
        $results = null;
        $search = null;
        $this->logger->debug('Adding filter for indices', $indices);
        $this->queryBuilder->setFilter(new IndexMatchSearchFilter($indices));

        try {
            $search = $this->buildSearch($queryStr, $indices);
            $results = $search->search();
        } catch (SearchQueryParseException $eqpe) {
            $error = "There is an error in your query:\n";
            $error .= $eqpe->getErrorString();
            if (!$silent) {
                $this->addFlash('error', nl2br($error, true));
            } else {
                // Pass the exception - needed to create better JSON error messages
                throw $eqpe;
            }
        } catch (PartialShardFailureException $psfe) {
            // TODO: Check if this hack is still needed after complete switch to Elastica API
            $response = $psfe->getResponse();
            if ($search !== null) {
                $results = (new DefaultBuilder())->buildResultSet($response, $search->getQuery());
            }
            $this->logger->debug('Ignored the following errors: ', $response->getData()['_shards']['failures']);
        } catch (ResponseException $e) {
            var_dump($e);
            $error = "Your search failed - there is probaly an error in your query:\n";
            $error .= implode("\n", self::guessSearchError($queryStr));
            if (!$silent) {
                $this->addFlash('error', nl2br($error, true));
                $this->addFlash('trace', htmlspecialchars($e->getMessage()));
            } else {
                // Pass the exception - needed to create better JSON error messages
                throw $e;
            }
        }
        if ($results !== null) {
            $this->logger->debug('Raw results: ', $results->getResponse()->getData());

            return $this->createResultArray($results);
        }

        return [];
    }

    protected function buildSearch(string $queryString, array $indices): Search
    {
        $cast = fn (object $o): MissingMethodFixedSearch => $o;
        $search = $cast($this->queryBuilder->getQuery());
        $search->setClient($this->client);
        $queryIndices = $search->getIndices();
        if (empty($queryIndices)) {
            $search->setIndices($indices);
        } else {
            foreach (array_diff($queryIndices, $indices) as $removeIndex) {
                $search->removeIndex($removeIndex);
            }
        }
        $this->logger->debug('Build Search object for the following Query on indices "' . implode(', ', $indices) . '": ', ['query' => $search->getQuery(), 'indexes' => $search->getIndices()]);
        $this->logger->notice("[CnP] Query is:\n" . json_encode($search->getQuery()->toArray(), JSON_THROW_ON_ERROR) . "\n");

        return $search;
    }

    // TODO: If this works, move it to Twig layer to avoid looping two times thru the array
    protected function createResultArray(ResultSet $results): array
    {
        $resultArr = [];
        foreach ($results as $result) {
            $index = $result->getIndex();
            if (!array_key_exists($index, $resultArr)) {
                $resultArr[$index] = [];
            }
            $resultArr[$index][] = $this->buildResultRowArray($result);
        }

        ksort($resultArr);

        return $resultArr;
    }

    protected function buildResultRowArray(Result $result): array
    {
        $source = $result->getSource();
        $highlight = $result->getHighlights();
        $innerHits = $result->getInnerHits();

        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
                            ->enableExceptionOnInvalidIndex()
                            ->getPropertyAccessor();

        $seperator = '.';
        if (!empty($highlight)) {
            $row = [];
            foreach ($source as $fieldKey => $fieldValue) {
                if (array_key_exists($fieldKey, $highlight)) {
                    // TODO: Check if the highlight array can also be longer
                    $highlightStr = $highlight[$fieldKey][0];
                    if ((strpos($fieldKey, $seperator) !== false)) {
                        // Dang! we got a innerHit somewhere down here :(
                        $row[$fieldKey] = $fieldValue;
                    // But since Elastic search doesn't guarantee an innerHit field for each nested match, we need do dig into the innerHits Array, see below
                    } else {
                        $row[$fieldKey] = $highlightStr;
                    }
                } elseif (array_key_exists($fieldKey, $innerHits) && $innerHits[$fieldKey]['hits']['total'] > 0) {
                    /*
                    // A good API works like a bank robbery: Give me all your money, or else...
                    // The Elasticsearch API works like writing a grant proposal: Write a long text and maybe you get what you want (or at least - expect), but almost certainly you don't...
                    //                                                            And even if you get something - we make it extra complicated to make you stop asking.
                    // This is a good example how to 'promote' features, that you aren't good in - scare the user away.
                    */
                    foreach ($innerHits[$fieldKey]['hits']['hits'] as $innerHit) {
                        $offset = $innerHit['_nested']['offset'];
                        foreach ($innerHit['highlight'] as $absoluteHighlightPath => $highlightArr) {
                            $relativeKeyBase = substr($absoluteHighlightPath, strlen($fieldKey . $seperator));
                            $key = '[' . $offset . '][' . $relativeKeyBase . ']';
                            $highlightStr = implode(', ', $highlightArr);
                            $propertyAccessor->setValue($fieldValue, $key, $highlightStr);
                        }
                    }
                    $row[$fieldKey] = $fieldValue;
                } else {
                    $row[$fieldKey] = $fieldValue;
                }
            }

            return $row;
        }

        return $source;
    }

    private function getFieldList(string $key, array $properties, array $field_config = []): array
    {
        $fields = [];
        foreach (array_keys($properties) as $property) {
            $searchable = true;
            if (array_key_exists($property, $field_config) && array_key_exists('_searchable', $field_config[$property])) {
                $searchable = $field_config[$property]['_searchable'];
            }
            if (!(isset($properties[$property]) && array_key_exists('type', $properties[$property]) && in_array($properties[$property]['type'], ['nested', 'object', 'alias']))) {
                if ($key == '') {
                    $prefix = '';
                } else {
                    $prefix = $key . '.';
                }
                if ($searchable) {
                    $fields[] = $prefix . $property;
                }
            } elseif (array_key_exists('type', $properties[$property]) && $searchable) {
                // Ignore all aliases
                if ($properties[$property]['type'] == 'alias') {
                    $searchable = false;
                } elseif (in_array($properties[$property]['type'], ['object', 'nested'])) {
                    if ($this->flatFieldList) {
                        foreach ($this->getFieldList($property, $properties[$property]['properties'], $field_config) as $nested) {
                            $fields[] = $nested;
                        }
                    } else {
                        $fields[] = $this->getFieldList($property, $properties[$property]['properties'], $field_config);
                    }
                } else {
                    throw new LogicException('This should never happen: Index configuration contains unknown type ("' . $properties[$property]['type'] . '") for "' . $property . '", but is marked as searchable (probably by default, you need to hide it explicitly)');
                }
            } elseif ($searchable) {
                throw new LogicException('This should never happen: Index configuration contains unknown attribute for "' . $property . '", but is marked as searchable (probably by default, you need to hide it explicitly) Contents: ' . print_r($properties[$property], true));
            }
        }

        return $fields;
    }

    protected function getIndicesMetadata(): array
    {
        $indices = [];
        foreach (array_keys($this->mngr->getAllIndexes()) as $index) {
            $indices[$index]['map']['enabled'] = false;
            $indices[$index]['nested'] = [];
            $indices[$index]['columns'] = [];
            foreach ($this->cfmgr->getIndexConfiguration($index)->getTypes() as $type) {
                $properties = $type->getMapping()['properties'];
                $field_config = [];
                if (array_key_exists('_meta', $type->getMapping()) && array_key_exists('field_meta', $type->getMapping()['_meta'])) {
                    $field_config = $type->getMapping()['_meta']['field_meta'];
                }
                $meta = $type->getMapping()['_meta'];
                foreach (array_keys($properties) as $property) {
                    $indices[$index]['columns'][$property] = [];
                    $columnHeadingClass = 'th-' . $property;
                    if (array_key_exists($property, $field_config) && array_key_exists('_enum', $field_config[$property]) && $field_config[$property]['_enum']) {
                        $columnHeadingClass .= ' enum';
                        $indices[$index]['columns'][$property]['enum'] = true;
                    }
                    $indices[$index]['columns'][$property]['class'] = $columnHeadingClass;
                    if (array_key_exists($property, $field_config) && array_key_exists('_mapable', $field_config[$property]) && $field_config[$property]['_mapable'] == true) {
                        $indices[$index]['map'] = ['enabled' => true, 'coords_field' => $property];
                    }
                    if (array_key_exists($property, $field_config) && array_key_exists('_displayable', $field_config[$property]) && $field_config[$property]['_displayable'] == false) {
                        if (isset($indices[$index]['hide_fields'])) {
                            $indices[$index]['hide_fields'][] = $property;
                        } else {
                            $indices[$index]['hide_fields'] = [$property];
                        }
                    }
                    $searchable = true;
                    if (array_key_exists($property, $field_config) && array_key_exists('_searchable', $field_config[$property])) {
                        $searchable = $field_config[$property]['_searchable'];
                    }
                    // Ignore all aliases
                    if (array_key_exists('type', $properties[$property]) && $properties[$property]['type'] == 'alias') {
                        $searchable = false;
                    }
                    if (array_key_exists('type', $properties[$property]) && $properties[$property]['type'] == 'nested' && $searchable) {
                        $indices[$index]['nested'][] = $property;
                    }
                }
                $indices[$index]['fields'] = $this->getFieldList('', $properties, $field_config);
                // Set type
                $model = $type->getModel();
                if (!is_null($model)) {
                    $full = explode('\\', $model);
                    $indices[$index]['entity'] = array_pop($full);
                } else {
                    throw new LogicException('This should never happen: No Entity for index "' . $index . '"');
                }
            }
            if (isset($indices[$index]['nested']) && $indices[$index]['nested'] !== []) {
                $indices[$index]['inner_hits'] = true;
            } else {
                $indices[$index]['inner_hits'] = false;
            }
        }

        return $indices;
    }

    protected static function guessSearchError(string $query): array
    {
        $errorList = [];

        foreach (["'", '"'] as $char) {
            if (strpos($query, $char) !== false && substr_count($query, $char) % 2 === 0) {
                $errorList[] = "Uneven count of '$char' detected!";
            }
        }
        if (substr_count($query, ':') > substr_count($query, ' ') + 1) {
            $errorList[] = 'You seem to search for specific fields, but no seperator (space) given!';
        }

        return $errorList;
    }

    //This is a dirty hack for type casting - use it to log certain objects
    public static function toElasticaQuery(object $query): Query
    {
        if ($query instanceof Search) {
            return $query->getQuery();
        }

        if ($query instanceof AbstractQuery) {
            return new Query($query);
        }

        throw new LogicException('Query could\'nt be casted to "Elastica\Query"');
    }
}
