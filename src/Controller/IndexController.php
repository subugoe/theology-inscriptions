<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function indexAction(Request $request): Response
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/help", name="help", methods={"GET"})
     */
    public function helpAction(Request $request): Response
    {
        return $this->render('help/index.html.twig');
    }

    /**
     * @Route("/about", name="about", methods={"GET"})
     */
    public function aboutAction(Request $request): Response
    {
        return $this->render('about/index.html.twig');
    }

    /**
     * @Route("/privacy", name="privacy", methods={"GET"})
     */
    public function privacyAction(Request $request): Response
    {
        return $this->render('privacy/index.html.twig');
    }

    /**
     * @Route("/imprint", name="privacy", methods={"GET"})
     */
    public function imprintAction(Request $request): Response
    {
        return $this->render('imprint/index.html.twig');
    }
}
