$(document).ready(function() {
    const mobileMenuButton = $('#main-menu-mobile-toggle');
    const mobileMenuWrapper = $('.mobile-menu-wrapper');
    const closeButton = $('.mobile-menu-wrapper .close');
    mobileMenuButton.on('click', () => {
        mobileMenuWrapper.addClass('show');
    });

    closeButton.on('click', () => {
        mobileMenuWrapper.removeClass('show');
    })
});