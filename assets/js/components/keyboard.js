// Make sure jQuery is a true global (with aliases)
const $ = global.$ = global.jQuery = require('jquery');

// Import frameworks for keyboard
require('jquery-ui/ui/core');
require('jquery-ui/ui/widgets/autocomplete');
require('./jquery-ext');
require('virtual-keyboard');

// Define custom keyboard structure and elements
const custom_keyboard = {
    usePreview: false,
    autoAccept: true,
    layout: 'custom',
    customLayout: {
        normal: [
            'Α Β Γ Δ Ε Ζ Η Θ Ι Κ Λ Μ',
            'Ν Ξ Ο Π Ρ Σ Τ Υ Φ Χ Ψ Ω',
            'Ἀ Ἁ Ἄ Ἅ Ἆ Ἇ Ἐ Ἑ Ἔ Ἕ Ἠ Ἡ',
            'Ἤ Ἥ Ἦ Ἧ Ἰ Ἱ Ἴ Ἵ Ἶ Ἷ Ὀ Ὁ',
            'Ὄ Ὅ Ὑ Ὕ Ὗ Ὠ Ὡ Ὤ Ὥ Ὦ Ὧ Ῥ',
            'α β γ δ ε ζ η θ ι κ λ μ ν',
            'ξ ο π ρ σ ς τ υ φ χ ψ ω ά',
            'ᾶ ἀ ἁ ὰ έ ἐ ἑ ὲ ή ῆ ἠ ἡ ὴ',
            'ί ῖ ἰ ἱ ὶ ἴ ἵ ἶ ἷ ό ὁ ὐ ὑ',
            'ὔ ὕ ὖ ὗ ύ ῦ ὺ ώ ῶ ὠ ὡ ὼ',
            '{clear:Suchfeld_leeren} {meta1} {accept}',
        ],
        meta1: [
            'Ḍ Ḏ Ğ Ġ Ḥ Ḫ',
            'Ṣ Ś Š Ṭ Ṯ Ẓ',
            'ğ ā ē ī ō ū â ê î',
            'ô û ə ʔ ʕ ḍ ḏ ġ ḥ',
            'ḫ ṣ ś š ṭ ṯ ẓ',
            'ὸ ὀ',
            '{clear:Suchfeld_leeren} {normal} {accept}',
        ],
    },
    display: {
        meta1: 'Others',
        clear: 'Clear',
        normal: 'Greek',
        accept: 'Close',
    },
};

// Add name of the field to this list to add a keyboard
// This list applies to the filters in the list view
const filterFieldsKeyboard = ['village', 'subregion', 'name', 'philologicalAnalysis', 'remarks', 'fullname'];

// See https://jsfiddle.net/Mottie/egb3a1sk/1520/
// Used by the keyboard inside select2
const keys = {
    bksp: 8,
    tab: 9,
    enter: 13,
    space: 32,
    delete: 46
};

//This is a function to be reusable in select2 Ajax call
export function getCustomSect2Keyboard (elem) {
    // See https://jsfiddle.net/Mottie/egb3a1sk/1520/
    var select2Keyboard = {
        reposition: true,
        usePreview: false,
        // Used by jQuery UI position utility
        position: {
            // null = attach to input/textarea;
            // use $(sel) to attach elsewhere
            of: $(elem).parent(),
            my: 'right top',
            at: 'right bottom',
            // used when "usePreview" is false
            at2: 'right bottom'
        },
        change: function(e, keyboard) {
            var key = (keyboard.last.key || '').toLowerCase();
            // trigger a keydown for "special" keys
            if (keys[key]) {
                e.type = 'keydown';
            } else {
                e.type = 'input';
            }
            //e.type = keys[key] ? 'keydown' : 'input';
            e.which = keys[key] || key.charCodeAt(0);
            keyboard.$el.trigger(e);
        }
    };
    return $.extend(custom_keyboard, select2Keyboard);
}

// This closes the keyboard of a given element
export function closeActiveKeyboard (elem) {
    var sourceId = elem.data().keyboardSource;
    //if no link is detected just return
    if (typeof sourceId === 'undefined') {
        return
    }
    var keyboardId = sourceId + '_keyboard';
    var keyboardDiv = $('#' + keyboardId).get(0);
    // Check if div really exists
    if ($(keyboardDiv).hasClass('ui-keyboard')) {
        $(keyboardDiv).data().keyboard.close();
    }
}

// This function adds a keyboad to the given element
export function addKeyboard(elem) {
    // Return if argument is empty
    if (!elem.length) {
        return;
    }

    // Set an id if there isn't provided one, we need it later for CSS (textareas) and select2
    if (typeof elem.get(0).attributes.id === 'undefined') {
        elem.uniqueId();
    }
    var elemId = elem.get(0).attributes.id.value;

    if ($(elem).hasClass('ui-keyboard-input')) {
        return;
    }
    if ($(elem).hasClass('.keyboard-input-textarea')) {
        // Keyboard position hack
        // Add a scroll function for 'textarea' inputs
        $(elem).click(function(){
            $('html, body').animate({'scrollTop': elem.offset().top - 100}, 'slow');
            $('#' + elem.id + '_keyboard').css({
                'top': 'auto',
                'bottom': 546
            });
            return false;
        });
        $(elem).keyboard(custom_keyboard);
    } else if ($(elem).hasClass('select2-hidden-accessible')) {
        //console.log('Warning! Element has \'select2\' widget enabled');
        var keyboardId = elemId + '_select2_search_field'
        // See https://jsfiddle.net/Mottie/egb3a1sk/1520/

        $(elem).on('select2:open', function () {
            // Add keyboard
            var keyboardInput = $('.select2-container--open .select2-search__field').
                css({'width': '50%'}).
                // Disable backspace for propagation since it will remove the last selection
                off('keydown').on('keydown', function(e) {
                    if (e.keyCode === 8) {
                        e.stopPropagation();
                    }
                });
            // Readding the keyboard attributes results in an infinitive loop
            if (!keyboardInput.hasAttr('id') && !keyboardInput.hasAttr('data-keyboard-source')) {
                // Add an id to the input field, later the keyboard will generate it's own id based on this
                keyboardInput.attr('id', keyboardId).attr('data-keyboard-target', elemId).
                    keyboard(getCustomSect2Keyboard(elem));
            }
            // This is needed to keep the focus inside the search box when the select2 has opened
            keyboardInput.on('focusout', function (e){
                e.stopPropagation();
            });
        });

        // Close keyboard on close of 'select2'
        $(elem).on('select2:close', function () {
            closeActiveKeyboard($(this));
        });
        // Save the id of the keyboard input field id to the element where the keyboard has been requested
        $(elem).attr('data-keyboard-source', keyboardId);
    } else {
        $(elem).keyboard(custom_keyboard);
    }
}

$(document).ready(function() {
// Selecting elements for adding virtual keyboard is now done in 'config/packages/easy_admin.yml'.
// Just add a class 'keyboard-input' to the HTML element of the form, not the form elements generated by Symfony.
// Use the class 'keyboard-input-textarea' if you want to have a smooth scrolling enabled.

    if ($('body.list').length) {
        // Add keyboard to modals (filter view)
        // See the list of fields above ('filterFieldsKeyboard')
        $('div#modal-filters').on('DOMNodeInserted', function() {
            $(this).find('[data-toggle="collapse"]').collapse();
            for (const field of filterFieldsKeyboard) {
                var selector = 'div#modal-filters div#filters_' + field + ' input[type=text], div#modal-filters div#filters_' + field + ' textarea';
                var elem = $(selector);
                if (elem.length) {
                    addKeyboard(elem);
                    // This is need to activate a filter generated by virtual keybord
                    $(document).on('change', selector, function (event) {
                        if (event.target.classList.contains('filter-checkbox')) {
                            return;
                        }
                        let filterCheckbox = event.target.closest('.filter-field').querySelector('.filter-checkbox');
                        if (!filterCheckbox.checked) {
                            filterCheckbox.checked = true;
                        }
                    });
                }
            }
        });
    }
    // Add keyboard by class (.keyboard-input, .keyboard-input-textarea)
    // Add keyboard to global search (div .global-actions .action-search input[type=search])
    // Also add keyboard to search box of SearchController and MapController
    $('.keyboard-input, .keyboard-input-textarea, div .global-actions .action-search input[type=search]').each(function() {
        addKeyboard($(this));
    });


});
