document.addEventListener('keydown', function(e) {
    var creditsHTML = '<h1>Script (Initial software design)</h1><h2>Mustafa Pamuk</h2><br/>\
                       <h1>Executive Producer (Project Coordination)</h1><h2>Tine Schima-Voigt</h2><br/>\
                       <h1>Hair/Makeup Designer (Usabilty)</h1><h2>Marcus Hellmann</h2><br/>\
                       <h1>Costume Designers (CSS / JavaScript)</h1><h2>Marcus Hellmann</h2><h2>Mustafa Pamuk</h2><h2>Christian Mahnke</h2><br/>\
                       <h1>Special Effects (Backend - PHP)</h1><h2>Marcus Hellmann</h2><h2>Mustafa Pamuk</h2><h2>Christian Mahnke</h2><br/>\
                       <h1>Carpentry (Docker)</h1><h2>Christian Mahnke</h2><br/>\
                       <h1>Stunt Coordination (Review / Testing)</h1><h2>Tine Schima-Voigt</h2><h2>Andreas Lüschow</h2><br/>\
                       <h1>Stunts (Testing Dummies)</h1><h2>Seleukos</h2><h2>Barates</h2><br/>';

    if (e.key === 'Escape') {
        var inputs = $('input, textarea').map(function () {
            return $(this).val().toLowerCase();
        }).get();
        if (inputs.includes('iddqd')) {
            // Create a function to close
            var close = function (bg) {
                $(bg).fadeOut(1000, function() {
                    $(this).remove();
                });
            }

            // Get the window hight we have to walk up
            // fh = frame hight
            var fh = $(document).height();
            // Create background div
            var background = document.createElement('div');
            $(background).attr('id', 'credit-wrapper').
                // set content
                html('<div id="team">' + creditsHTML + '</div>').
                // Register events for removing the background
                on('click', function () {
                    close(background);
                }).
                on('keydown', function(e) {
                    if (e.key === 'Escape') {
                        close(background);
                    }
                });
            // Add background to body
            $('body').prepend(background);
            // We need to hide stuff to able to measure the height
            $('#team').children('h1, h2, h3').each(function () {
                $(this).css({'opacity': 0});
            });
            // and show it
            $(background).fadeIn(1000, function () {
                // position the div below the page bottom (including buffer) and make text visible again
                var h = parseInt($('#team').height());
                $('#team').css({
                    'position': 'absolute',
                    'bottom': '-' + h + 'px'
                }).
                    children('h1, h2, h3').each(function () {
                        $(this).css({'opacity': 1});
                    });
                // Start animation
                $('#team').animate({'bottom': fh + 'px'}, {
                    'duration': 30000,
                    'complete': function () {
                        close(background);
                    }
                });
            });

        }
    }
});
