// Add methods to jQuery
// See https://j11y.io/snippets/jquery-with-method/
$.fn.with = function(elems,action){
    return this.each(function(){
        $(elems,this).each(action);
    });
}
// https://stackoverflow.com/a/31047
$.fn.exists = function(){
    return this.length > 0;
}

// A simple jQuery function to check if an attribute exists
$.fn.hasAttr = function(a){
    var attr = $(this).attr(a);
    if (typeof attr !== 'undefined' && attr !== false) {
        return true;
    }
    return false;
}

// This gets a list of classes
$.fn.classes = function(){
    if ($(this).hasAttr('class')) {
        return $(this).attr('class').split(/\s+/u);
    }
    return [];
}
