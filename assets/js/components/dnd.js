// Import jQuery
import $ from 'jquery';

// Function to add drag and drop (DnD) to the person listing
// See https://www.html5rocks.com/en/tutorials/dnd/basics/
export function addDnDPerson(elem) {
    // Start of drag
    $(elem).on('dragstart', function (e) {
        this.style.opacity = '0.5';
        e.originalEvent.dataTransfer.effectAllowed = 'copy';
        e.originalEvent.dataTransfer.setData('fromPersonId', $(elem).text());
    });
    $(elem).on('dragenter', function () {
        // Check if we are the element we are dragging over - currently done at drop
        /*
        var from = e.originalEvent.dataTransfer.getData('fromPersonId');
        var to = $(elem).text();
        console.log(from + '->' + to);
        if (from != to) {
            this.classList.add('over');
        }
        */
        this.classList.add('over');
    });
    $(elem).on('dragover', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    });
    $(elem).on('dragleave', function () {
        this.classList.remove('over');
    });
    // Handler after drop on target
    $(elem).on('drop', function (e) {
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        this.classList.remove('over');
        var from = e.originalEvent.dataTransfer.getData('fromPersonId');
        var to = $(elem).text();
        if (from == to) {
            console.log('from and to id are identical');
            return false;
        }
        window.location.href = '/?entity=Family&action=new&fromPerson=' + from + '&person=' + to;
        return false;
    });
    // Handler for the source element after drag is finished
    $(elem).on('dragend', function () {
        this.style.opacity = '1';
    });
}