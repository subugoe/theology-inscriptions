const $ = global.$ = global.jQuery = require('jquery');
require('bootstrap');
// import CSS
import 'datatables.net-dt/css/jquery.dataTables.css';
import '../../vendor/easycorp/easyadmin-bundle/src/Resources/public/app.css';
import '../css/search.scss';
// Import keyboard
require('./components/keyboard');

// Import table library
require('datatables.net');

// Variables
// This structure is used to evaluate the query in the client
// It has a 'name' as key which is used to reference hits later, the following keys are allowed in each sub hash:
// - replacement: A regular expression or function to apply a quickfix - could be made optional
// - match: A regular expression or function to check if the contents of the search box match
// - id: The Id of the message box (could be optimized away)
// - msg: The message to be displayed
var warnings = {
    'and': {
        'replacement': 'AND',
        'match':'/and/g',
        'id': 'search-box-warning-and',
        'msg': 'Your query contains the phrase "and" (in lower case) - if you\'re meaning the logical search operator, you need to use "AND" (in upper case).'
    },
    'or':  {
        'replacement': 'OR',
        'match':'/or/g',
        'id': 'search-box-warning-or',
        'msg': 'Your query contains the phrase "or" (in lower case) - if you\'re meaning the logical search operator, you need to use "OR" (in upper case).'
    }

    /*,
'andor':{'replacement': function (str) {
    var andPos = str.indexOf('AND')
    var orPos = str.indexOf('OR')
    if (andPos < orPos) {
        return str.replace('OR', '');
    } 
    return str.replace('AND', '');
},
'match': function (str) {
    if (str.includes('AND') && str.includes('OR')) {
        return true; 
    } 
    return false; 
},
'id': 'search-box-warning-andor',
'msg': 'Your query contains the phrases "AND" or "OR" - only either of both is allowed at the same time.'}
*/
};

function initSearchBox (boxId) {
    var box = $('#' + boxId);

    // Check the search box on every change
    box.on('keyup change', function () {
        for (var str in warnings) {
            var change = false;
            var id = warnings[str].id;
            // Regex matcher found
            if ('match' in warnings[str] && typeof warnings[str].match !== 'function' && $(this).val().match(warnings[str].match)) {
                change = true;
            // Function matcher found
            } else if ('match' in warnings[str] && typeof warnings[str].match === 'function' && warnings[str].match($(this).val())) {
                change = true;
            // No special matcher defined, check string
            } else if ($(this).val().includes(str)) {
                change = true;
            }
            //if (('match' in warnings[str] && $(this).val().match(warnings[str].match)) || $(this).val().includes(str)) {
            if (change === true) {
                var replacement = warnings[str].replacement;
                var text = warnings[str].msg;
                var msg = '<div id="' + id + '" class="alert-warning alert alert-dismissible fade show search-warning-and" role="alert">' + text +
                      '<a class="quick-fix" data-str="' + str + '" data-replacement="' + replacement + '" data-ref="#' + boxId + '">Fix</a>' + 
                      '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                      '<span aria-hidden="true">&times;</span></button></div>';
                if (!$('#' + id).length) {
                    box.parent('.input-group').append(msg);
                }
            } else if ($('#' + id).length) {
                $('#' + id).remove();
            }
        }
    });
    $(document).on('click', '.searchbox .input-group .quick-fix', function() {
        var idRef = $(this).attr('data-ref');
        var replacement = $(this).attr('data-replacement');
        var str = ''
        // TODO: Get rid of eval and just use the key provided by the data-str attribute
        if (replacement.includes('function')) {
            replacement = eval('(' + replacement + ')');
            str = /.*/gu;
        } else {
            str = $(this).attr('data-str');
        }
        var newQuery = $(idRef).val().replace(str, replacement);
        $(idRef).val(newQuery);
        $(this).parent().remove();
    });
}

function initStatistics (divId, tableId) {
    //The jQuery selector to find hits
    var hitSelector = 'em.hl';
    var div = $('#' + divId);
    var table = $('#' + tableId).DataTable();
    div.append('<li class="statistics-row"><span class="statistics-title">Rows:</span> ' + table.rows().count() + '</li>');
    // DataTable has an insanely complicated API - 
    table.columns().every(function () {
        var hits = this.nodes().to$().find(hitSelector).length;
        // The title of the column
        var columnTitle = this.header().innerHTML;
        // The number of the column, can be used to address  it via DataTables API, jQuery only works if column is visible
        var index = this.index();
        // Variables needed for counting distinct values
        // 'distinctValues' could be optimized away by just counting the keys in 'distinctCounter'
        var countColumnNodes = [], 
            distinctCounter = [], 
            distinctValues = [];
        if (this.nodes().to$().find('.enum-value').length) {
            countColumnNodes = this.nodes().to$().find('.enum-value').toArray();
        } else {
            countColumnNodes = this.nodes().to$().toArray();
        }
        countColumnNodes.map(function(el){ 
            // Stupid JavaScript doesn't even supply an equalsIgnoreCase method out of the box!
            var value = el.innerText.trim();
            if (value != 'Empty' && value != '') {
                if (!distinctValues.includes(value)) {
                    distinctValues.push(value);
                    distinctCounter[value] = 1;
                } else {
                    distinctCounter[value] += 1;
                }
            }
            return Object.keys(distinctCounter);
        });
        // This is used to hide a column
        var toggle = '<input class="toggle" type="checkbox" data-column="' + index + '" checked>';
        var hitCountStr = '<span class="hits">' + hits + ' Hit';
        // Create plural form if more then one hit
        if (hits > 1) {
            hitCountStr += 's';
        }
        hitCountStr += '</span>'
        var distinctCountStr = '';
        // ID of the popover
        var popoverId = divId + '-popover-' + index;
        var value = 'value'
        // Create plural form if more then one distinct value
        if (distinctValues.length > 1) {
            value = 'values'
        }
        // Ignore nested document results
        if (!this.nodes().to$().find('>div.inline-table').length > 0) {
            var tooltip = '';
            // Only show popover if hits fit inside
            // Since we have 24 relationship types this is the maximum for predefined values
            if (distinctValues.length < 25) {
                tooltip = '<i id="' + popoverId + '" data-toggle="popover" class="clickable fas fa-info-circle" data-column="' + index + '"></i>'
            }
            distinctCountStr = ', <span class="distinct distinct-tooltip">' + distinctValues.length + ' Distinct ' + value + ' ' + tooltip + '</span>';
        }
        var clazz = 'statistics-row column-' + index;
        div.append('<li class="' + clazz + '">' + toggle + '<span class="statistics-title">Column "' + columnTitle + '":</span> ' + hitCountStr + distinctCountStr + '</li>');
        //Popover
        if ($('#' + popoverId)) {
            var popover = $('#' + popoverId);
            // Create content of popover
            var content = '<ul class="popover-distinct-value" data-column="' + index + '">';
            for (var v of distinctValues) {
                content += '<li class="distinct-selector">' + v + ': ' + distinctCounter[v] + '</li>';
            }
            content += '</ul>';
            // Title and disable button
            var title = '<span class="popover-title">Distinct ' + value + ' in column "' + columnTitle + '"</span>';
            popover.popover({
                // Use 'a' element instead of 'button' since stupid Bootstrap seems to filter things - passing 'data-' doesn't work either
                // A better Framework would provide an option for a close button
                'title': '<a class="btn statistics-popover-close" id="' + popoverId + '-close"><i class="clickable fas fa-times"></i></a>' + title,
                // Set content
                'content': content,
                'html': true,
                'placement': 'right'
            });
            // Enable close button
            $(document).on('click', '#' + popoverId + '-close', function(e) {
                e.preventDefault();
                // Disable active filters
                $(this).parents('.popover').find('li.distinct-selector.active').click();
                $('#' + popoverId).popover('hide');
            });
        }
        
        return this;
    }, {});
    // Enable table filters for distinct values
    // TODO: Filtering isn't working yet
    $(document).on('click', '.popover-distinct-value li.distinct-selector', function() {
        // This doesn't work since the 'data-column' attribute isn't passed
        //var columnNr = $(this).parents('ul.popover-distinct-value').attr('data-column');
        var columnNr = $(this).parents('.popover-body').prev('.popover-header').find('a.statistics-popover-close').attr('id').replace(/.*(\d+).*/gum, '$1');
        var ctx = table.column(columnNr);
        if (!$(this).hasClass('active')) {
            var text = $(this).text();
            ctx.search(text).draw();
            $(this).addClass('active');
        } else {
            ctx.search('').draw();
            $(this).removeClass('active');
        }
    });
    
    // Column toggle
    div.find('input.toggle').on('click', function () {
        var column = table.column($(this).attr('data-column'));
        // Disable popover for hidden columns 
        if ($(this).parent().find('.distinct-tooltip').length) {
            var popover = $(this).parent().find('.distinct-tooltip i[data-toggle="popover"]');
            popover.popover('toggleEnabled');
            if (popover.hasClass('clickable')) {
                popover.removeClass('clickable');
            } else {
                popover.addClass('clickable');
            }
        }
        column.visible(!column.visible());
        var all = true
        if (div.find('input:checkbox:not(:checked)').length) {
            all = false;
        }
        div.parent().find('.toggle-all input').prop('checked', all);
    });
    // Switch for all column toggles
    div.parent().append('<div class="table-functions"><span class="toggle-all"><input class="toggle" id="' + divId + '-all" type="checkbox" checked>Enable all</span></div>');
    $('#' + divId + '-all').on('click', function () {
        div.find('input:checkbox:not(:checked)').each(function () {
            $(this).click();
        });
    });
    // Disabled until implemented
    /*
    $('#' + divId + '-filter').on('click', function () {
        if ($(this).is(':checked')) {
            table.on('column-visibility', function (e, settings, colNr, state) {
                console.log('Column ' + colNr + ' has changed to ' + state);
                
                var hitRows = [];
                table.$('tr').find('td:nth-child(' + colNr + ') em.hl').parents('tr').each(function () {
                hitRows.push(this.index());
                });
                
                //table.column(colNr).nodes().to$().find('em.hl').parents('tr')
                
                // table.$('tr').find('td:nth-child(' + 2 + ') em.hl').parents('tr').each().index(this)
                //table.$('tr').find('td:nth-child(' + 2 + ') em.hl').parents('tr')
                //var rows = $('#' + tableId + ' tr td:nth-child(' + colNr + ')').parents('tr');
                console.log('found hits in');

            });
        } else {
            table.off('column-visibility');
        }
    });
    */

    if ($('#' + divId).hasAttr('data-refresh')) {
        var collapsableId = $('#' + divId).attr('data-refresh');
        $('#' + collapsableId).one('shown.bs.collapse', function() {
            // See https://stackoverflow.com/questions/6677035/jquery-scroll-to-element
            $([document.documentElement, document.body]).animate({scrollTop: $('#' + collapsableId).prevAll('.index-heading').offset().top}, 500);
        });
    }
}

$(document).ready(function(){
    initSearchBox('search-query-input');
    $('.search-panel .dropdown-menu').find('a').click(function(event) {
        event.preventDefault();
        var param = $(this).attr('href').replace('*','\\*').replace('#','') + ':';
        var concept = $(this).text();
        // Set the value in the dropdown
        $('.search-panel span#search_concept').text(concept);
        // Get cursor position
        // TODO: This doesn't work very well
        var inputElement = $('.input-group #search-query-input')[0];
        var pos = inputElement.selectionStart;
        var newPos = inputElement.selectionStart + param.length;
        // Get existing content
        var content = inputElement.value;
        if (pos != 0) {
            $(inputElement).val(content.substring(0, pos) + param + content.substring(pos));
        } else {
            $(inputElement).val(content + param)
        }
        // Activate input on new added string
        inputElement.selectionStart = newPos;
        $(inputElement).focus();
    });
   
    // Enable tooltips
    $('[data-toggle="tooltip"]').tooltip()
    $('.search-result-table.datagrid').each(function() {
        $(this).DataTable({
            'pageLength': 50,
            'autoWidth': false,
            'initComplete': function () {
                $(this).show();
                initStatistics($(this).attr('data-statistics-div'), $(this).attr('id'));
                $(this).find('.inline-table-content em.hl').closest('.inline-table-content').removeClass('collapse');
            }
        });
    });
});

$(document).ready(function() {
    const toggle = $('.dropdown-toggle');
    toggle.dropdown().on('click', () => {
        toggle.dropdown('toggle')
    })
});

$(document).ready(function() {
    const toggles = $('.btn.result-functions');

    toggles.each(function() {
        const toggle = $(this);
        console.log(toggle)
        toggle.collapse().on('click', () => {
            $(toggle.data('target')).collapse('toggle')
        })
    });
});

window.initStatistics = initStatistics;