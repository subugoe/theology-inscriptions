const $ = global.$ = global.jQuery = require('jquery');
// Our assets
var keyboard = require('./components/keyboard');
var dnd = require('./components/dnd');
require('./components/jquery-ext');
// Dependencies
require('select2');
require('jquery.are-you-sure');
require('bootstrap');
// Import CSS
import '../css/app.scss';

// Change 'select2' to avoid breakup of selected items
// See https://github.com/select2/select2/issues/3354
$.fn.select2.amd.require(['select2/selection/search'], function (Search) {
    var oldRemoveChoice = Search.prototype.searchRemoveChoice;

    Search.prototype.searchRemoveChoice = function () {
        oldRemoveChoice.apply(this, arguments);
        this.$search.val('');
    };

    $('#test').select2({width:'300px'});
});

// Needed for reloading select2 fields - only available in edit mode
// Add URLs for routes
let Routing = require('../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router');
let Routes = require('./js_routes');
// Loading routes
Routing.setRoutingData(Routes);

function array_remove(ar, rem) {
    return ar.filter((item) => item !== rem);
}

$(document).ready(function(){
    $(document).off('click.bs.collapse.data-api');
    if ($('body.edit').length || $('body.new').length) {
        // Applies to 'new' and 'edit'
        $('form select.select2-reload[data-widget="select2"]').each(function() {
            // Add route URL to select2 fields
            var url = Routing.generate($(this).attr('data-refresh-route'));
            $(this).attr('data-url', url);
        });
    }

    if ($('body.edit').length) {
        // Set up reloading for edit pages
        $('form select.select2-reload[data-widget="select2"]').each(function() {
            // Setup select2
            if ($(this).hasClass('select2-hidden-accessible')) {
                var select2Widget = $(this).select2;
                if (select2Widget != null) {
                    select2Widget.ajax = {
                        url: $(this).attr('data-url'),
                        dataType: 'json',
                        delay: 250,
                        data: {},
                        processResults: function (data) {
                            return data;
                        }
                    };
                    return;
                }
                console.log('Select2 widget not found at \'' + $(this).get(0).dataset.select2Id + '\', returning');
            } else {
                console.log('Select2 widget not initialized');
            }
        });
    } else if ($('body.list').length || $('body.show').length) {
        // Replace labels with "null"
        $('span.badge:contains(Null)').text('Empty');

        if ($('body.list-person').length) {
        // Drag and Drop for Person Ids
            $('div.content table.datagrid tr td span[draggable=true]').each(function() {
                dnd.addDnDPerson($(this));
            });
        }
    }

    // Additions to the edit and new mode of persons
    if ($('body.edit-person').length || $('body.new-person').length) {
        // Disable edit of generated FamilyMembers in Person edit
        $('div.field-family_members input[type=hidden][value=1]').closest('div.field-family_members').each(function () {
            //$(this).find(':input').prop('disabled', true);
            $(this).find('option:not(:selected)').prop('disabled', true);
            // This applies to the normal select element containing the relationship types
            $(this).find('select').addClass('relationship-generated');
            // Gray out the select2
            $(this).find('select ~ span.select2-container span.select2-selection').css('background-color', 'rgb(238, 238, 238)');
            //Disable mouse events
            $(this).find('select ~ span.select2-container').css('pointer-events', 'none');
            $(this).find(':input').prop('readonly', true);
            // Disable Link
            $(this).find('a').off().remove();
        });

        var create = function (htmlStr) {
            var frag = document.createDocumentFragment(),
                temp = document.createElement('div');
            temp.innerHTML = htmlStr;
            while (temp.firstChild) {
                frag.appendChild(temp.firstChild);
            }
            return frag;
        };

        // Draw a frame to indicate generated relationships
        var familyRelations = document.getElementsByClassName('field-family_members');
        Array.prototype.forEach.call(familyRelations, function(elmnt) {
            var generatedStateFamilyRelation = elmnt.getElementsByClassName('generated-relationship'),
                isGeneratedAutomatically = generatedStateFamilyRelation[0].getAttribute('value'),
                refNodes = elmnt.getElementsByClassName('form-group');
            if (isGeneratedAutomatically) {
                elmnt.firstChild.classList.add('relationship-generated-automatically');
                var fragment = create('<div><span class="badge badge-secondary auto-gen-label">Generated automatically</span></div>');
                refNodes[0].parentNode.insertBefore(fragment, refNodes[0]);
            }
        });

        // Reuse existing infrastructure to change new form collection items
        $(document).on('easyadmin.collection.item-added', function () {
            // Set route URLs for reloading selects
            $('.select2-reload').each(function() {
                var url = Routing.generate($(this).attr('data-refresh-route'));
                $(this).attr('data-url', url);
            });
            // Reenable "Add new person"
            $('small.form-help a.custom-help-conditions, small.form-help span.add-person-warning').css({'display': 'inline'});

            // Adding keyboard(s) was done in 'assets/js/components/keyboard.js'
            // Add keyboard to dynamic data collections (Family Members tab for Person)
            // This currently only works for the remarks field, since the visibility (z axis) seems to be wrong
            // We need to make sure that the keyboard is added after any 'select2' fields are initialized.
            // To do that we use the easyadmin event system

            $(this).find('div.form-widget-compound:last').find('.keyboard-input, .keyboard-input-textarea').each(function() {
                // Looking for 'select2-reload' is enough for the family members of persons
                if ($(this).hasClass('select2-reload')) {
                    // 'select2' has been enabled and initialized
                    if ($(this).hasClass('select2-hidden-accessible')) {
                        keyboard.addKeyboard($(this));
                    }
                } else {
                    keyboard.addKeyboard($(this));
                }
            });
        });

        // Disable adding new family members (as persons) if no relation exist
        if ($('div.form-widget-compound div.collection-empty').length) {
            $('small.form-help a.custom-help-conditions, small.form-help span.add-person-warning').css({'display': 'none'});
        }
        // Disable adding new family members (as persons) if last relationship is generated
        if ($('div.form-widget-compound:last div input[type=hidden][value=1]').length) {
            $('small.form-help a.custom-help-conditions, small.form-help span.add-person-warning').css({'display': 'none'});
        }

        // Remove link to add new person, if no form element is left
        $('div.form-widget').on('DOMNodeRemoved', function() {
            if ($(this).find('div.form-widget-compound div.collection-empty').length || $('.form-group .form-widget-compound').length <= 2 || $(this).find('.form-widget-compound:last div input[type=hidden][value=1]').length) {
                console.log($('.form-group .form-widget-compound').length);
                $('small.form-help a.custom-help-conditions, small.form-help span.add-person-warning').css({'display': 'none'});
            }
        });
    }

});

// Wait for reload until tab switched back from newly opened
document.onvisibilitychange = function() {

    var tabClasses = $('.select2-reload').closest('div.tab-content > div').map(function () {
        return $(this).classes();
    }).get();

    if (document.visibilityState === 'visible' && tabClasses.includes('active')) {
        $('form select.select2-reload[data-widget="select2"]').each(function() {
            // Check if we are in a repeatable form field
            if ($(this).closest('div[data-empty-collection]').length) {
                // If the current node is a child of the last element in repeatable form element
                // The function needs DOM nodes.
                if (!$.contains($(this).closest('div[data-empty-collection]').find('div.form-widget-compound:last').get(0), $(this).get(0))) {
                    return;
                }
                // Check if generated
                if ($(this).closest('div.field-family_members').find('input[type=hidden][value=1]').length) {
                    return;
                }
                // Check if disabled - reload still in progress
                if ($(this).prop('disabled')) {
                    return;
                }
            }
            // Check if there is a keyboard attached, if yes, close it
            keyboard.closeActiveKeyboard($(this));
            var adapter = $(this).data().select2.dataAdapter;
            var existingValues = [];
            $(this).children().each(function () {
                // Return if child element isn't a supported type
                if (!$(this).is('option') && !$(this).is('optgroup')) {
                    return;
                }
                // Create a list with existing values, needed to check if something has been removed
                existingValues.push(adapter.item($(this)).id);

            });
            // Block 'select' during load, set a placeholder if select is empty
            $(this).select2(function () {
                var init = {'disabled': true};
                if (!existingValues.length) {
                    init.placeholder = 'Loading...';
                }
                return init;
            }());
            // Merge this with the 'processResults' method of 'select2'
            $.ajax({
                type: 'GET',
                url: $(this).attr('data-url'),
                context: $(this)
            }).then(function(response) {
                // Loop thru the responses
                for (let index = 0; index < response.length; index++) {
                    // The value for the select option
                    var value = response[index].id.toString();
                    var content;
                    if ('name' in response[index]) {
                        content = response[index].name;
                    } else if ('fullname' in response[index]) {
                        content = response[index].fullname;
                    }
                    // Remove value from existing
                    existingValues = array_remove(existingValues, value);
                    //existingValues = existingValues.filter((item) => item !== value);
                    // Add option if value not found
                    if (!$(this).find('option[value=\'' + value + '\']').length) {
                        var option = new Option(content, value, true, true);
                        // Add new option value to list of selected
                        $(this).append(option).trigger('change');
                    }
                }
                // Handle the orphaned leftovers
                if (existingValues.length > 0) {
                    console.log('The following items have been removed since last load:' + existingValues.join(', '));
                    //Remove the elements
                    var context = $(this);
                    existingValues.forEach(function(optVal) {
                        context.find('option[value=' + optVal + ']').remove();
                        // Trigger refresh after change
                        context.trigger('change');
                    });
                }
                $(this).select2(function () {
                    var init = {'disabled': false};
                    if (!existingValues.length) {
                        init.placeholder = null;
                    }
                    return init;
                }());
                $(this).siblings('span').find('li.select2-selection__choice').css({'background-color': 'white'});
                // If keyboard is requested reenable it
                if ($(this).hasClass('keyboard-input')) {
                    keyboard.addKeyboard($(this));
                }
            });
            console.log('Visible - reload triggered');
        });
    }
};

// Showing the current name above of the page is now done via Twig templates
//require('./components/credits');
