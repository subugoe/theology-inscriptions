<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\If_\SimplifyIfElseToTernaryRector;
use Rector\CodeQuality\Rector\If_\SimplifyIfReturnBoolRector;
use Rector\CodingStyle\Rector\FuncCall\ArraySpreadInsteadOfArrayMergeRector;
use Rector\CodingStyle\Rector\String_\SymplifyQuoteEscapeRector;
use Rector\Config\RectorConfig;
use Rector\DeadCode\Rector\Cast\RecastingRemovalRector;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\Privatization\Rector\Class_\FinalizeClassesWithoutChildrenRector;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;
use Rector\Symfony\Set\SymfonySetList;

return static function (RectorConfig $rectorConfig): void {

    // here we can define, what sets of rules will be applied
    $rectorConfig->sets([
        SetList::CODE_QUALITY,
        LevelSetList::UP_TO_PHP_74,
        SymfonySetList::SYMFONY_44,
        DoctrineSetList::DOCTRINE_ORM_25,
        DoctrineSetList::DOCTRINE_CODE_QUALITY,
        DoctrineSetList::DOCTRINE_DBAL_211,
    ]);

    $rectorConfig->paths([__DIR__ . '/src']);

    $rectorConfig->importNames();
    $rectorConfig->skip([
        SimplifyIfReturnBoolRector::class,
        RecastingRemovalRector::class,
        SymplifyQuoteEscapeRector::class,
        FinalizeClassesWithoutChildrenRector::class,
        SimplifyIfElseToTernaryRector::class,
        ArraySpreadInsteadOfArrayMergeRector::class,
        SimplifyIfReturnBoolRector::class,
        __DIR__ . '/src/DataFixtures/*',
        __DIR__ . '/src/Migrations/*',
        __DIR__ . '/src/Util/Search/Query*',
        __DIR__ . '/src/Entity/FamilyMembers.php',
        __DIR__ . '/src/Entity/Languages.php',
    ]);
};
