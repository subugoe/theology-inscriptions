Theology inscriptions
=====================

Inscription system with EasyAdmin on Symfony 5

<a name="introduction"></a>

# Introduction

The application can be run via docker-compose as a set of related containers
(php, web frontend, database and search engine).
   
## Structure of the documentation

This documentation contains several sub pages and sections intended for different target audiences (developer, system administrator).
It's not expected that everyone understands everything, to dive right into the development environment head to the section:

  - [Status and TODO](./docs/status.md)

  - [Development](./docs/development.md)

  - [Technical background](./docs/development.md#technical)

    - [Docker usage tips](./docs/development.md#docker)
  
    - [Docker images and `docker-compose` files](./docs/docker.md)
  
    - [Handling Geo data](./docs/geodata.md)
  
    - [Changing the included help](./docs/content.md)
    
    - [Frontend tests using Selenium](./docs/selenium.md)

  - [Production](./docs/production.md)

  - [Advanced Development](./docs/advanced-development.md)

  - [Troubleshooting Guide](./docs/troubleshoot.md)

  - [GitLab CI](./docs/gitlab.md)

# Quick start

## Prerequisites (software and configuration)

The following software is needed, use the software management of your OS to get these:

- [Git](https://git-scm.com/), to get the sources.

- [Docker](https://www.docker.com/), make sure you have a version same as or newer then 18.09

- [Bash](https://www.gnu.org/software/bash/), is needed to run several scripts

To build the images you need Docker to [allow experimental features](https://docs.docker.com/engine/reference/commandline/dockerd/#description).

To run the PHP based tools (like static code checkers, Composer etc.) you need to have at least PHP 7.4 on path.

## Cloning and checkout of `dev` branch

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone --recurse-submodules https://gitlab.gwdg.de/subugoe/theology-inscriptions.git inscriptions
cd inscriptions
git checkout dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Run the development environment with one command:

For development there is a special `docker-compose` file:
`docker-compose.dev.yml`. It sets it's own environment variables and mounts the
local directory `.` inside the containers (see all changes in [`docker.md`](./docs/docker.md)).
This example uses pre build images from the [public registry](https://gitlab.gwdg.de/subugoe/theology-inscriptions/container_registry). 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Shut down the development environment with one command:

Make sure that you remove the volumes of the development version to have a fresh new version on next startup.,

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.dev.yml down -v
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Accessing the web application

Point your browser to <http://localhost:8008/>

## Updating a existing repository

If you already have checked out the repository, make sure to run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.dev.yml pull
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After each:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git pull
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Important note to developers

Before you commit your changes read this documentation, especially the [section on commiting changes](./docs/development.md#commit).
